#include <terra/geoid/Geoid.h>
#include <terra/terra.h>
#include <timber/math.h>
#include <sstream>
#include <fstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;
using namespace ::terra::geoid;

/**
 * This program is used to convert MSL to height above the ellipsoid.
 */
int main(int argc, char** argv)
{
   if (argc!=3) {
      ::std::cerr << "Usage : " << argv[0] << " <latitude> <longitude>" << ::std::endl;
      return 1;
   }

   istringstream sin(string(argv[1])+" " +argv[2]);

   double lat,lon;
   sin >> lat >> lon;

   if (sin.fail()) {
      ::std::cerr << "Usage : " << argv[0] << " <latitude> <longitude>" << ::std::endl;
      return 1;
   }

   ifstream geoidData("data/WW15MGH.GRD");
   Reference<Geoid> geoid = Geoid::read(geoidData);

   bool validHeight=false;

   double h = geoid->heightAboveEllipsoid(toRadians(lat),toRadians(lon),validHeight);
   if (!validHeight) {
      ::std::cerr << "Invalid location " << lat << "," << lon << ::std::endl;
      return 1;
   }
   ::std::cerr << "Height at " << lat << ", " << lon << " : " << h << ::std::endl;

   ::std::cout << h << ::std::endl;
   return 0;
}
