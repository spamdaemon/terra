#include <terra/CoordinateReferenceSystem.h>
#include <terra/Location.h>
#include <sstream>
#include <iostream>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;

int main(int argc, char** argv)
{

   Reference< CoordinateReferenceSystem> crs = CoordinateReferenceSystem::getEPSG4326();
   double latlon[] = { -89.5, -180 };
   Location loc(latlon, crs);

   ::std::cout << "Location : " << loc << ::std::endl;
   ////////////////////////////////////////////////////////////

   // do the projection
   Location mercator(loc);
   bool ok = mercator.setCRS(CoordinateReferenceSystem::getEPSG3395());
   if (!ok) {
      ::std::cerr << "Conversion failed" << ::std::endl;
      return 1;
   }
   ::std::cout << "Mercator : " << mercator << ::std::endl;
   ////////////////////////////////////////////////////////////
   Location invMercator(mercator);
   ok = invMercator.setCRS(crs);
   if (!ok) {
      ::std::cerr << "Inverse Conversion failed" << ::std::endl;
      return 1;
   }
   ::std::cout << "InvMercator : " << invMercator << ::std::endl;
   ////////////////////////////////////////////////////////////

   return 0;
}
