#include <terra/terra.h>
#include <terra/Angle.h>
#include <terra/GeodeticCoordinates.h>
#include <iostream>
#include <iomanip>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;

static double testDistance(double x1, double y1, double x2, double y2)
{
  double az1,az2;
  const Reference<GeodeticDatum> e = GeodeticDatum::create();
  Angle lat1=Angle::fromDegrees(x1);
  Angle lat2=Angle::fromDegrees(x2);
  Angle lon1=Angle::fromDegrees(y1);
  Angle lon2=Angle::fromDegrees(y2);
  Angle diffLon=Angle::fromDegrees(abs(y2-y1));
  
  double ap  = e->spheroid().computeApproximateDistance(lat1.radians(),lon1.radians(),lat2.radians(),lon2.radians());
  double d1 = e->spheroid().computeGeodesic(lat1.radians(),lon1.radians(),lat2.radians(),lon2.radians(),az1,az2);
  double d2 = 0;
  if (x1==x2) {
    double diff = diffLon.radians();
    diff = min(2.*Angle::PI-diff,diff);
    d2 = e->spheroid().parallelArcLength(lat1.radians(),diff);
  }
  else if (y1==y2) {
    d2=e->spheroid().meridianArcLength(lat1.radians(),lat2.radians());
    if (lat2.radians()<lat1.radians()) {
      d2 = -d2;
    }
  }
  else {
    //    assert ("Must have either same latitudes or same longitudes"==0);
    d2=-1;
  }
  cerr << setw(30) << setprecision(10) << "("<<x1<<", " << y1 << ") to ("<<x2<<", " << y2<< ") Distance : " << d1 << "  -  " << d2 << " = " << (d2-d1) << "  ~approx: " << ap <<  endl;
  assert (d2==-1 || abs(d1-d2) < 1000); // less than a km difference
  return (d1+d2)/2;
}

static void assertSameCoordinates (const GeodeticCoordinates& a, const GeodeticCoordinates& b) 
{
  cerr << "a : " << a << endl;
  cerr << "b : " << b << endl;

  assert (abs(a.latitude()-b.latitude())<toRadians(1.));
  assert (abs(a.longitude()-b.longitude())<toRadians(1.));
}

static void assertSameAngle(Double expected, Double actual, Double err)
{ 
  cerr << "SameAngle : expected " << toDegrees(expected) << ", was " << toDegrees(actual) << endl;

  assert(abs(sin(expected)-sin(actual)) <= err); 
  assert(abs(cos(expected)-cos(actual)) <= err); 
}

static GeodeticCoordinates parseValid (const char* s) throw ()
{
  GeodeticCoordinates loc = parseGeodeticCoordinates(s,GeodeticDatum::create());
  cerr << "Parsed " << s << " = " << loc << endl;
  return loc;
}

static void parseInvalid (const char* s)
{
  try {
    parseGeodeticCoordinates(s,GeodeticDatum::create());
    assert(false);
  }
  catch (const exception& e) {
    return;
  }
}

void utest_1()
{
  GeodeticCoordinates l1=GeodeticCoordinates::fromDegrees (45,40);
  GeodeticCoordinates l2=GeodeticCoordinates::fromDegrees (45,40);
  GeodeticCoordinates l3=GeodeticCoordinates::fromDegrees (46,40);

  assert (l1.equals(l2));
  assert (!l1.equals(l3));

  {
    GeodeticCoordinates x1=GeodeticCoordinates::fromDegrees(90,0);
    GeodeticCoordinates x2=GeodeticCoordinates::fromDegrees(90,180);
    assert (!x1.equals(x2));
    assert (x1.isSameLocation(x2));
  }

  {
    GeodeticCoordinates x1=GeodeticCoordinates::fromDegrees(-90,0);
    GeodeticCoordinates x2=GeodeticCoordinates::fromDegrees(-90,180);
    assert (!x1.equals(x2));
    assert (x1.isSameLocation(x2));
  }
  
  {
    GeodeticCoordinates x1=GeodeticCoordinates::fromDegrees(0,-180);
    GeodeticCoordinates x2=GeodeticCoordinates::fromDegrees(0,180);
    assert (!x1.equals(x2));
    assert (x1.isSameLocation(x2));
  }
  
}

void utest_2()
{
  GeodeticCoordinates l1 = parseValid("0100000.00E480000.00N");
  assert(l1==GeodeticCoordinates::fromDegrees(48,10));
  l1 = parseValid("480000.00S0100000.00W");
  assert(l1==GeodeticCoordinates::fromDegrees(-48,-10));
  l1 = parseValid("48S100W");
  assert(l1==GeodeticCoordinates::fromDegrees(-48,-100));
  l1 = parseValid("90S180W");
  assert(l1==GeodeticCoordinates::fromDegrees(-90,-180));
  parseValid("895959.12345678S1795959.123456789E");
  parseValid("89 59 59.12345678S  179  59  59.12345678 E");
}

void utest_3()
{
  parseInvalid("180000.1E20S");
  parseInvalid("9010N20E");
  parseInvalid("9060N20E");
  parseInvalid("900060N20E");
  parseInvalid("9060E20S");
  parseInvalid("900060E20S");
  parseInvalid("9 0 6. 0E20S");
  parseInvalid("9010N20 0 0. 00E");
}

void utest_4()
{
  cerr << "TEST 4" << endl;
  const Reference<GeodeticDatum> e = GeodeticDatum::createWGS84();
  const Reference<GeodeticDatum> s = GeodeticDatum::create(Spheroid::createSphere(e->spheroid().majorAxisLength(),0,0,0));
  
  for (Double lat=-90;lat<=90;lat+= 5) {
    for (Double lon=-180;lon<=180;lon+=5) {
      
      const GeodeticCoordinates a=GeodeticCoordinates::fromDegrees(lat,lon,e);
      const GeodeticCoordinates b=GeodeticCoordinates::fromDegrees(lat,lon,s);
      
      double pA[2],pB[2];
      a.mercatorProjection(pA[0],pA[1]);
      b.mercatorProjection(pB[0],pB[1]);

      const GeodeticCoordinates A(GeodeticCoordinates::inverseMercatorProjection(pA[0],pA[1],a.datum()));
      const GeodeticCoordinates B(GeodeticCoordinates::inverseMercatorProjection(pB[0],pB[1],b.datum()));
     
      cerr << "GEODETICFRAME";
      assertSameCoordinates(b,B);
      cerr << "ELLIPSOID";
      assertSameCoordinates(a,A);
    }
  }
}

void utest_5()
{
  {
    GeodeticCoordinates x=GeodeticCoordinates::fromDegrees(0,0);
    GeodeticCoordinates y=x.antipodalCoordinates();
    assertSameAngle(x.latitude(),-y.latitude(),1E-10);
    assertSameAngle(x.longitude(),y.longitude()+M_PI,1E-6);
  }

  {
    GeodeticCoordinates x=GeodeticCoordinates::fromDegrees(90,0);
    GeodeticCoordinates y=x.antipodalCoordinates();
    assertSameAngle(x.latitude(),-y.latitude(),1E-10);
    assertSameAngle(x.longitude(),y.longitude()+M_PI,1E-6);
  }


}



static void test6()
{
  Reference<GeodeticDatum> e = GeodeticDatum::create();
  GeodeticCoordinates p1=parseGeodeticCoordinates("144 25 29.5244E 37 57 03.7203S",e);
  GeodeticCoordinates p2=parseGeodeticCoordinates("143 55 35.3839E 37 39 10.1561S",e);
  testDistance(toDegrees(p1.latitude()),toDegrees(p1.longitude()),toDegrees(p2.latitude()),toDegrees(p2.longitude()));
  testDistance(0,0,0,-10);
  testDistance(0,0,0,20);
  testDistance(0,10,0,20);
  testDistance(0,0,10,0);
  testDistance(0,0,45,0);
  testDistance(10,0,45,0);
  testDistance(-45,0,45,0);
  testDistance(-45,10,45,10);
  testDistance(45,0,-45,0);
  testDistance(-45,0,-10,0);
  testDistance(45,0,10,0);
  testDistance(0,0,0,10);
  testDistance(0,0,0,0);

  testDistance(0, 0, 10, 0);
  testDistance( -90, 0, 90, 0);
  testDistance(0, 0, 0, 0);
  testDistance(1, 1, 0, 0);
  testDistance(0, 0, 90, 0);
  testDistance(0, -180, 0, 180);
  testDistance( -90, -180, 90, 180);


}

static void test7()
{
  GeodeticCoordinates O;
  double dist = 12345;
  {
    GeodeticCoordinates north = O.travelAlongLoxodrome(dist,0);
    GeodeticCoordinates east = O.travelAlongLoxodrome(dist,PI/2);
    GeodeticCoordinates south = O.travelAlongLoxodrome(dist,PI);
    GeodeticCoordinates west = O.travelAlongLoxodrome(dist,-PI/2);
    
    // check with geodesic distance (this is on purpose and only works because
    // we're either traveling north/south or east/west
    assert(::std::abs(north.geodesicDistance(O) - dist) < .0001);
    assert(::std::abs(east.geodesicDistance(O) - dist) < .0001);
    assert(::std::abs(south.geodesicDistance(O) - dist) < .0001);
    assert(::std::abs(west.geodesicDistance(O) - dist) < .0001);
  }
  
  {
    GeodeticCoordinates north = O.travelAlongGeodesic(dist,0);
    GeodeticCoordinates east = O.travelAlongGeodesic(dist,PI/2);
    GeodeticCoordinates south = O.travelAlongGeodesic(dist,PI);
    GeodeticCoordinates west = O.travelAlongGeodesic(dist,-PI/2);
    
    assert(::std::abs(north.loxodromicDistance(O) - dist) < .0001);
    assert(::std::abs(east.loxodromicDistance(O) - dist) < .0001);
    assert(::std::abs(south.loxodromicDistance(O) - dist) < .0001);
    assert(::std::abs(west.loxodromicDistance(O) - dist) < .0001);
  }
}

static void test8()
{
  Reference< Spheroid> datum1 = Spheroid::create(Spheroid::WGS_84);
  Reference< Spheroid> datum2 = Spheroid::createSphere(datum1->majorAxisLength(), 0, 0, 0);
  GeodeticCoordinates O1(0,0,0,GeodeticDatum::create(datum1));
  GeodeticCoordinates O2(O1);
  O2.changeDatum(GeodeticDatum::create(datum2));
  assert(O2.datum()->spheroid().equals(*datum2));
}
