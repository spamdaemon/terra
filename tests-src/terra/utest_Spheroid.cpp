#include <terra/Spheroid.h>
#include <terra/Angle.h>
#include <terra/terra.h>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;

static void assertEquals(double expected, double actual, double err = 1E-6)
{
   ::std::cerr << "Equals : expected " << expected << ", was " << actual << " |diff|=" << abs(expected - actual)
         << endl;

   assert(abs(expected - actual) <= err);
}
static void assertSameAngle(double expected, double actual, double err)
{
   cerr << "SameAngle : expected " << toDegrees(expected) << ", was " << toDegrees(actual) << endl;

   assert(abs(sin(expected) - sin(actual)) <= err);
   assert(abs(cos(expected) - cos(actual)) <= err);
}

static void testConversion(double lat1, double lon1, double alt)
{
   cerr << "1GEO: " << lat1 << " " << lon1 << " " << alt << endl;

   Angle lat = Angle::fromDegrees(lat1);
   Angle lon = Angle::fromDegrees(lon1);

   double az1, az2;
   double x[3];
   const Reference< Spheroid> e = Spheroid::create();
   e->computeECEF(lat.radians(), lon.radians(), alt, x);
   cerr << "ECEF:" << x[0] << " " << x[1] << " " << x[2] << endl;
   double lat2, lon2, alt2;
   alt2 = -1E8;
   e->computeGeodetic(x, lat2, lon2, alt2);
   cerr << "GEO: " << lat2 << " " << lon2 << " " << alt2 << endl;

   double d = e->computeGeodesic(lat.radians(), lon.radians(), lat2, lon2, az1, az2);

   cerr << "Distance after conversions : " << d << endl;
   assert(abs(lat.radians() - lat2) < .000001);
   assert(abs(lon.radians() - lon2) < .000001);
   assert(abs(alt - alt2) < .000001);
}

void utest_2()
{
   const Reference< Spheroid> e = Spheroid::create();
   {
      Angle lat1 = Angle::fromDegrees(-10);
      Angle lat2 = Angle::fromDegrees(10);

      assert(e->meridianArcLength(lat1.radians()) < 0);
      assert(e->meridianArcLength(lat2.radians()) >= 0);
      // in theory, these should be equal, but have opposite sign. Optimized code can however
      // cause them to be slightly off
      ::std::cerr << "MeridianArcLength : " << ::std::endl << "      " << lat1.degrees() << " "
            << e->meridianArcLength(lat1.radians()) << ::std::endl << "      " << lat2.degrees() << " "
            << e->meridianArcLength(lat2.radians()) << ::std::endl << "delta "
            << e->meridianArcLength(lat1.radians()) + e->meridianArcLength(lat2.radians()) << ::std::endl;

      assertEquals(e->meridianArcLength(lat1.radians()), -e->meridianArcLength(lat2.radians()));

      assertEquals(lat1.radians(), e->inverseMeridianArcLength(e->meridianArcLength(lat1.radians())), toRadians(.0001));
      double a1 = e->area(lat1.radians());
      double a2 = e->area(lat2.radians());
      cerr << "AREA1 " << a1 << "   AREA2 " << a2 << endl;
      assertEquals(a1, a2, 1E-10); // must be exactly the same, because there is no difference
      assertEquals(a1, a2);

      double a12 = e->area(lat1.radians(), lat2.radians());
      cerr << "AREA1+2 " << (a1 + a2) << "   AREA12 " << a12 << endl;

      assertEquals(a12, (a1 + a2), 1000);

      assert(e->area(0) < .00000001);
      assert(e->area(Angle::fromDegrees(10).radians()) < e->area(Angle::fromDegrees(20).radians()));
      assert(a1 >= 0);
      assert(a2 >= 0);
   }

   {
      double lat = toRadians(60.);
      double dist = e->meridianArcLength(lat);
      assertEquals(dist, -e->meridianArcLength(-lat));
      assertEquals(2 * dist, e->meridianArcLength(-lat, lat));
      assertEquals(2 * dist, -e->meridianArcLength(lat, -lat));
   }

   {
      Reference< Spheroid> s = Spheroid::create(Spheroid::GRS_1975);
      double lat1 = toRadians(40.);
      double lat2 = toRadians(41.);
      double dist = s->meridianArcLength(lat1, lat2);
      assertEquals(111042.39, dist, 2);
   }
}

void utest_3()
{
   testConversion(0, 0, 0);
   testConversion(0, 0, 45);
   testConversion(90, 0, 0);
   testConversion(0, 180, 0);
   testConversion(-90, 0, 4);
   testConversion(0, -180, 0);
   testConversion(-90, -180, 0);
   testConversion(90, 180, 0);
}

void utest_4()
{
   const Reference< Spheroid> S = Spheroid::createSphere(1.0, 0, 0, 0);

   {
      cerr << "test5" << endl;
      double lat1 = toRadians(1.);
      double lat2 = toRadians(2.);
      double lon = toRadians(0.);
      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon, lat2, lon, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertSameAngle(az12, 0.0, 1E-7);
      assertSameAngle(az21, Angle::PI, 1E-7);
   }

   {
      cerr << "-----------------------" << endl;
      double lat1 = toRadians(90.);
      double lat2 = toRadians(2.);
      double lon = toRadians(0.);
      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon, lat2, lon, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertSameAngle(az21, 0.0, 1E-7);
   }

   {
      cerr << "-----------------------" << endl;
      double lat1 = toRadians(2.);
      double lat2 = toRadians(2.);
      double lon = toRadians(0.);
      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon, lat2, lon, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertEquals(r, 0.0, 1E-7);
   }

   {
      cerr << "-----------------------" << endl;
      double lat1 = toRadians(90.);
      double lat2 = toRadians(-90.);
      double lon = toRadians(0.);
      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon, lat2, lon, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertEquals(r, Angle::PI, 1E-7);
   }

   {
      cerr << "-----------------------" << endl;
      double lat1 = toRadians(90.);
      double lat2 = toRadians(90.);
      double lon = toRadians(0.);
      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon, lat2, lon, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertEquals(r, 0, 1E-7);
   }
}

void utest_5()
{
   const Reference< Spheroid> S = Spheroid::createSphere(1.0, 0, 0, 0);

   {
      cerr << "test6" << endl;
      double lat1 = toRadians(1.);
      double lat2 = toRadians(2.);
      double lon1 = toRadians(0.);
      double lon2 = toRadians(90.);

      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
   }

   {
      cerr << "----------------" << endl;
      double lat1 = toRadians(0.);
      double lat2 = toRadians(90.);
      double lon1 = toRadians(0.);
      double lon2 = toRadians(90.);

      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertEquals(r, Angle::PI / 2.0, 1E-7);
      assertSameAngle(az12, 0.0, 1E-7);
   }

   {
      cerr << "----------------" << endl;
      double lat1 = toRadians(90.);
      double lat2 = toRadians(0.);
      double lon1 = toRadians(0.);
      double lon2 = toRadians(90.);

      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertEquals(r, Angle::PI / 2.0, 1E-7);
      assertSameAngle(az21, 0.0, 1E-7);
   }

   {
      cerr << "----------------" << endl;
      double lat1 = toRadians(90.);
      double lat2 = toRadians(90.);
      double lon1 = toRadians(0.);
      double lon2 = toRadians(90.);

      double az12, az21, r;
      r = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "R   : " << r << endl << "AZ12: " << az12 << endl << "AZ21: " << az21 << endl;

      assert(!::std::isnan(r));
      assert(!::std::isnan(az12));
      assert(!::std::isnan(az21));
      assertEquals(r, 0.0, 1E-7);
   }

}

void utest_6()
{
   const Reference< Spheroid> S = Spheroid::createSphere(1.0, 0, 0, 0);
   const Reference< Spheroid> E = Spheroid::createEllipsoid(1.0, 1E-12, 0, 0, 0);
   double err = 1E-5;

   {
      double az12, az21;
      double ll[] = { 20, 20, 30, 30 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      double e = E->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(s, e, err);
   }

}

void utest_7()
{
   Reference< Spheroid> S = Spheroid::create(Spheroid::WGS_84);
   double err = .05;
   cerr << "Testing GEODESIC calculations for WGS 84" << endl;
   {
      double az12, az21;
      double ll[] = { 0, 0, 10, 10 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1565109.058357, s, err);
   }

   {
      double az12, az21;
      double ll[] = { 10, 10, 20, 20 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1541856.393044, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 20, 20, 30, 30 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1497148.883105, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 30, 30, 40, 40 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1434648.648015, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 40, 40, 50, 50 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1359994.841018, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 50, 50, 60, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1280889.181790, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 60, 60, 70, 70 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1206952.731226, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 70, 70, 80, 80 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1148942.028767, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 80, 80, 90, 90 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(1116825.815520, s, err);
   }

   err = .25;
   {
      double az12, az21;
      double ll[] = { 0, 0, 10, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(6733982.052350, s, err);
   }

   {
      double az12, az21;
      double ll[] = { 0, 0, 20, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(6895385.608517, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 0, 0, 30, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(7154900.375924, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 0, 0, 40, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(7500166.430964, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 0, 0, 50, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(7916757.847739, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 0, 0, 60, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(8389654.591465, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 0, 0, 70, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(8904125.604470, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 0, 0, 80, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(9446067.760905, s, err);
   }
   {
      double az12, az21;
      double ll[] = { 0, 0, 90, 60 };
      double s = S->computeGeodesic(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12, az21);
      assertEquals(10001965.504973, s, err);
   }

}

void utest_8()
{
   Reference< Spheroid> S = Spheroid::create(Spheroid::WGS_84);

   double sErr = .01;
   double lat1 = 0;
   double lon1 = toRadians(0.);

   {
      double lat2 = toRadians(10.);
      double lon2 = toRadians(10.);
      double az12, az21, s;
      cerr << "Loxodrome from " << lat1 << "," << lon1 << " to " << lat2 << "," << lon2 << endl;
      s = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "GEODESIC   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s
            << endl;
      s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "LOXODROME   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s
            << endl;
      assertEquals(1565125.307, s, sErr);
      assertSameAngle(toRadians(45.2), S->computeTrueCourse(lat1, lon1, lat2, lon2), toRadians(.2));
      assertSameAngle(S->computeTrueCourse(lat1, lon1, lat2, lon2), az12, toRadians(.0001));
      assertSameAngle(toRadians(225.2), az21, toRadians(.2));
      double az12_2, az21_2;
      double s2 = S->computeLoxodrome(lat2, lon2, lat1, lon1, az12_2, az21_2);
      assertEquals(s, s2, 1E-8);
      assertSameAngle(az12_2, az21, 1E-7);
      assertSameAngle(az21_2, az12, 1E-7);
   }
}

void utest_9()
{
   Reference< Spheroid> S = Spheroid::create(Spheroid::WGS_84);
   double sErr = 30;
   double lat1 = 0;
   double lon1 = 0;

   {
      double lat2 = toRadians(10.);
      double lon2 = lon1;
      double az12, az21, s;
      cerr << "Loxodrome from " << lat1 << "," << lon1 << " to " << lat2 << "," << lon2 << endl;
      s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s << endl;
      assert(lon1 == lon2);
      assertEquals(s, S->meridianArcLength(lat1, lat2), sErr);

   }

   {
      double lat2 = lat1;
      double lon2 = toRadians(10.);
      double az12, az21, s;
      cerr << "Loxodrome from " << lat1 << "," << lon1 << " to " << lat2 << "," << lon2 << endl;
      s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s << endl;
      assert(lat1 == lat2);
      assertEquals(S->parallelArcLength(lat1, lon2 - lon1), s, sErr);
   }

   {
      double lat2 = toRadians(1.);
      double lon2 = toRadians(1.);
      double az12, az21, s;
      cerr << "Loxodrome from " << lat1 << "," << lon1 << " to " << lat2 << "," << lon2 << endl;
      s = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "GEODESIC   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s
            << endl;
      s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "LOXODROME   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s
            << endl;
      assertEquals(156902.2, s, sErr);
      assertSameAngle(toRadians(45.2), S->computeTrueCourse(lat1, lon1, lat2, lon2), toRadians(.1));
      assertSameAngle(S->computeTrueCourse(lat1, lon1, lat2, lon2), az12, toRadians(.1));
      assertSameAngle(toRadians(225.2), az21, toRadians(.1));
   }

   {
      double lat2 = toRadians(10.);
      double lon2 = toRadians(10.);
      double az12, az21, s;
      cerr << "Loxodrome from " << lat1 << "," << lon1 << " to " << lat2 << "," << lon2 << endl;
      s = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "GEODESIC   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s
            << endl;
      s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "LOXODROME   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s
            << endl;
      assertEquals(1565151.3, s, sErr);
      assertSameAngle(toRadians(45.2), S->computeTrueCourse(lat1, lon1, lat2, lon2), toRadians(.2));
      assertSameAngle(toRadians(225.2), az21, toRadians(.2));
      assertSameAngle(S->computeTrueCourse(lat1, lon1, lat2, lon2), az12, toRadians(.001));
   }
}

void utest_10()
{
   Reference< Spheroid> S = Spheroid::create(Spheroid::WGS_84);

   double sErr = 30;
   double lat1 = 0;
   double lon1 = toRadians(-175.);

   {
      double lat2 = lat1;
      double lon2 = toRadians(175.);
      double az12, az21, s;
      cerr << "Loxodrome from " << lat1 << "," << lon1 << " to " << lat2 << "," << lon2 << endl;
      s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s << endl;
      assert(lat1 == lat2);
      assertEquals(S->parallelArcLength(lat1, toRadians(10.)), s, sErr);
   }

   {
      double lat2 = toRadians(10.);
      double lon2 = toRadians(175.);
      double az12, az21, s;
      cerr << "Loxodrome from " << lat1 << "," << lon1 << " to " << lat2 << "," << lon2 << endl;
      s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);
      cerr << "   az12 : " << toDegrees(az12) << ", az21 : " << toDegrees(az21) << ",  distance : " << s << endl;
      assertEquals(1565151.3, s, sErr);
      assertSameAngle(toRadians(134.9), az21, toRadians(.2));
      assertSameAngle(toRadians(314.9), S->computeTrueCourse(lat1, lon1, lat2, lon2), toRadians(.2));
      assertSameAngle(S->computeTrueCourse(lat1, lon1, lat2, lon2), az12, toRadians(.001));
   }
}

void utest_11()
{
   Reference< Spheroid> S = Spheroid::create();
   double err = 1E-10;
   {
      double az12, az21;
      double ll[] = { 0, 0, 70, 60 };
      double s = S->computeLoxodrome(toRadians(ll[0]), toRadians(ll[1]), toRadians(ll[2]), toRadians(ll[3]), az12,
            az21);
      double lat, lon;
      S->travelAlongLoxodrome(toRadians(ll[0]), toRadians(ll[1]), az12, s, lat, lon);
      assertEquals(ll[2], toDegrees(lat), err);
      assertEquals(ll[3], toDegrees(lon), err);
      S->travelAlongLoxodrome(toRadians(ll[2]), toRadians(ll[3]), az21, s, lat, lon);
      assertEquals(ll[0], toDegrees(lat), err);
   }

   {
      double lat1 = -Angle::PI / 2;
      double lon1 = 0.0;
      double loxodromeLat, loxodromeLon;
      double geodesicLat, geodesicLon;

      S->travelAlongLoxodrome(lat1, lon1, Angle::PI / 2, 1000000, loxodromeLat, loxodromeLon);
      S->travelAlongGeodesic(lat1, lon1, Angle::PI / 2, 1000000, geodesicLat, geodesicLon);

      assertEquals(loxodromeLat, geodesicLat, 0.0);
      assertEquals(loxodromeLon, geodesicLon, 0.0);
      assertEquals(loxodromeLon, Angle::PI / 2, 1E-10);
   }

}
#if 0
void utest_12()
{
   Reference<Spheroid> S;
   double lat1=toRadians(-89.5);
   double lon1=toRadians(-48);

   double lat2 = toRadians(-89.5);
   double lon2 = toRadians(132);
   double az12,az21,s;
   s = S->computeGeodesic(lat1,lon1,lat2,lon2,az12,az21);
   assertEquals(Angle::PI,az12,.000001);
   assertEquals(Angle::PI,az21,.000001);

   GeodeticCoordinates p1(lat1,lon1);
   GeodeticCoordinates p2(lat2,lon2);

   Geodesic G(p1,p2);
   assertSameAngle(Angle::PI,G.bearing(),.00001);
}
#endif

void utest_13()
{
   const Reference< Spheroid> S = Spheroid::createSphere(Spheroid::create(Spheroid::WGS_84)->majorAxisLength(), 0, 0,
         0);
   double lat1 = toRadians(-89.5);
   double lon1 = toRadians(-180.);

   double lat2 = toRadians(0.);
   double lon2 = toRadians(-180.);
   double az12, az21, s;
   s = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
   assertEquals(0, az12, .000001);
   assertEquals(Angle::PI, az21, .001);
   assertEquals(S->meridianArcLength(lat1, lat2), s, 1);
   double ll[2];
   S->travelAlongGeodesic(lat1, lon1, az12, s, ll[0], ll[1]);
   assertEquals(lat2, ll[0], .01);
   assertEquals(lon2, ll[1], .01);

   lat2 = lat1;
   lon2 = toRadians(-167.);
   s = S->computeGeodesic(lat1, lon1, lat2, lon2, az12, az21);
   double az12_2, az21_2;
   double s2 = S->computeGeodesic(lat2, lon2, lat1, lon1, az12_2, az21_2);
   S->travelAlongGeodesic(lat2, lon2, az12_2, s2, ll[0], ll[1]);
   assertEquals(s, s2, 1E-3);
   assertSameAngle(lat1, ll[0], toRadians(.01));
   assertSameAngle(lon1, ll[1], toRadians(.01));
}

void utest_14()
{
   Reference< Spheroid> S = Spheroid::create(Spheroid::WGS_84);
   S = Spheroid::createSphere(S->majorAxisLength(), 0, 0, 0);
   {
      double lat1 = toRadians(0.);
      double lon = toRadians(67.);
      double lat2 = toRadians(0.);
      double d = S->meridianArcLength(lat1, lat2);
      double x, y;
      S->travelAlongMeridianArc(lat1, lon, lon, d, x, y);
      assertSameAngle(lat2, x, toRadians(.01));
      assertSameAngle(lon, y, 1E-10);
   }

   {
      double lat1 = toRadians(73.);
      double lon = toRadians(67.);
      double lat2 = toRadians(66.);
      double d = S->meridianArcLength(lat1, lat2);
      double x, y;
      S->travelAlongMeridianArc(lat1, lon, lon, d, x, y);
      assertSameAngle(lat2, x, toRadians(.01));
      assertSameAngle(lon, y, 1E-10);
   }

   {
      double lat1 = toRadians(73.);
      double lon = toRadians(67.);
      double lat2 = toRadians(-66.);
      double d = S->meridianArcLength(lat1, lat2);
      double x, y;
      S->travelAlongMeridianArc(lat1, lon, lon, d, x, y);
      assertSameAngle(lat2, x, toRadians(.01));
      assertSameAngle(lon, y, 1E-10);
   }

   {
      double lat1 = toRadians(90.);
      double lon = toRadians(67.);
      double lat2 = toRadians(-66.);
      double d = S->meridianArcLength(lat1, lat2);
      double x, y;
      S->travelAlongMeridianArc(lat1, lon, lon, -d, x, y);
      assertSameAngle(lat2, x, toRadians(.01));
      assertSameAngle(lon, y, 1E-10);
   }

   {
      double lat1 = toRadians(-90.);
      double lon = toRadians(67.);
      double lat2 = toRadians(-66.);
      double d = S->meridianArcLength(lat1, lat2);
      double x, y;
      S->travelAlongMeridianArc(lat1, lon, lon, d, x, y);
      assertSameAngle(lat2, x, toRadians(.01));
      assertSameAngle(lon, y, 1E-10);
   }

   {
      double lat1 = toRadians(-90.);
      double lon = toRadians(67.);
      double lat2 = toRadians(-66.);
      double d = S->meridianArcLength(lat1, lat2);
      double x, y;
      S->travelAlongMeridianArc(lat1, lon, lon, -d, x, y);
      assertSameAngle(lat2, x, toRadians(.01));
      assertSameAngle(lon + Angle::PI, y, 1E-10);
   }
}

/** Test that failed in the Java port */
static void utest_15()
{
   Reference< Spheroid> S = Spheroid::create(Spheroid::WGS_84);
   double lat1 = toRadians(33.0111);
   double lon1 = toRadians(-114.49995);
   double lat2 = toRadians(33.00614929199219);
   double lon2 = toRadians(-114.49662017822266);
   double az12, az21;

   double s = S->computeLoxodrome(lat1, lon1, lat2, lon2, az12, az21);

   assertEquals(632, s, 10.0);
}
