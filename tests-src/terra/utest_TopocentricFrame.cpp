#include <iostream>
#include <cassert>
#include <cmath>

#include <terra/ECEFCoordinates.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/TopocentricCoordinates.h>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;
using namespace ::newton;

void utest_rotateToECEF()
{
  double latDeg = 32.1;
  double lonDeg = -110.9;
  double hae    = 865;
  
  // get the rotation matrix to transform to ECEF, we use this transform the local
  // covariance into an absolute/global covariance
  Matrix<double> P(3,3);
  // we're using teh circular error as the covariance (for now...)
  P(0,0) = 100;
  P(1,1) = P(0,0);   // since the error is circular, we assign ce to both x and y
  P(2,2) = 100;
  
  // create the point
  const GeodeticCoordinates geo = GeodeticCoordinates::fromDegrees(latDeg,lonDeg,hae);
  const ECEFCoordinates ecef(geo);
  
  // create a 3D covariance with ce and le
  const TopocentricCoordinates tcs(geo);
  
  Matrix<double> R = tcs.frame()->getRotationToECEF();
  
  P = (R*P*R.transpose());

  // the covariance must be near symmetric, so test for it
  for (size_t i=0;i<3;++i) {
    for (size_t j=i;j<3;++j) {
      assert(::std::abs(P(i,j)-P(j,i)) < .01);
    }
  }
}
