#include <terra/terra.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/ECEFCoordinates.h>
#include <terra/Geodesic.h>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;
using namespace timber;
using namespace terra;

static void assertEquals(double expected, double actual, double err)
{
  cerr << "Equals : expected " << expected << ", was " << actual<< " |diff|="<< abs(expected-actual) << endl;

  assert(abs(expected-actual) <= err); 
}
static void assertSameAngle(double expected, double actual, double err)
{ 
  cerr << "SameAngle : expected " << toDegrees(expected) << ", was " << toDegrees(actual) << endl;

  assert(abs(sin(expected)-sin(actual)) <= err); 
  assert(abs(cos(expected)-cos(actual)) <= err); 
}

static void checkCoords (const GeodeticCoordinates& c1, const GeodeticCoordinates& c2, double diff)
{
  ECEFCoordinates a(c1);
  ECEFCoordinates b(c2);
  
  for (size_t i=0;i<3;++i) {
    double d = abs(a(i)-b(i));
    if (d > diff) {
      cerr << setiosflags(ios::fixed) << setprecision(4);
      cerr << c1 << " ->  " << a << endl;
      cerr << c2 << " ->  " << b << endl;
      cerr << "DIFF : " << i << " = " << d << "(" << diff << ")" << endl;
      assert (d <= diff);
    }
  }
}

static void verifyDistance (const char* a, const char* b, double d) 
{
  const GeodeticCoordinates c1(parseGeodeticCoordinates(a,GeodeticDatum::create()));
  const GeodeticCoordinates c2(parseGeodeticCoordinates(b,GeodeticDatum::create()));
  const Geodesic rb(c1,c2);
  
  double diff = abs(rb.range()-d);
  double tol = .1;

  if (diff>=tol) {
    cerr << setiosflags(ios::fixed) << setprecision(4) << setw(12);
    cerr << "VERIFY DISTANCE FAILED : " << a << " to " << b 
		<< "  =  " << rb.range() << ", should be " << d 
		<< " approx " << c1.approximateDistance(c2) 
		<< endl;
    assert (diff < tol);
  }
}


void utest_1()
{
  GeodeticCoordinates p1=GeodeticCoordinates::fromDegrees(20,0);
  Geodesic rb1(12000000,0);
  GeodeticCoordinates p2 = rb1.move(p1);
  cerr << "From : " << p1 << "  to  " << p2 << endl;
  Geodesic rb2(p1,p2);
  cerr << "Range bearing " << rb1 << " vs. " << rb2 << endl;
  assert (abs(rb2.bearing()) < 1E-8);
  
  p1 = GeodeticCoordinates::fromDegrees(-20,0);
  rb1 = Geodesic(12000000,M_PI);
  p2 = rb1.move(p1);
  cerr << "From : " << p1 << "  to  " << p2 << endl;
  rb2=Geodesic(p1,p2);
  cerr << "Range bearing " << rb1 << " vs. " << rb2 << endl;
  assert (abs(rb2.bearing()-M_PI) < 1E-8);
}

void utest_2()
{
  GeodeticCoordinates p1 =GeodeticCoordinates::fromDegrees(-89.9,-180);
  GeodeticCoordinates p2 =GeodeticCoordinates::fromDegrees(-30,-180);
  cerr << "1. From : " << p1 << "  to  " << p2 << endl;
  Geodesic rb12(p1,p2);
  Geodesic rb21(p2,p1);
  GeodeticCoordinates q1 = rb21.move(p2);
  GeodeticCoordinates q2 = rb12.move(p1);
  cerr << "Range bearing " << rb12 << " vs. " << rb21 << endl;
  cerr << "2. From : " << q1 << "  to  " << q2 << endl;

  cerr << "P1   : " << p1 << '\n'
	      << "Q1   : " << q1 << '\n'
	      << "RB12 : " << rb12 << '\n'
	      << "P2   : " << p2 << '\n'
	      << "Q2   : " << q2 << '\n'
	      << "RB21 : " << rb21  << '\n';

  
  checkCoords(p1,q1,2);
  checkCoords(p2,q2,2);
}

void utest_3()
{
  cerr << "Test 3: not yet working" << endl;
#if 0
  {
    Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(6378137.0,.5));
    GeodeticCoordinates p1=GeodeticCoordinates::fromDegrees(10,0,0,e);
    GeodeticCoordinates p2=GeodeticCoordinates::fromDegrees(10,170,0,e);
    cerr << Geodesic(p1,p2) << endl;
  }
  {
    Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(6378137.,.8));
    GeodeticCoordinates p1=GeodeticCoordinates::fromDegrees(10,0,0,e);
    GeodeticCoordinates p2=GeodeticCoordinates::fromDegrees(10,170,0,e);
    cerr << Geodesic(p1,p2) << endl;
  }
  {
    Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(6378137.,1.0/300));
    GeodeticCoordinates p1=GeodeticCoordinates::fromDegrees(10,0,0,e);
    GeodeticCoordinates p2=GeodeticCoordinates::fromDegrees(10,170,0,e);
    cerr << Geodesic(p1,p2) << endl;
  }
#endif
}


void utest_4()
{
  cerr << setiosflags(ios::fixed) << setprecision(10);
  cerr << "82.5 - PI/2 = " << abs((abs(toRadians(-82.5)) - M_PI/2.0)) << endl;
  for (double lat1=-89.99;lat1<=89.99;lat1 += 9.5) {
    cerr << "LAT1 MAX DIFF : " << lat1 << endl;
    for (double lon1=-180;lon1<=180; lon1 +=31) {
      GeodeticCoordinates p1=GeodeticCoordinates::fromDegrees(lat1,lon1);
      for (double lat2=-89.99;lat2<=89.99;lat2 += 11.5) {
	//	cerr << "LAT2 MAX DIFF : " << lat2 << endl;
	for (double lon2=-180;lon2<=180; lon2 +=17) {
	  GeodeticCoordinates p2=GeodeticCoordinates::fromDegrees(lat2,lon2);
	  const Geodesic rb12 (p1,p2);
	  const Geodesic rb21 (p2,p1);
	  assert (abs(rb12.range()-rb21.range()) < .002);
	  
	  const GeodeticCoordinates q1 = rb21.move(p2);
	  const GeodeticCoordinates q2 = rb12.move(p1);

	  double absdiff = max(2.,rb12.range()/120000.);
	  checkCoords(p1,q1,absdiff);
	  checkCoords(p2,q2,absdiff);
	}
      }
    }
  }
  
}

void utest_5()
{
  const Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(1000000,10E-10,0,0,0));
  const Reference<GeodeticDatum> s=GeodeticDatum::create(Spheroid::createSphere(e->spheroid().majorAxisLength(),0,0,0));
  
  GeodeticCoordinates pe=GeodeticCoordinates::fromDegrees(0,0,0,e);
  GeodeticCoordinates ps=GeodeticCoordinates::fromDegrees(0,0,0,s);
  cout << setiosflags(ios::fixed) << setprecision(4) << setw(12);
  for (double lon = -178;lon<180;lon += 2) {
    GeodeticCoordinates qe=GeodeticCoordinates::fromDegrees(45,lon,0,e);
    GeodeticCoordinates qs=GeodeticCoordinates::fromDegrees(45,lon,0,s);
    Geodesic s12(ps,qs);
    Geodesic e12(pe,qe);
    assertEquals(e12.range(),s12.range(),.1);
    if (abs(e12.range())>.001) {
      assertSameAngle(e12.bearing(),s12.bearing(),toRadians(.25));
    }
    Geodesic s21(qs,ps);
    Geodesic e21(qe,pe);
    assertEquals(e21.range(),s21.range(),.1);
    assertEquals(e12.range(),e21.range(),.1);
    if (abs(e21.range())>.001) {
      assertSameAngle(e21.bearing(),s21.bearing(),toRadians(.25));
    }
  }
}

void utest_6()
{
  cerr << "Test 6" << endl;
  verifyDistance("30 0 0 N 0 0 0 E","30 0 0 S 30 0 0 E",7362630.0557);
  verifyDistance("90 0 0 N 0 0 0 E","30 0 0 S 30 0 0 E",13322079.1271);
  verifyDistance("90 0 0 N 0 0 0 E","30 0 0 S 180 0 0 E",13322079.1271);
  verifyDistance("80 0 0 N 0 0 0 E","30 0 0 S 30 0 0 E",12343148.4628);
  verifyDistance("0  0 0 N 0 0 0 E"," 0 0 0 S 30 0 0 E",3339584.7238);
  verifyDistance("30 0 0 N 100 0 0 E","30 0 0 S 100 0 0 W",18101709.5872);
  verifyDistance("90 0 0 N 100 0 0 E","90 0 0 S 100 0 0 W",20003931.4585);
  verifyDistance("30 0 0 N 180 0 0 E","30 0 0 N 180 0 0 W",0);
  cerr << endl;
}

void utest_7()
{
  const Reference<GeodeticDatum> S=GeodeticDatum::create(Spheroid::createSphere(Spheroid::create(Spheroid::WGS_84)->majorAxisLength(),0,0,0));
  GeodeticCoordinates a = GeodeticCoordinates::fromDegrees(10,0,S);
  GeodeticCoordinates b = GeodeticCoordinates::fromDegrees(10,10,S);
  
  Geodesic G(a,b);
  G = Geodesic(G.range()/2,G.bearing());
  GeodeticCoordinates bPrime = G.move(a);
  Geodesic H(a,bPrime);
  cerr << "a="<< a << "\nb="<<b<<"\nb'="<<bPrime<<"\nG="<<G<<"\nH="<<H<< endl;
  assert (abs(G.range()-H.range())/G.range() < .00025);

}

void utest_8()
{
  const Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(1000000,10E-10,0,0,0));
  const Reference<GeodeticDatum> s=GeodeticDatum::create(Spheroid::createSphere(e->spheroid().majorAxisLength(),0,0,0));

  GeodeticCoordinates pe=GeodeticCoordinates::fromDegrees(0,-170,0,e);
  GeodeticCoordinates ps=GeodeticCoordinates::fromDegrees(0,-170,0,s);
  GeodeticCoordinates qe=GeodeticCoordinates::fromDegrees(45,170,0,e);
  GeodeticCoordinates qs=GeodeticCoordinates::fromDegrees(45,170,0,s);

  Geodesic s12(ps,qs);
  Geodesic e12(pe,qe);
  Geodesic s21(qs,ps);
  Geodesic e21(qe,pe);
  assert(s12.range()>0);
  assert(e12.range()>0);
  assert(s21.range()>0);
  assert(e21.range()>0);

  GeodeticCoordinates xpe=GeodeticCoordinates::fromDegrees(0,0,0,e);
  GeodeticCoordinates xps=GeodeticCoordinates::fromDegrees(0,0,0,s);
  GeodeticCoordinates xqe=GeodeticCoordinates::fromDegrees(45,20,0,e);
  GeodeticCoordinates xqs=GeodeticCoordinates::fromDegrees(45,20,0,s);
  Geodesic xs12(ps,qs);
  Geodesic xe12(pe,qe);
  Geodesic xs21(qs,ps);
  Geodesic xe21(qe,pe);
  
  assertEquals(xs12.range(),s12.range(),.1);
  assertEquals(xe12.range(),e12.range(),.1);
  assertEquals(xs21.range(),s21.range(),.1);
  assertEquals(xe21.range(),e21.range(),.1);

  assertSameAngle(xs12.bearing(),s12.bearing(),.001);
  assertSameAngle(xe12.bearing(),e12.bearing(),.001);
  assertSameAngle(xs21.bearing(),s21.bearing(),.001);
  assertSameAngle(xe21.bearing(),e21.bearing(),.001);
}

void utest_9()
{
  const Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(1000000,10E-10,0,0,0));
  const Reference<GeodeticDatum> s=GeodeticDatum::create(Spheroid::createSphere(e->spheroid().majorAxisLength(),0,0,0));

  GeodeticCoordinates pe=GeodeticCoordinates::fromDegrees(0,0,0,e);
  GeodeticCoordinates ps=GeodeticCoordinates::fromDegrees(0,0,0,s);
  
  GeodeticCoordinates qe=GeodeticCoordinates::fromDegrees(45,0,0,e);
  GeodeticCoordinates qs=GeodeticCoordinates::fromDegrees(45,0,0,s);

  Geodesic s12(ps,qs);
  Geodesic e12(pe,qe);
  Geodesic s21(qs,ps);
  Geodesic e21(qe,pe);
  assert(s12.range()>0);
  assert(e12.range()>0);
  assert(s21.range()>0);
  assert(e21.range()>0);
}

