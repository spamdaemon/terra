#include <iostream>
#include <cassert>
#include <cmath>

#include <terra/UnsupportedCRS.h>
#include <terra/CoordinateReferenceSystem.h>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;

void utest_throw1()
{
   Reference< CoordinateReferenceSystem> crs1 = CoordinateReferenceSystem::getEPSG3395();
   Reference< CoordinateReferenceSystem> crs2 = CoordinateReferenceSystem::getEPSG4326();

   try {
      if (!crs1->equals(*crs2)) {
         throw UnsupportedCRS(crs1, crs2);
      }
      assert(false);
   }
   catch (const UnsupportedCRS& e) {
      assert(e.cause() == crs1);
      assert(e.supportedCRS().size() == 1);
      assert(e.supportedCRS()[0] == crs2);
   }
}
