#include <iostream>
#include <terra/Angle.h>
#include <terra/ECEFCoordinates.h>
#include <terra/TopocentricCoordinates.h>
#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;

static double normalizeAngleToDegrees(double rad)
{
  double deg = fmod(toDegrees(rad),360);
  if (deg < 0) {
    deg += 360;
  }
  return deg;
}

void utest_1()
{
  GeodeticCoordinates orig = GeodeticCoordinates::fromDegrees(0,0,10);
  {
    TopocentricCoordinates tcs(orig);
    assert(tcs.z()==0);
    
    ::std::cerr << "Origin at " << tcs.geodeticCoordinates() << ::std::endl;
    assert(orig.approximateDistance(tcs.geodeticCoordinates()) < 10);
  }

  {
    
    TopocentricCoordinates tcs(orig);
    TopocentricCoordinates pt(tcs.frame(), GeodeticCoordinates::fromDegrees(0,1,10));
    ::std::cerr << __LINE__ << ":" << pt << ::std::endl;
    assert(pt.x()>0);
    // distance is only valid at the equator!!!
    assert(pt.distance(tcs) < 112000 && pt.distance(tcs) > 111000);

    ::std::cerr << "TCS  distance : " << pt.distance(tcs)  << ::std::endl;
    ::std::cerr << "True distance : " << GeodeticCoordinates::fromDegrees(0,1,10).approximateDistance(orig) << ::std::endl;
  }
  {
    TopocentricCoordinates tcs(orig);
    TopocentricCoordinates pt(tcs.frame(), GeodeticCoordinates::fromDegrees(0,-1,10));
    ::std::cerr << __LINE__ << ":" << pt << ::std::endl;
    assert(pt.x()<0);
  }
  
  {
    TopocentricCoordinates tcs(GeodeticCoordinates::fromDegrees(0,179,10));
    TopocentricCoordinates pt(tcs.frame(), GeodeticCoordinates::fromDegrees(0,-179,10));
    ::std::cerr << __LINE__ << ":" << pt << ::std::endl;
    assert(pt.x()>0);
  }
  {
    TopocentricCoordinates tcs(GeodeticCoordinates::fromDegrees(0,-179,10));
    TopocentricCoordinates pt(tcs.frame(), GeodeticCoordinates::fromDegrees(0,179,10));
    ::std::cerr << __LINE__ << ":" << pt << ::std::endl;
    assert(pt.x()<0);
  }
}

void utest_2()
{
  GeodeticCoordinates ctr=GeodeticCoordinates::fromDegrees(33.5,43.5);
  TopocentricCoordinates tcs(ctr);
  tcs = TopocentricCoordinates(tcs.frame(),-10000,-10000,0);
  GeodeticCoordinates geo = tcs.geodeticCoordinates();

  ::std::cerr << geo << ::std::endl;
  assert(toDegrees(ctr.latitude()-geo.latitude())<.15);
  assert(toDegrees(::std::abs(ctr.longitude()-geo.longitude()))<.15);
}

void utest_3()
{
  GeodeticCoordinates ctr(0,0);
  
  Reference<TopocentricFrame> frame = TopocentricFrame::create(ctr);
  TopocentricCoordinates north(frame,0,1000,0);
  TopocentricCoordinates east(frame,1000,0,0);
  TopocentricCoordinates south(frame,0,-1000,0);
  TopocentricCoordinates west(frame,-1000,0,0);

  assert(::std::abs(normalizeAngleToDegrees(north.azimuth())-0) < 0.001);
  assert(::std::abs(normalizeAngleToDegrees(east.azimuth())-90) < 0.001);
  assert(::std::abs(normalizeAngleToDegrees(west.azimuth())-270) < 0.001);
  assert(::std::abs(normalizeAngleToDegrees(south.azimuth())-180) < 0.001);
}

void utest_5()
{
    GeodeticCoordinates ctr(0,0);
    ECEFCoordinates pot = ctr.ecefCoordinates();
    ECEFCoordinates up = ECEFCoordinates(pot.x()+1,pot.y(),pot.z());
    Reference<TopocentricFrame> frame = TopocentricFrame::create(ctr);
    TopocentricCoordinates tcs(frame,up);
    ::std::cerr << "TCS : " << tcs << ::std::endl;
    assert(::std::abs(tcs.x()-0) < .001);
    assert(::std::abs(tcs.y()-0) < .001);
    assert(::std::abs(tcs.z()-1) < .001);
}

void utest_rotateToECEF()
{
  {
    GeodeticCoordinates ctr(0,0);
    Reference<TopocentricFrame> frame = TopocentricFrame::create(ctr);
    ::newton::Matrix<> toECEF = frame->getRotationToECEF();
    ::newton::Matrix<> fromECEF = frame->getRotationFromECEF();

    ::newton::Vector<> tcs(3),tcs2(3);
    ::newton::Vector<> ecef(3);
    tcs(0) = 0;
    tcs(1) = 0;
    tcs(2) = 1;
    ecef = toECEF*tcs;
    tcs2 = fromECEF*ecef;

    ::std::cerr << "I=" << fromECEF*toECEF << ::std::endl;
    ::std::cerr << "fromECEF " << fromECEF << ::std::endl;
    ::std::cerr << "toECEF   " << toECEF << ::std::endl;
    assert(::std::abs(ecef(0) - 1) < .001);
    assert(::std::abs(ecef(1) - 0) < .001);
    assert(::std::abs(ecef(2) - 0) < .001);

    ::std::cerr << "tcs2 " << tcs2 << ::std::endl;
    assert(::std::abs(tcs(0)-tcs2(0)) < 0.001);
    assert(::std::abs(tcs(1)-tcs2(1)) < 0.001);
    assert(::std::abs(tcs(2)-tcs2(2)) < 0.001);

    tcs(0) = 1;
    tcs(1) = 0;
    tcs(2) = 0;
    ecef = toECEF*tcs;
    tcs2 = fromECEF*ecef;

    assert(::std::abs(ecef(0) - 0) < .001);
    assert(::std::abs(ecef(1) - 1) < .001);
    assert(::std::abs(ecef(2) - 0) < .001);
    assert(::std::abs(tcs(0)-tcs2(0)) < 0.001);
    assert(::std::abs(tcs(1)-tcs2(1)) < 0.001);
    assert(::std::abs(tcs(2)-tcs2(2)) < 0.001);

    tcs(0) = 0;
    tcs(1) = 1;
    tcs(2) = 0;
    ecef = toECEF*tcs;
    tcs2 = fromECEF*ecef;

    assert(::std::abs(ecef(0) - 0) < .001);
    assert(::std::abs(ecef(1) - 0) < .001);
    assert(::std::abs(ecef(2) - 1) < .001);
    assert(::std::abs(tcs(0)-tcs2(0)) < 0.001);
    assert(::std::abs(tcs(1)-tcs2(1)) < 0.001);
    assert(::std::abs(tcs(2)-tcs2(2)) < 0.001);
  }

  {
    GeodeticCoordinates ctr = GeodeticCoordinates::fromDegrees(0,90);
    Reference<TopocentricFrame> frame = TopocentricFrame::create(ctr);
    ::newton::Matrix<> toECEF = frame->getRotationToECEF();
    ::newton::Matrix<> fromECEF = frame->getRotationFromECEF();

    ::newton::Vector<> tcs(3),tcs2(3);
    ::newton::Vector<> ecef(3);
    tcs(0) = 0;
    tcs(1) = 0;
    tcs(2) = 1;
    ecef = toECEF*tcs;
    tcs2 = fromECEF*ecef;

    assert(::std::abs(ecef(0) - 0) < .001);
    assert(::std::abs(ecef(1) - 1) < .001);
    assert(::std::abs(ecef(2) - 0) < .001);
    assert(::std::abs(tcs(0)-tcs2(0)) < 0.001);
    assert(::std::abs(tcs(1)-tcs2(1)) < 0.001);
    assert(::std::abs(tcs(2)-tcs2(2)) < 0.001);

    tcs(0) = 1;
    tcs(1) = 0;
    tcs(2) = 0;
    ecef = toECEF*tcs;
    tcs2 = fromECEF*ecef;

    assert(::std::abs(ecef(0) - (-1)) < .001);
    assert(::std::abs(ecef(1) - 0) < .001);
    assert(::std::abs(ecef(2) - 0) < .001);
    assert(::std::abs(tcs(0)-tcs2(0)) < 0.001);
    assert(::std::abs(tcs(1)-tcs2(1)) < 0.001);
    assert(::std::abs(tcs(2)-tcs2(2)) < 0.001);

    tcs(0) = 0;
    tcs(1) = 1;
    tcs(2) = 0;
    ecef = toECEF*tcs;
    tcs2 = fromECEF*ecef;

    assert(::std::abs(ecef(0) - 0) < .001);
    assert(::std::abs(ecef(1) - 0) < .001);
    assert(::std::abs(ecef(2) - 1) < .001);
    assert(::std::abs(tcs(0)-tcs2(0)) < 0.001);
    assert(::std::abs(tcs(1)-tcs2(1)) < 0.001);
    assert(::std::abs(tcs(2)-tcs2(2)) < 0.001);
  }
}

void utest_northPole()
{
  TopocentricCoordinates eq(GeodeticCoordinates::fromDegrees(0,0,0));
  TopocentricCoordinates east(eq.frame(),GeodeticCoordinates::fromDegrees(0,90,0));
  TopocentricCoordinates north(eq.frame(),GeodeticCoordinates::fromDegrees(90,0,0));

  ECEFCoordinates ecefEq = eq.ecefCoordinates();
  ECEFCoordinates ecefEast = GeodeticCoordinates::fromDegrees(0,90,0).ecefCoordinates();
  ECEFCoordinates ecefNorth = GeodeticCoordinates::fromDegrees(90,0,0).ecefCoordinates();

  ::std::cerr << "EAST  : " << east << endl
	      << "NORTH : " << north << endl;
  assert(::std::abs(east.x()-ecefEast.y())<.01);
  assert(::std::abs(east.z()+ecefEq.x())<.01);

  assert(::std::abs(north.y()-ecefNorth.z())<.01);
  assert(::std::abs(north.z()+ecefEq.x())<.01);

}

void utest_elevation()
{
  TopocentricCoordinates tcs(0,1,1);
  assert(::std::abs(tcs.elevation() - PI/4) < .0001);
  tcs = TopocentricCoordinates(0,1,-1);
  assert(::std::abs(tcs.elevation() -  -PI/4) < .0001);

  tcs = TopocentricCoordinates().travelRangeBearingElevation(1,PI/2,PI);
  assert(::std::abs(tcs.x()- -1) < .0001);
  assert(::std::abs(tcs.y()- 0) < .0001);
  assert(::std::abs(tcs.z()- 0) < .0001);

  tcs = TopocentricCoordinates().travelRangeBearingElevation(1,PI/2,PI/2);
  assert(::std::abs(tcs.x()- 0) < .0001);
  assert(::std::abs(tcs.y()- 0) < .0001);
  assert(::std::abs(tcs.z()- 1) < .0001);

  tcs = TopocentricCoordinates().travelRangeBearingElevation(1,PI/2,-PI/2);
  assert(::std::abs(tcs.x()- 0) < .0001);
  assert(::std::abs(tcs.y()- 0) < .0001);
  assert(::std::abs(tcs.z()- -1) < .0001);

}
