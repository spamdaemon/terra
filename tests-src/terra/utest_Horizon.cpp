#include <terra/Horizon.h>
#include <terra/GeodeticCoordinates.h>

using namespace timber;
using namespace terra;


void utest_1()
{
  GeodeticCoordinates p = GeodeticCoordinates::fromDegrees(0,0,1000);

  Horizon H(p);

  ::std::cerr << "Distance to horizon : " << H.distance(0) <<  "  @  " << H.point(0) << ::std::endl;
}
void utest_2()
{
  GeodeticCoordinates p = GeodeticCoordinates::fromDegrees(0,0,1000);

  Horizon H(p);

  for (double phi = 0;phi < 360; phi += 60) {
    ::std::cerr << "Distance to horizon : " << H.distance(toRadians(phi)) << "  @   " << H.point(toRadians(phi)) << ::std::endl;
  }
}

void utest_3()
{
  Reference<GeodeticDatum> S = GeodeticDatum::create(Spheroid::create());
  S = GeodeticDatum::create(Spheroid::createSphere(.5 * (S->spheroid().majorAxisLength()+S->spheroid().minorAxisLength()),0,0,0));
  GeodeticCoordinates p = GeodeticCoordinates::fromDegrees(0,0,1000,S);

  Horizon H(p);

  for (double phi = 0;phi < 360; phi += 60) {
    ::std::cerr << "Distance to horizon : " << H.distance(toRadians(phi)) << "  @   " << H.point(toRadians(phi)) << ::std::endl;
  }
}

void utest_4()
{
  Reference<GeodeticDatum> S = GeodeticDatum::create(Spheroid::create());
  S = GeodeticDatum::create(Spheroid::createSphere(.5 * (S->spheroid().majorAxisLength()+S->spheroid().minorAxisLength()),0,0,0));


  for (double h = 0;h<10000; h += 100) {
    GeodeticCoordinates p = GeodeticCoordinates::fromDegrees(0,0,h,S);
    Horizon H(p);
    ::std::cerr << "Distance to horizon : " << H.distance(0) << "  @  " << H.point(0) << ::std::endl;
  }
}

