#include <terra/Bounds.h>
#include <terra/GeodeticCoordinates.h>
#include <iostream>

using namespace std;
using namespace timber;
using namespace terra;

static void printBounds (const Bounds& b)
{
  ::std::cerr << "Bounds " << b << ::std::endl;
}

static Int toIntDegrees(Double radians)
{
  Double x = toDegrees(radians);
  x += x>=0 ? .5 : -.5;
  return (Int)x;
}

static void verifyBounds (const Bounds& b, Int tlLat, Int tlLon, Int brLat, Int brLon)
{
  Int leftLat = toIntDegrees(b.northWest().latitude());
  Int leftLon = toIntDegrees(b.northWest().longitude());
  Int rightLat = toIntDegrees(b.southEast().latitude());
  Int rightLon = toIntDegrees(b.southEast().longitude());
  
  printBounds(b);

  assert (tlLat==leftLat);
  assert (tlLon==leftLon);
  assert (brLat==rightLat);
  assert (brLon==rightLon);
}

void utest_1()
{
  {
    GeodeticCoordinates topleft=GeodeticCoordinates::fromDegrees(10,180,10);
    GeodeticCoordinates bottomright=GeodeticCoordinates::fromDegrees(-10,-180);
    Bounds bounds(topleft,bottomright);
    assert(bounds.contains(GeodeticCoordinates::fromDegrees(0,180)));
  }
  {
    GeodeticCoordinates topleft=GeodeticCoordinates::fromDegrees(10,160);
    GeodeticCoordinates bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds bounds(topleft,bottomright);
    assert(bounds.contains(GeodeticCoordinates::fromDegrees(0,170)));
    assert(bounds.contains(GeodeticCoordinates::fromDegrees(0,-170)));
    assert(!bounds.contains(GeodeticCoordinates::fromDegrees(0,150)));
    assert(!bounds.contains(GeodeticCoordinates::fromDegrees(0,-150)));
  }

}

void utest_2()
{
  Bounds bounds;
  
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(0,180)));
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(0,-180)));
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(90,-180)));
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(-90,-180)));
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(90,180)));
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(-90,180)));
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(90,0)));
  assert(bounds.contains(GeodeticCoordinates::fromDegrees(-90,0)));
}

void utest_3()
{
  GeodeticCoordinates topleft,bottomright;
  {
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds B(topleft,bottomright);
    assert (A.intersects(B) && B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,150);
    bottomright=GeodeticCoordinates::fromDegrees(-10,159);
    Bounds B(topleft,bottomright);
    assert (!A.intersects(B) && !B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,150);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-150);
    Bounds B(topleft,bottomright);
    assert (A.intersects(B) && B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,150);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-150);
    Bounds B(topleft,bottomright);
    assert (A.intersects(B) && B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,0);
    bottomright=GeodeticCoordinates::fromDegrees(-10,60);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,80);
    bottomright=GeodeticCoordinates::fromDegrees(-10,150);
    Bounds B(topleft,bottomright);
    assert (!A.intersects(B) && !B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,0);
    bottomright=GeodeticCoordinates::fromDegrees(-10,60);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,30);
    bottomright=GeodeticCoordinates::fromDegrees(-10,150);
    Bounds B(topleft,bottomright);
    assert (A.intersects(B) && B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,0);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-10);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,30);
    bottomright=GeodeticCoordinates::fromDegrees(-10,150);
    Bounds B(topleft,bottomright);
    assert (A.intersects(B) && B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,170);
    bottomright=GeodeticCoordinates::fromDegrees(-10,175);
    Bounds B(topleft,bottomright);
    assert (A.intersects(B) && B.intersects(A));
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,-175);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-170);
    Bounds B(topleft,bottomright);
    assert (A.intersects(B) && B.intersects(A));
  }

}

void test4()
{
  GeodeticCoordinates topleft,bottomright;
  {
    topleft=GeodeticCoordinates::fromDegrees(10,-10);
    bottomright=GeodeticCoordinates::fromDegrees(-10,10);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,10);
    bottomright=GeodeticCoordinates::fromDegrees(-10,20);
    Bounds B(topleft,bottomright);
    assert (B.intersects(A) && A.intersects(B));
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,-10,-10,20);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,-10,-10,20);
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,170);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-170);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,-175);
    bottomright=GeodeticCoordinates::fromDegrees(-10,175);
    Bounds B(topleft,bottomright);
    assert (B.intersects(A) && A.intersects(B));
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,-180,-10,180);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,-180,-10,180);
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,170);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-170);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,175);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-175);
    Bounds B(topleft,bottomright);
    assert (B.intersects(A) && A.intersects(B));
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,170,-10,-170);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,170,-10,-170);
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,150);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-150);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,170);
    Bounds B(topleft,bottomright);
    assert (B.intersects(A) && A.intersects(B));
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,150,-10,-150);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,150,-10,-150);
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,150);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-150);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,-170);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds B(topleft,bottomright);
    assert (B.intersects(A) && A.intersects(B));
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,150,-10,-150);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,150,-10,-150);
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,170);
    bottomright=GeodeticCoordinates::fromDegrees(-10,180);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,-180);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-170);
    Bounds B(topleft,bottomright);
    assert (B.intersects(A) && A.intersects(B));
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,170,-10,-170);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,170,-10,-170);
  }

  
}

void test5()
{
  GeodeticCoordinates topleft,bottomright;
  {
    topleft=GeodeticCoordinates::fromDegrees(10,-10);
    bottomright=GeodeticCoordinates::fromDegrees(-10,10);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,20);
    bottomright=GeodeticCoordinates::fromDegrees(-10,30);
    Bounds B(topleft,bottomright);
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,-10,-10,30);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,-10,-10,30);
  }

  {
    topleft=GeodeticCoordinates::fromDegrees(10,160);
    bottomright=GeodeticCoordinates::fromDegrees(-10,170);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates::fromDegrees(10,-170);
    bottomright=GeodeticCoordinates::fromDegrees(-10,-160);
    Bounds B(topleft,bottomright);
    Bounds C = A.merge(B);
    printBounds(C);
    verifyBounds(C,10,160,-10,-160);
    Bounds D = B.merge(A);
    printBounds(D);
    verifyBounds(D,10,160,-10,-160);
  }
}

void test6()
{
  GeodeticCoordinates topleft,bottomright;
  {
    topleft=GeodeticCoordinates(PI/2,-PI);
    bottomright=GeodeticCoordinates(-PI/2,PI);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates(0,-PI);
    bottomright=GeodeticCoordinates(-PI/2,-PI/2);
    Bounds B(topleft,bottomright);
    Bounds C[3];
    Int n = A.intersect(B,C);
    assert(n==1);
    printBounds(C[0]);
    verifyBounds(C[0],0,-180,-90,-90);
  }
}

void test7()
{
  GeodeticCoordinates topleft,bottomright;
  {
    topleft=GeodeticCoordinates(PI/2,-PI);
    bottomright=GeodeticCoordinates(-PI/2,PI);
    Bounds A(topleft,bottomright);
    topleft=GeodeticCoordinates(PI/4,PI/2);
    bottomright=GeodeticCoordinates(-PI/4,-PI/2);
    Bounds B(topleft,bottomright);
    Bounds C[3];
    Int n = A.intersect(B,C);
    assert(n==1);
    printBounds(C[0]);
    verifyBounds(C[0],45,90,-45,-90);
  }
}

void test8()
{
  GeodeticCoordinates topleft,bottomright;
  {
    topleft=GeodeticCoordinates::fromDegrees(60,-140);
    bottomright=GeodeticCoordinates::fromDegrees(-60,-160);
    Bounds A(topleft,bottomright);

    topleft=GeodeticCoordinates::fromDegrees(45,130);
    bottomright=GeodeticCoordinates::fromDegrees(-45,-130);
    Bounds B(topleft,bottomright);
    Bounds C[3];
    Int n = A.intersect(B,C);
    assert(n==3);
    printBounds(C[0]);
    printBounds(C[1]);
    printBounds(C[2]);
  }
}
