#include <terra/terra.h>
#include <terra/Spheroid.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Loxodrome.h>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;
using namespace timber;
using namespace terra;

static void assertEquals(double expected, double actual, double err)
{
  cerr << "Equals : expected " << expected << ", was " << actual<< " |diff|="<< abs(expected-actual) << endl;

  assert(abs(expected-actual) <= err); 
}
static void assertSameAngle(double expected, double actual, double err)
{ 
  cerr << "SameAngle : expected " << toDegrees(expected) << ", was " << toDegrees(actual) << endl;

  assert(abs(sin(expected)-sin(actual)) <= err); 
  assert(abs(cos(expected)-cos(actual)) <= err); 
}

void utest_1()
{
  Reference<GeodeticDatum> e = GeodeticDatum::create(Spheroid::createEllipsoid(1000000.,10E-10,0,0,0));
  Reference<GeodeticDatum> s=GeodeticDatum::create(Spheroid::createSphere(e->spheroid().majorAxisLength(),0,0,0));

  GeodeticCoordinates pe=GeodeticCoordinates::fromDegrees(0,0,0,e);
  GeodeticCoordinates ps=GeodeticCoordinates::fromDegrees(0,0,0,s);
  
  GeodeticCoordinates qe=GeodeticCoordinates::fromDegrees(45,0,0,e);
  GeodeticCoordinates qs=GeodeticCoordinates::fromDegrees(45,0,0,s);

  Loxodrome s12(ps,qs);
  Loxodrome e12(pe,qe);
  Loxodrome s21(qs,ps);
  Loxodrome e21(qe,pe);
  assert(s12.range()>0);
  assert(e12.range()>0);
  assert(s21.range()>0);
  assert(e21.range()>0);
}

void utest_2()
{
  Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(1000000.,10E-10,0,0,0));
  Reference<GeodeticDatum> s=GeodeticDatum::create(Spheroid::createSphere(e->spheroid().majorAxisLength(),0,0,0));

  GeodeticCoordinates pe=GeodeticCoordinates::fromDegrees(0,-170,0,e);
  GeodeticCoordinates ps=GeodeticCoordinates::fromDegrees(0,-170,0,s);
  GeodeticCoordinates qe=GeodeticCoordinates::fromDegrees(45,170,0,e);
  GeodeticCoordinates qs=GeodeticCoordinates::fromDegrees(45,170,0,s);

  Loxodrome s12(ps,qs);
  Loxodrome e12(pe,qe);
  Loxodrome s21(qs,ps);
  Loxodrome e21(qe,pe);
  assert(s12.range()>0);
  assert(e12.range()>0);
  assert(s21.range()>0);
  assert(e21.range()>0);

  GeodeticCoordinates xpe=GeodeticCoordinates::fromDegrees(0,0,0,e);
  GeodeticCoordinates xps=GeodeticCoordinates::fromDegrees(0,0,0,s);
  GeodeticCoordinates xqe=GeodeticCoordinates::fromDegrees(45,20,0,e);
  GeodeticCoordinates xqs=GeodeticCoordinates::fromDegrees(45,20,0,s);
  Loxodrome xs12(ps,qs);
  Loxodrome xe12(pe,qe);
  Loxodrome xs21(qs,ps);
  Loxodrome xe21(qe,pe);
  
  assertEquals(xs12.range(),s12.range(),.1);
  assertEquals(xe12.range(),e12.range(),.1);
  assertEquals(xs21.range(),s21.range(),.1);
  assertEquals(xe21.range(),e21.range(),.1);

  assertSameAngle(xs12.bearing(),s12.bearing(),.001);
  assertSameAngle(xe12.bearing(),e12.bearing(),.001);
  assertSameAngle(xs21.bearing(),s21.bearing(),.001);
  assertSameAngle(xe21.bearing(),e21.bearing(),.001);
}

void utest_3()
{
  Reference<GeodeticDatum> e=GeodeticDatum::create(Spheroid::createEllipsoid(1000000.,10E-10,0,0,0));
  Reference<GeodeticDatum> s=GeodeticDatum::create(Spheroid::createSphere(e->spheroid().majorAxisLength(),0,0,0));
  
  GeodeticCoordinates pe=GeodeticCoordinates::fromDegrees(0,0,0,e);
  GeodeticCoordinates ps=GeodeticCoordinates::fromDegrees(0,0,0,s);
  cout << setiosflags(ios::fixed) << setprecision(4) << setw(12);
  for (double lon = -178;lon<180;lon += 2) {
    GeodeticCoordinates qe=GeodeticCoordinates::fromDegrees(45,lon,0,e);
    GeodeticCoordinates qs=GeodeticCoordinates::fromDegrees(45,lon,0,s);
    Loxodrome s12(ps,qs);
    Loxodrome e12(pe,qe);
    assertEquals(e12.range(),s12.range(),.1);
    if (abs(e12.range())>.001) {
      assertSameAngle(e12.bearing(),s12.bearing(),toRadians(.25));
    }
    Loxodrome s21(qs,ps);
    Loxodrome e21(qe,pe);
    assertEquals(e21.range(),s21.range(),.1);
    assertEquals(e12.range(),e21.range(),.1);
    if (abs(e21.range())>.001) {
      assertSameAngle(e21.bearing(),s21.bearing(),toRadians(.25));
      assertSameAngle(e12.bearing(),e21.bearing()+M_PI,toRadians(.001));
    }
  }
}
