#include <terra/Extent.h>
#include <terra/Bounds.h>
#include <terra/GeodeticCoordinates.h>
#include <iostream>

using namespace std;
using namespace timber;
using namespace terra;

static void printBounds (const Bounds& b)
{
  ::std::cerr << "Bounds " << b << ::std::endl;
}

static Int toIntDegrees(Double radians)
{
  Double x = toDegrees(radians);
  x += x>=0 ? .5 : -.5;
  return (Int)x;
}

static void verifyBounds (const Bounds& b, Int tlLat, Int tlLon, Int brLat, Int brLon)
{
  Int leftLat = toIntDegrees(b.northWest().latitude());
  Int leftLon = toIntDegrees(b.northWest().longitude());
  Int rightLat = toIntDegrees(b.southEast().latitude());
  Int rightLon = toIntDegrees(b.southEast().longitude());
  
  printBounds(b);

  assert (tlLat==leftLat);
  assert (tlLon==leftLon);
  assert (brLat==rightLat);
  assert (brLon==rightLon);
}

void utest_1()
{
  Extent e(1,PI/2,1,PI/2+2*PI);
  Extent n(e);
  assert(e.shiftTowards(n));
}
