#include <iostream>
#include <cassert>
#include <terra/terra.h>
#include <cmath>

using namespace std;
using namespace terra;

void utest_clampLongitudeNoEps()
{
   {
      double lon = 3 * PI;
      double clon = clampLongitude(lon);
      cerr << "lon: " << lon << "   CLon: " << clon << endl;
      assert(::std::abs(clon - PI) < 1E-8 || (::std::abs(clon + PI) < 1E-8));
   }

   {
      double lon = 1.5* PI;
      double clon = clampLongitude(lon);
      cerr << "lon: " << lon << "   CLon: " << clon << endl;
      assert((::std::abs(clon + PI/2) < 1E-8));
   }
}

void utest_clampLoingitudeWithEps()
{
   double eps = 0.5;
   {
      double lon = 3 * PI;
      double clon = clampLongitude(lon,eps);
      cerr << "lon: " << lon << "   CLon: " << clon << endl;
      assert(::std::abs(clon - PI) < 1E-8 || (::std::abs(clon + PI) < 1E-8));
   }

   {
      double lon = 1.5* PI;
      double clon = clampLongitude(lon,eps);
      cerr << "lon: " << lon << "   CLon: " << clon << endl;
      assert((::std::abs(clon + PI/2) < 1E-8));
   }

   {
      double lon = PI+eps/2;
      double clon = clampLongitude(lon,eps);
      cerr << "lon: " << lon << "   CLon: " << clon << endl;
      assert(::std::abs(clon - PI) < 1E-8);
   }

   {
      double lon = -PI-eps/2;
      double clon = clampLongitude(lon,eps);
      cerr << "lon: " << lon << "   CLon: " << clon << endl;
      assert((::std::abs(clon + PI) < 1E-8));
   }

}
