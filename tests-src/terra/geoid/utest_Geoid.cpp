#include <terra/geoid/Geoid.h>
#include <terra/terra.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;
using namespace ::terra::geoid;

static double egmTest[][3] = { 
  {  38.6281550,   269.7791550,     -31.628},
  { -14.6212170,   305.0211140,      -2.969},
  {  46.8743190,   102.4487290,     -43.575},
  { -23.6174460,   133.8747120,      15.871},
  {  38.6254730,   359.9995000,      50.066},
  {   -.4667440,      .0023000,      17.329},
  {  -1,-1,-1 }
};

static double hae(Reference<Geoid> geoid, double latDeg,double lonDeg)
{
  bool isValid;
  double h = geoid->heightAboveEllipsoid(toRadians(latDeg),toRadians(lonDeg), isValid);
  if (!isValid) {
    throw ::std::runtime_error("Could not compute height");
  }
  return h;
}


void utest_loadGeoid()
{
  ifstream in("data/WW15MGH.GRD");

  try {
    Reference<Geoid> geoid = Geoid::read(in);
    double hNorth = hae(geoid,89,0);
    double hSW = hae(geoid,51,10);
    double hNE = hae(geoid,52,11);
    double hMidPoint = hae(geoid,51.5,10.5);
    double hEq = hae(geoid,0,0);
    double N45E110 = hae(geoid,45.1,10.2);
    ::std::cerr <<"North: " << hNorth << ::std::endl;
    ::std::cerr <<"SW: " << hSW << ::std::endl;
    ::std::cerr <<"NE: " << hNE << ::std::endl;
    ::std::cerr <<"Mid: " << hMidPoint << ::std::endl;
    ::std::cerr <<"Equator: " << hEq << ::std::endl;
    ::std::cerr <<"45.1N10.2E: " << N45E110 << ::std::endl;

    assert(::std::abs(hSW-47.14) < .1);
    assert(::std::abs(hNE-44.31) < .1);
    assert(::std::abs(hMidPoint-45.85) < .1);
    assert(::std::abs(hNorth-15.45) < .1);
    assert(::std::abs(hEq-17.16) < .1);
    assert(::std::abs(N45E110-39.16) < 1);

    for (int i=0;true;++i) {
      double lat = egmTest[i][0];
      double lon = egmTest[i][1];
      double alt = egmTest[i][2];
      if (lat<0 && lon<0 && alt<0) {
	break;
      }
      double cAlt = hae(geoid,lat,lon);
      ::std::cerr << lat << ", " <<  lon << " : expected " << alt << ", got " << cAlt << ::std::endl;

      assert(::std::abs(alt-cAlt) < 1);
    }
  }
  catch (const exception& e) {
    cerr << "Exception " << e.what() << endl;
    throw;
  }
}

