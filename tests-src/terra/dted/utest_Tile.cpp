#include <terra/dted/Tile.h>
#include <terra/geoid/Geoid.h>
#include <terra/GeodeticDatum.h>
#include <terra/terra.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;
using namespace ::terra::dted;

static double hae(Reference<Tile> tile, double latDeg,double lonDeg)
{
  bool isValid;
  double h = tile->heightAboveEllipsoid(toRadians(latDeg),toRadians(lonDeg), isValid);
  if (!isValid) {
    throw ::std::runtime_error("Could not compute height");
  }
  return h;
}

void utest_loadTile()
{
  ifstream in("data/dted/e010n51.dt0");

  try {
    Reference<Tile> tile = Tile::read(in,HeightMap::createConstantHeightMap(GeodeticDatum::create(Spheroid::create(Spheroid::WGS_84)),0));
    double hSW = hae(tile,51,10);
    double hNE = hae(tile,52,11);
    double hMidPoint = hae(tile,51.5,10.5);

    ::std::cerr <<"SW: " << hSW << ::std::endl;
    ::std::cerr <<"NE: " << hNE << ::std::endl;
    ::std::cerr <<"Mid: " << hMidPoint << ::std::endl;

    assert(::std::abs(hSW-388) < 1);
    assert(::std::abs(hNE-103) < 1);
    assert(::std::abs(hMidPoint-309) < 1);
  }
  catch (const exception& e) {
    cerr << "Exception " << e.what() << endl;
    throw;
  }
}

void utest_loadTileWithGeoid()
{
  Pointer< ::terra::geoid::Geoid> geoid;
  try {
    ifstream in("data/WW15MGH.GRD");
    geoid = ::terra::geoid::Geoid::read(in);
  }
  catch (const exception& e) {
    cerr << "Exception " << e.what() << endl;
    throw;
  }

  ifstream in("data/dted/e010n51.dt0");

  try {
    Reference<Tile> tile = Tile::read(in,geoid);
  }
  catch (const exception& e) {
    cerr << "Exception " << e.what() << endl;
    throw;
  }
}

