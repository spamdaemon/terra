#include <terra/dted/Terrain.h>
#include <terra/HeightMap.h>
#include <terra/GeodeticDatum.h>
#include <iostream>
#include <cmath>

using namespace ::std;
using namespace ::timber;
using namespace ::terra;
using namespace ::terra::dted;

static double hae(Reference<Terrain> tile, double latDeg,double lonDeg)
{
  bool isValid;
  double h = tile->heightAboveEllipsoid(toRadians(latDeg),toRadians(lonDeg), isValid);
  if (!isValid) {
    throw ::std::runtime_error("Could not compute height");
  }
  return h;
}
void utest_Terrain()
{
  try {
    Reference<GeodeticDatum> S = GeodeticDatum::create(Spheroid::create(Spheroid::WGS_84));
    Reference<HeightMap> geoid = HeightMap::createConstantHeightMap(S,0);
    Pointer<Terrain> t;
    try { 
      // load a terrain with bounds that are a little bit bigger than the 
      // exact numbers in degrees
       t = Terrain::create(Bounds::fromDegrees(29.9,-112.1,52.1,10.1,S),geoid,"/usr/local/data/dted");
       if(!t) {
         throw ::std::runtime_error("Could not load terrain"); 
       }
    }
    catch (...) {
      // probably could not load terrain
      ::std::cerr << "Could not load terrain from \"/usr/local/data/dted\"" << ::std::endl;
      return ;
    }

    double hSW = hae(t,51,10);
    ::std::cerr <<"SW: " << hSW << ::std::endl;
    double hNE = hae(t,52,11);
    ::std::cerr <<"NE: " << hNE << ::std::endl;
    double hMidPoint = hae(t,51.5,10.5);
    ::std::cerr <<"Mid: " << hMidPoint << ::std::endl;

    assert(::std::abs(hSW-388) < 1);
    assert(::std::abs(hNE-103) < 1);
    assert(::std::abs(hMidPoint-309) < 1);
    
    assert(hae(t,32,-110)>100);
  }
  catch (const exception& e) {
    ::std::cerr << "Exception " << e.what() << ::std::endl;
    throw;
  }
  
}
