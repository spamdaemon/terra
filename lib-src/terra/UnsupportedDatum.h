#ifndef _TERRA_UNSUPPORTEDDATUM_H
#define _TERRA_UNSUPPORTEDDATUM_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TERRA_DATUM_H
#include <terra/Datum.h>
#endif

#include <stdexcept>
#include <vector>

namespace terra {

   /**
    * This exception can be used to indicate that a specific Datum is not supported.
    * Alternative datums that are supported may be provided via this exception.
    */
   class UnsupportedDatum : public ::std::runtime_error
   {
         /**
          * Create an unsupported CRS exception.
          * @param datum the datum that is not supported
          * @param supported the different CRS's supported (may be empty)
          */
      public:
         UnsupportedDatum(const ::timber::Reference< Datum>& datum,
               ::std::vector< ::timber::Reference< Datum> >& supported);

         /**
          * Create an unsupported CRS exception and provide an alternative CRS that is supported.
          * @param datum the datum that is not supported
          * @param supported a supported CRS
          */
      public:
         UnsupportedDatum(const ::timber::Reference< Datum>& datum, const ::timber::Reference< Datum>& supported);

         /**
          * Create an unsupported CRS exception and provide an alternative CRS that is supported.
          * @param datum the datum that is not supported
          */
      public:
         UnsupportedDatum(const ::timber::Reference< Datum>& datum);

         /** Destructor */
      public:
         ~UnsupportedDatum() throw();

         /**
          * Get the CRS that caused this exception.
          * @return the CRS that is unsupported by the object that has thrown this exception
          */
      public:
         inline const ::timber::Reference< Datum>& cause() const throw()
         {
            return _unsupported;
         }

         /**
          * Get a CRS that are supported. The list of reference systems returned in the vector is not
          * necessarily an exhaustive list.
          * @return a vector of supported CRS's
          */
      public:
         inline const ::std::vector< ::timber::Reference< Datum> >& supportedCRS() const throw()
         {
            return _supported;
         }

         /** The datum that is not supported */
      private:
         ::timber::Reference< Datum> _unsupported;

         /** The datum that are supported */
      private:
         ::std::vector< ::timber::Reference< Datum> > _supported;
   };

}

#endif
