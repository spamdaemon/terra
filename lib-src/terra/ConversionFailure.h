#ifndef _TERRA_CONVERSIONFAILURE_H
#define _TERRA_CONVERSIONFAILURE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace terra {

   /**
    * This exception is thrown when some kind of conversion fails.
    */
   class ConversionFailure : public ::std::runtime_error
   {
         /**
          * Default constructor.
          */
      public:
         ConversionFailure();

         /** Destructor */
      public:
         ~ConversionFailure() throw();
   };

}

#endif
