#ifndef _TERRA_LATLON_H
#define _TERRA_LATLON_H

namespace terra {
  
  /**
   * A latitude longitude pair in degree or radians.
   */
  typedef double LatLon[2];
}

#endif
