#ifndef _TERRA_SPHEROID_H
#define _TERRA_SPHEROID_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace terra {
   /**
    * Instances of this class define the various spheroids
    * used to approximate the true geoid. According to the
    * <a href="www.usgs.gov">U.S. Geological Survey</a>, a
    * spheroid is:
    * <p><center>
    * Mathematical figure closely approaching the geoid in
    * form and size and used as a surface of reference for
    * geodetic surveys. A reference spheroid or ellipsoid
    * is a spheroid determined by revolving an ellipse about
    * its shorter (polar) axis and used as a base for geodetic
    * surveys of a large section of the Earth (such as the Clarke
    * spheroid of 1866 which is used for geodetic surveys in the
    * United States).
    * </center></p>
    * When specifying a spheroid, 3 parameters are required:
    * <ol>
    * <li>The major axis radius, which is normally the radius of Earth at the Equator
    * <li>The flattening value which is used to determine the minor axis length as <tt>major*(1-flattening)<tt>.
    * <li>The offset of the spheroid from the WGS84 spheroid in cartesian coordinates. This offset
    * is defined as the vector from the center of the WGS84 spheroid to the center this spheroid.
    * </ol>
    */
   class Spheroid : public ::timber::Counted
   {
         /**
          * A default set of spheroids that are available. Most
          * reference spheroids contain the year at which the
          * spheroid was established.
          */
      public:
         enum ReferenceSpheroid
         {
            AIRY_1830,
            BESSEL_1841,
            CLARKE_1866,
            CLARKE_1880,
            EVEREST_1830,
            FISCHER_1960,
            FISCHER_1968,
            GRS_1967,
            GRS_1975,
            GRS_1980,
            HOUGH_1956,
            INTERNATIONAL,
            KRASSOVSKY_1940,
            SOUTH_AMERICAN_1969,
            WGS_60,
            WGS_66,
            WGS_72,
            WGS_84
         };

         /**
          * Default constructor
          */
      protected:
         Spheroid()throws();

         /** Destructor */
      public:
         ~Spheroid()throws();

         /**
          * Set the default spheroid. Once called, the default
          * constructor will create an instance of e. Setting the
          * default spheroid is not a thread-safe operation.
          * @param e an ellisoid
          */
      public:
         static void setDefaultSpheroid(const ::timber::Reference< Spheroid>& e)throws();

         /**
          * Create the default spheroid.
          * @return a reference to a default spheroid
          */
      public:
         static ::timber::Reference< Spheroid> create()throws()
         {  return getEPSG7030();}

         /**
          * Create a specific spheroid.
          * @param re a reference spheroid
          * @return a spheroid
          */
      public:
         static ::timber::Reference< Spheroid> create(ReferenceSpheroid re)throws();

         /**
          * Create the EPSG7030 spheroid. This is the also the WGS84 spheroid.
          * @return the spheroid correspondin to EPSG7030
          */
      public:
         static ::timber::Reference< Spheroid> getEPSG7030()throws();

         /**
          * Create a custom spheroid. The size of the semi-major axis
          * is constrained sensical values, as is the flatting constant.
          * This should guard against accidentally using different units such
          * as kilometers, yards, or miles for the semi-major axis.
          * <p>
          * The flattening factor is used to control the eccentricity of the
          * spheroid. A flattening of 0 creates a sphere of radius a.
          * </p>
          * @param a the length of the semi-major axis in meters
          * @param flat the flatting factor
          * #param dx x-offset of the center relative to the WGS84 (EPSG:7030) spheroid
          * #param dy y-offset of the center relative to the WGS84 (EPSG:7030) spheroid
          * #param dz z-offset of the center relative to the WGS84 (EPSG:7030) spheroid
          * @pre REQUIRE_GREATER(a, 0.0)
          * @pre REQUIRE_GREATER_OR_EQUAL(flat,0.0);
          * @pre REQUIRE_LESS(flat,1.0);
          */
      public:
         static ::timber::Reference< Spheroid> createEllipsoid(double a, double flat, double dx, double dy, double dz)throws();

         /**
          * Create a custom spheroid that is a sphere. Use a real sphere
          * as a Spheroid enables use of simpler algorithms than on an
          * ellipsoid.
          * @param a radius of the spheroid
          * #param dx x-offset of the center relative to the WGS84 (EPSG:7030) spheroid
          * #param dy y-offset of the center relative to the WGS84 (EPSG:7030) spheroid
          * #param dz z-offset of the center relative to the WGS84 (EPSG:7030) spheroid
          * @pre REQUIRE_GREATER(a, 0.0)
          */
      public:
         static ::timber::Reference< Spheroid> createSphere(double a, double dx, double dy, double dz)throws();

         /**
          * Test if two spheroids are the same.
          * @param e an spheroid
          * @return true if e and this are the same spheroid
          */
      public:
         virtual bool equals(const Spheroid& e) const throws() = 0;

         /**
          * Convert a location to another spheroid.
          * @param toSpheroid the spheroid to which to convert
          * @param lat the latitude to be converted (in-out)
          * @param lon the longitude to be converted (in-out)
          * @param alt the altitude to be converted (in-out)
          */
      public:
         void convertGeodeticLocation(const Spheroid& toSpheroid, double& lat, double& lon, double& alt) const throws();

         /**
          * Convert a ecef coordinates to another spheroid.
          * @param toSpheroid the spheroid to which to convert
          * @param xyz the 3-cartesian coordinates (in-out)
          */
      public:
         void convertECEFLocation(const Spheroid& toSpheroid, double* xyz) const throws();

         /**
          * Convert the ecef coordinates to the reference ellipsoid.
          * @param xyz the xyz coordinates to be converted
          */
      public:
         virtual void ecefToWGS84(double* xyz) const throws() = 0;

         /**
          * Convert the ecef coordinates with respect to the reference ellipsoid to this ellipsoid.
          * @param xyz the xyz coordinates to be converted
          */
      public:
         virtual void wgs84ToECEF(double* xyz) const throws() = 0;


         /**
          * @name Accessing the various properties of the spheroid.
          * @{
          */

         /**
          * Get the size of the semi-major axis of this spheroid in meters.
          * This is essentially the radius of the Earth at the equator.
          * @return the size of the semi-major axis of this spheroid in meters
          */
      public:
         virtual double majorAxisLength() const throws() = 0;

         /**
          * Get the size of the semi-minor axis of this spheroid in meters.
          * This is essentially the radius of the Earth at the poles.
          * @return the size of the semi-minor axis of this spheroid in meters
          */
      public:
         virtual double minorAxisLength() const throws() = 0;

         /**
          * Get the flattening factor of this spheroid.
          * @return the flattening of this spheroid
          */
      public:
         virtual double flattening() const throws() = 0;

         /**
          * Get the offset of the center with respect to the WGS84 (EPSG:7030)
          * spheroid. This offset is defined as the vector from the center of WGS84
          * spoheroid to the center of this spheroid.
          * @param dx (out)
          * @param dy (out)
          * @param dz (out)
          */
      public:
         virtual void wgs84CenterOffset(double& dx, double& dy, double& dz) const throws() = 0;

         /**
          * Get the the eccentricity of this spheroid.
          * @return the eccentricity  of this spheroid
          */
      public:
         virtual double eccentricity() const throws() = 0;

         /**
          * Get the square of the eccentricity.
          * @return the square of the eccentricity
          */
      public:
         virtual double eccentricity2() const throws() = 0;

         /**
          * Get the the second eccentricity of this spheroid.
          * @return the second eccentricity  of this spheroid
          */
      public:
         virtual double secondEccentricity() const throws() = 0;

         /**
          * Get the square of the second eccentricity.
          * @return the square of the second eccentricity
          */
      public:
         virtual double secondEccentricity2() const throws() = 0;

         /*@}*/

         /**
          * @name Computations involving latitudes or longitudes (in radians!)
          * @{
          */

         /**
          * Compute the radius of curvature along a meridian.
          * @param lat a latitude in radians
          * @pre REQUIRE_RANGE(lat,-M_PI/2.0.0,M_PI/2.0)
          * @return the radius of curvature at the given latitude along a meridian
          */
      public:
         virtual double meridianRadius(double lat) const throws() = 0;

         /**
          * Compute the radius of curvature along the prime vertical.
          * @param lat a latitude in radians
          * @pre REQUIRE_RANGE(lat,-M_PI/2.0,M_PI/2.0)
          * @return the radius of curvature along the prime vertical
          */
      public:
         virtual double primeVerticalRadius(double lat) const throws() = 0;

         /**
          * Compute the radius of a circle parallel to the equator at some latitude.
          * @param lat a latitude value in radians
          * @pre REQUIRE_RANGE(lat,-M_PI/2.0,M_PI/2.0)
          * @return the radius of the circle at parallel to the equator
          */
      public:
         virtual double circleRadius(double lat) const throws() = 0;

         /**
          * Compute the mean radius of curvature at given latitude.
          * @param lat a latitude value in radians
          * @pre REQUIRE_RANGE(lat,-M_PI/2.0,M_PI/2.0)
          * @return return the mean radius of curvature
          */
      public:
         virtual double meanRadius(double lat) const throws() = 0;

         /**
          * Compute the arc length along a meridian from the equator to the given latitude.
          * The returned value is positive, regardless of the latitude. The result of a negative
          * latitude is <em>exactly</em> the same, that is, <code>meridianArcLength(x)==meridianArcLength(-x)</code>
          * @param lat a latitude value in radians
          * @pre REQUIRE_RANGE(lat,-M_PI/2.0,M_PI/2.0)
          * @return the signed distance in meters from the equator to the given latitude
          */
      public:
         virtual double meridianArcLength(double lat) const throws() = 0;

         /**
          * Compute the inverse of the meridian arc length. Given a distance from the
          * equator in meters, compute the latitude to which this value corresponds.
          * @param s a distance along a meridian to the equator
          * @return the latitude corresponding to given distance
          */
      public:
         virtual double inverseMeridianArcLength(double s) const throws() = 0;

         /**
          * Compute the arc length along a meridian from lat1 to lat2. If
          * lat1<lat2, then the arc length will be positive, otherwise it will
          * be negative.
          * @param lat1 a latitude value in radians
          * @param lat2 a latitude value in radians
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.0,M_PI/2.0)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.0,M_PI/2.0)
          * @return the distance in meters between two latitudes
          */
      public:
         virtual double meridianArcLength(double lat1, double lat2) const throws() = 0;

         /**
          * Compute the arc length along a given latitude.
          * @param lat the latitude at which to compute the arc length (radians)
          * @param lonDiff the number of longitudes that define the arc length (radians)
          * @pre REQUIRE_RANGE(lat,-M_PI/2.0,M_PI/2.0)
          * @pre REQUIRE_RANGE(lonDiff,.0,2.0*M_PI)
          * @return the distance in meters traveling lon degree radians at the given latitude
          */
      public:
         virtual double parallelArcLength(double lat, double lonDiff) const throws() = 0;

         /**
          * Compute the area of a rectangle extending from the equator to the
          * given latitude and being one degree radians wide.
          * The returned value is positive, regardless of the latitude. The
          * result of a negative
          * latitude is <em>exactly</em> the same, that is, <code>area(x)==area(-x)</code>
          * @param lat the latitude at which to compute the area (radians)
          * @pre REQUIRE_RANGE(lat,-M_PI/2.0,M_PI/2.0)
          * @return the area of a rectangular region in square meters
          */
      public:
         virtual double area(double lat) const throws() = 0;

         /**
          * Compute the area of a rectangle between two latitudes and being
          * one degree radians wide.
          * @param lat1 the latitude at which to compute the area (radians)
          * @param lat2 the latitude at which to compute the area (radians)
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.,M_PI/2.)
          * @return the area of a rectangular region in square meters between the two latitudes
          */
      public:
         virtual double area(double lat1, double lat2) const throws() = 0;

         /**
          * Compute the geocentric latitude
          * @param lat a latitude
          */
      public:
         virtual double geocentricLatitude(double lat) const throws() = 0;

         /**
          * Convert a geodetic latitude to a paramtric latitude.
          * If this spheroid is a sphere, then the argument is returned. The
          * parametric latitude is also known as the <em>reduced latitude</em>.
          * @param lat a geodetic latitude
          * @return a parametric latitude
          */
      public:
         virtual double parametricLatitude(double lat) const throws() = 0;

         /**
          * Convert a geodetic latitude to an isometric latitude.
          * @param lat a latitude
          * @return an isometric latitude
          */
      public:
         virtual double isometricLatitude(double lat) const throws() = 0;

         /**
          * Compute an approximation to the shortest distance between two points
          * given as latitude and longitude. Any distance along a meridian
          * (lon1==lon2) will most likely differ from the result obtained
          * from <code>meridianArcLength()</code>.
          * @param lat1 the latitude of the first point in radians
          * @param lon1 the longitude of the first point in radians
          * @param lat2 the latitude of the seconds point in radians
          * @param lon2 the longitude of the second point in radians
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lon1,-M_PI,M_PI)
          * @pre REQUIRE_RANGE(lon2,-M_PI,M_PI)
          * @return the shortest distance between the two points on the spheroid
          */
      public:
         virtual double computeApproximateDistance(double lat1, double lon1, double lat2, double lon2) const throws() = 0;

         /**
          * Travel along the specified meridian. Travel along the meridian m
          * for a distance of d meters. If the starting location is not on one of the poles
          * then m must be the same longitude as the starting points longitude lon1.
          * @param lat1 the start latitude (in radians)
          * @param lon1 the start longitude (in radians)
          * @param m the meridian longitude (in radians)
          * @param d the signed distance
          * @param lat2 the resulting latitude
          * @param lon2 the resulting longitude
          */
      public:
         virtual void
               travelAlongMeridianArc(double lat1, double lon1, double m, double d, double& lat2, double& lon2) const throws () = 0;

         /**
          * Compute the true course between locations.
          * @param lat1 the latitude of the first point in radians
          * @param lon1 the longitude of the first point in radians
          * @param lat2 the latitude of the seconds point in radians
          * @param lon2 the longitude of the second point in radians
          * @return true course from (lat1,lon1) to (lat2,lon2)
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lon1,-M_PI,M_PI)
          * @pre REQUIRE_RANGE(lon2,-M_PI,M_PI)
          */
      public:
         virtual double computeTrueCourse(double lat1, double lon1, double lat2, double lon2) const throws() = 0;

         /**
          * Compute the geodesic distance between two points given as
          * latitude and longitude. Any
          * distance along a meridian (lon1==lon2) will most likely differ
          * from the result obtained from <code>meridianArcLength()</code>.
          * @param lat1 the latitude of the first point in radians
          * @param lon1 the longitude of the first point in radians
          * @param lat2 the latitude of the seconds point in radians
          * @param lon2 the longitude of the second point in radians
          * @param az12 the bearing from point 1 to point 2
          * @param az21 the bearing from point 2 to point 1
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lon1,-M_PI,M_PI)
          * @pre REQUIRE_RANGE(lon2,-M_PI,M_PI)
          * @pre REQUIRE_FALSE(::terra::abs(lon1-lon2) == M_PI)
          * @return the shortest distance between the two points on the spheroid
          */
      public:
         virtual double
               computeGeodesic(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws() = 0;

         /**
          * Compute the coordinates of a point given relative to another point by following a geodesic.
          * If lat1 is at one of the poles, then the resulting coordinates are given by travelling
          * along the <em>az</em> meridian for a distance of d meters.
          * @param lat1 the latitude of point 1 (in radians)
          * @param lon1 the longitude of point 1 (in radians)
          * @param az the bearing to point 2 (in radians)
          * @param d the distance to point 2 (in meters)
          * @param lat2 the latitude of point 2  (in radians)
          * @param lon2 the longitude of point 2 (in radians)
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_GREATER_OR_EQUAL(d,0.0)
          */
      public:
         virtual void
               travelAlongGeodesic(double lat1, double lon1, double az, double d, double& lat2, double& lon2) const throws() =0;

         /**
          * Compute the shortest loxodrome between two points given as
          * latitude and longitude. Any
          * distance along a meridian (lon1==lon2) will most likely differ
          * from the result obtained from <code>meridianArcLength()</code>.
          * @param lat1 the latitude of the first point in radians
          * @param lon1 the longitude of the first point in radians
          * @param lat2 the latitude of the seconds point in radians
          * @param lon2 the longitude of the second point in radians
          * @param az12 the bearing from point 1 to point 2
          * @param az21 the bearing from point 2 to point 1
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lon1,-M_PI,M_PI)
          * @pre REQUIRE_RANGE(lon2,-M_PI,M_PI)
          * @pre REQUIRE_FALSE(::terra::abs(lon1-lon2) == M_PI)
          * @return the length of the shortest loxodrome between the two points on the spheroid
          */
      public:
         virtual double
               computeLoxodrome(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws() = 0;

         /**
          * Compute the coordinates of a point by travelling along a loxodrome.
          * If lat1 is at one of the poles, then the resulting coordinates are given by travelling
          * along the <em>az</em> meridian for a distance of d meters.
          * @param lat1 the latitude of point 1 (in radians)
          * @param lon1 the longitude of point 1 (in radians)
          * @param az the bearing to point 2 (in radians)
          * @param d the distance to point 2 (in meters)
          * @param lat2 the latitude of point 2  (in radians)
          * @param lon2 the longitude of point 2 (in radians)
          * @pre REQUIRE_RANGE(lat1,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lat2,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_GREATER_OR_EQUAL(d,0.0)
          */
      public:
         virtual void
               travelAlongLoxodrome(double lat1, double lon1, double az, double d, double& lat2, double& lon2) const throws() = 0;

         /*@}*/

         /**
          * Compute earth centred, earth fixed coordinates of a point in geodetic coordinates.
          * @param lat the latitude of the point (in radians)
          * @param lon the longitude of the point (in radians)
          * @param alt the altitude of the point abover the spheroid (in meters)
          * @param x the result will be stored as x,y,z in meters
          * @pre REQUIRE_RANGE(lat,-M_PI/2.,M_PI/2.)
          * @pre REQUIRE_RANGE(lon,-M_PI,M_PI)
          */
      public:
         virtual void computeECEF(double lat, double lon, double alt, double x[3]) const throws() = 0;

         /**
          * Compute geodetic coordinates of point in ECEF coordinates.
          * @param x the point in  earth centred, earth fixed coordinates (in meters)
          * @param lat the latitude of the equivalent geodetic point (in radians)
          * @param lon the longitud eof the equivalent geodetic point (in radians)
          * @param alt the altitude of the equivalent geodetic point above the spheroid (in meters)
          */
      public:
         virtual void computeGeodetic(const double x[3], double& lat, double& lon, double& alt) const throws() = 0;

         /**
          * Compute the mercator projection of a point in latitude and longitude
          * @param lon0 the central meridian
          * @param lat the latitude of a point (in radians)
          * @param lon the longitude of a point (in radians)
          * @param x the resulting x coordinate (in meters)
          * @param y the resulting y coordinate (in meters)
          */
      public:
         virtual void computeMercatorProjection(double lon0, double lat, double lon, double& x, double& y) const throws() = 0;

         /**
          * Compute the inverse mercator projection of a point.
          * @param lon0 the central meridian
          * @param x the resulting x coordinate (in meters)
          * @param y the resulting y coordinate (in meters)
          * @param lat the latitude of a point  (in radians)
          * @param lon the longitude of a point  (in radians)
          * @note the resulting latitude and longitude values may be slightly outside the valid range
          */
      public:
         virtual void computeInverseMercatorProjection(double lon0, double x, double y, double& lat, double& lon) const throws() = 0;

   };
}

#endif
