#include <terra/Location.h>
#include <cassert>
#include <iostream>
#include <iomanip>

using namespace ::std;
using namespace ::timber;

namespace terra {

   Location::Location()
   throws()
   : _coords(new double[3]),
   _crs(CoordinateReferenceSystem::getEPSG4979())
   {
      assert(_crs->coordinateCount()==3);
      ::std::fill(_coords,_coords+3,0);
   }

   Location::Location(const double* coords, const ::timber::Reference< CoordinateReferenceSystem>& xcrs)
   throws()
   : _coords(new double[xcrs->coordinateCount()]),
   _crs(xcrs)
   {
      ::std::copy(coords,coords+_crs->coordinateCount(),_coords);
   }

   Location::Location(const Location& src)
   throws()
   : _coords(new double[src._crs->coordinateCount()]),
   _crs(src._crs)
   {
      ::std::copy(src._coords,src._coords+src._crs->coordinateCount(),_coords);
   }

   Location::Location(double x1, double x2, const ::timber::Reference< CoordinateReferenceSystem>& xcrs)
   throws()
   : _coords(new double[2]),_crs(xcrs)
   {
      assert(xcrs->coordinateCount()==2);
      _coords[0] = x1;
      _coords[1] = x2;
   }

   Location::Location(double x1, double x2, double x3, const ::timber::Reference< CoordinateReferenceSystem>& xcrs)
   throws()
   : _coords(new double[3]),_crs(xcrs)
   {
      assert(xcrs->coordinateCount()==3);
      _coords[0] = x1;
      _coords[1] = x2;
      _coords[2] = x3;
   }

   Location& Location::operator=(const Location& src)
   throws()
   {
      if (_coords!=src._coords) {
         size_t sz = src.coordinateCount();
         if (sz!=coordinateCount()) {
            double* newCoords=new double[sz];
            delete[] _coords;
            _coords = newCoords;
         }
         ::std::copy(src._coords,src._coords+src._crs->coordinateCount(),_coords);
         _crs = src._crs;
      }
      return *this;
   }

   Location::Location(const ::timber::Reference< CoordinateReferenceSystem>& xcrs)
   throws()
   : _coords(new double[xcrs->coordinateCount()]),
   _crs(xcrs)
   {
      ::std::fill(_coords,_coords+xcrs->coordinateCount(),0);
   }

   Location::Location(const GeodeticCoordinates& coords)
   throws()
   : _coords(new double[3]),
   _crs(CoordinateReferenceSystem::getEPSG4979())
   {
      // we're expecting to have 3 coordinates
      assert(_crs->coordinateCount()==3);
      const CoordinateReferenceSystem::EPSG4979Coordinates wgs84 = {coords.latitude(),coords.longitude(),coords.height()};
      _crs->fromEPSG4979(wgs84,_coords);
   }

   Location::Location(const CoordinateReferenceSystem::EPSG4979Coordinates& epsg)
   throws()
   : _coords(new double[3]),
   _crs(CoordinateReferenceSystem::getEPSG4979())
   {
      _coords[0] = epsg.lat;
      _coords[1] = epsg.lon;
      _coords[2] = epsg.alt;
   }

   Location::~Location()
   throws()
   {
      delete[] _coords;
   }

   void Location::epsg4979Coordinates(CoordinateReferenceSystem::EPSG4979Coordinates& epsg) const
   throws()
   {
      _crs->toEPSG4979(_coords,epsg);
   }

   void Location::setEpsg4979Coordinates(const CoordinateReferenceSystem::EPSG4979Coordinates& epsg)
   throws()
   {
      if (_crs->coordinateCount()!=3) {
         double* nc = new double[3];
         delete[] _coords;
         _coords = nc;
      }

      _crs = CoordinateReferenceSystem::getEPSG4979();
      _coords[0] = epsg.lat;
      _coords[1] = epsg.lon;
      _coords[2] = epsg.alt;
   }
   Location Location::convert(const ::timber::Reference< CoordinateReferenceSystem>& xcrs) const
throws (ConversionFailure)
{
   Location res(*this);
   if (!res.setCRS(xcrs)) {
      throw ConversionFailure();
   }
   return res;
}

GeodeticCoordinates Location::epsg4979Coordinates() const
throws()
{
   CoordinateReferenceSystem::EPSG4979Coordinates wgs84;
   _crs->toEPSG4979(_coords,wgs84);
   return GeodeticCoordinates(wgs84.lat,wgs84.lon,wgs84.alt,GeodeticDatum::createWGS84());
}

double Location::distance(const Location& loc) const
throws()
{
   if (loc._crs->equals(*_crs)) {
      return _crs->distance(_coords,loc._coords);
   }
   Location tmp(loc);
   if(!tmp.setCRS(_crs)) {
      return -1;
   }
   return _crs->distance(_coords,tmp._coords);
}

bool Location::setPosition(const double* coords)
throws()
{
   // TODO: bounds need to be checked before copying
   ::std::copy(coords,coords+_crs->coordinateCount(),_coords);
   return true;
}

bool Location::setPosition(const GeodeticCoordinates& geo)
throws()
{
   // create a temporary point
   Location loc(geo);
   // change the coordinate system of the location to this location's crs
   if (loc.setCRS(_crs)) {
      // conversion was successful, copy the points
      ::std::copy(loc._coords, loc._coords + coordinateCount(), _coords);
      return true;
   }
   else {
      // could not change
      return false;
   }
}

bool Location::setPosition(const Location& loc)
throws()
{
   // if the coordinate systems are the same, then just copy the points
   // otherwise we need  to convert via the wgs84 coordinates.
   if (loc._crs->equals(*_crs)) {
      ::std::copy(loc._coords, loc._coords + coordinateCount(), _coords);
      return true;
   }
   else {
      return setPosition(loc.epsg4979Coordinates());
   }
}

bool Location::setCRS(const ::timber::Reference< CoordinateReferenceSystem>& newCrs)
throws()
{
   // if the coordinate systems are already equal, then don't bother
   if (_crs->equals(*newCrs)) {
      return true;
   }
   // get the coordinates in wgs84 coordinates, but consult the crs directly; we cannot
   // invoke some of the other methods as those invoke this method and would yield infinite
   // recursion
   Location loc(newCrs);
   CoordinateReferenceSystem::EPSG4979Coordinates epsg4979;
   _crs->toEPSG4979(_coords,epsg4979);
   if (newCrs->fromEPSG4979(epsg4979,loc._coords)) {
      ::std::copy(loc._coords, loc._coords + coordinateCount(), _coords);
      _crs = newCrs;
      return true;
   }
   else {
      return false;
   }
}

bool Location::equals(const Location& pt) const
throws()
{
   if (&pt!=this) {
      // if the crs are not equal then the locations are not considered
      // equal
      if (!pt._crs->equals(*_crs)) {
         return false;
      }
      // check all coordinates for equality
      for (size_t i=0,sz=coordinateCount();i<sz;++i) {
         if (_coords[i]!=pt._coords[i]) {
            return false;
         }
      }
   }
   return true;
}

bool Location::isSameLocation(const Location& pt) const
throws()
{
   return equals(pt) || epsg4979Coordinates().equals(pt.epsg4979Coordinates());
}

}

::std::ostream& operator<<(::std::ostream& out, const ::terra::Location& loc)
{
out << "Location [ ";
for (size_t i = 0, sz = loc.coordinateCount(); i != sz; ++i) {

   out << fixed << setw(12) << loc[i] << ' ';
}
return out << "] ";
}
