#include <terra/Spheroid.h>
#include <terra/terra.h>
#include <timber/logging.h>

#include <cmath>

using namespace ::timber;
using namespace ::timber::logging;
using namespace ::std;

namespace terra {
   namespace {
      static const double PI = 3.14159265358979323846;
      static const double EPS = 1E-10;
      static inline double sqr(double x)
      {
         return x * x;
      }

      // the ellipsoid definitions for each reference spheroid
      static const double SPHEROIDS[][5] = {
            { 6377563.396, 1.0 / 299.3249646, 0, 0, 0 }, { 6377397.155, 1.0 / 299.1528128, 0, 0, 0 }, { 6378206.4, 1.0
                  / 294.9786982, 0, 0, 0 }, { 6378249.145, 1.0 / 293.465, 0, 0, 0 }, {
                  6377276.345, 1.0 / 300.8017, 0, 0, 0 }, { 6378166, 1.0 / 298.3, 0, 0, 0 }, {
                  6378150, 1.0 / 298.3, 0, 0, 0 }, { 6378160, 1.0 / 298.247167427, 0, 0, 0 }, {
                  6378140, 1.0 / 298.257, 0, 0, 0 }, { 6378137, 1.0 / 298.257222101, 0, 0, 0 }, {
                  6378270, 1.0 / 297, 0, 0, 0 }, { 6378388, 1.0 / 297, 0, 0, 0 }, { 6378245, 1.0 / 298.3, 0, 0, 0 }, {
                  6378160, 1.0 / 298.25, 0, 0, 0 }, { 6378165, 1.0 / 298.3, 0, 0, 0 },
            { 6378145, 1.0 / 298.25, 0, 0, 0 }, { 6378135, 1.0 / 298.26, 0, 0, 0 }, {
                  6378137, 1.0 / 298.257223563, 0, 0, 0 } };

      static Reference< Spheroid> createEPSG7030Spheroid()
      {
         return Spheroid::createEllipsoid(6378137.0, 1.0 / 298.257223563, 0, 0, 0);
      }

      static inline double checkLatitude(double lat)
      {
         assert(lat >= -PI / 2);
         assert(lat <= PI / 2);
         return lat;
      }

      static inline double checkLongitude(double lon)
      {
         assert(lon >= -PI);
         assert(lon <= PI);
         return lon;
      }

      inline static double normalizeLongitude(double lon, double eps=1E-8)
      {
        return clampLongitude(lon,eps);
      }

      static inline double computeDeltaLon(double lon1, double lon2)
      {
         double dLon = lon2 - lon1;
         return normalizeLongitude(dLon,0);
      }

      /**
       * Clamp a cosine or sine value into the valid range.
       * @param v a value that should be between -1 and 1
       * @return a value between -1 and 1
       */
      static inline double clampUnitInterval(double v)
      {
         return v < -1 ? -1 : (v > 1 ? 1 : v);
      }

      struct Ellipsoid : public Spheroid
      {
            Ellipsoid(double semi, double flat, double dx, double dy, double dz)throws() :
            _major(semi), _flattening(flat)
            {
               _wgs84CenterOffset[0] = dx;
               _wgs84CenterOffset[1] = dy;
               _wgs84CenterOffset[2] = dz;

               _minor = semi * (1.0 - flat);
               assert(_minor <= _major);
               _eccentricity2 = flat * (2.0 - flat);
               _1_minus_eccentricity2 = 1.0 - _eccentricity2;
               _sqrt_of_1_minus_eccentricity2 = sqrt(1.0 - _eccentricity2);
               _eccentricity = sqrt(abs(_eccentricity2));
               _secondEccentricity2 = _eccentricity2 / (1. - _eccentricity2);
               _secondEccentricity = sqrt(_secondEccentricity2);

               // initialize the arc-length support
               const double e2 = _eccentricity2;
               const double e4 = e2 * e2;
               const double e6 = e2 * e4;
               const double e8 = e4 * e4;

               // divide here by the numbers, so we don't have to do it later
               _arcLengthSupport[0] = (1.0 + 3. / 4 * e2 + 45. / 64 * e4 + 175. / 256 * e6 + 11025. / 16384 * e8) / 1.;
               _arcLengthSupport[1] = (3. / 4 * e2 + 15. / 16 * e4 + 525. / 512 * e6 + 2205. / 2048 * e8) / 2.;
               _arcLengthSupport[2] = (15. / 64 * e4 + 105. / 256 * e6 + 2205. / 4096 * e8) / 4.;
               _arcLengthSupport[3] = (35. / 512 * e6 + 315. / 2048 * e8) / 6.;
               _arcLengthSupport[4] = (315. / 16384 * e8) / 8.;

               // mercator support
               _mercatorSupport[0] = (0.5 * e2 + 5.0 / 24. * e4 + 1. / 12. * e6 + 13. / 360. * e8);
               _mercatorSupport[1] = (7.0 / 48. * e4 + 29. / 240. * e6 + 811. / 11520. * e8);
               _mercatorSupport[2] = (7. / 120. * e6 + 81. / 1120. * e8);
               _mercatorSupport[3] = (4279. / 161280. * e8);

               // the rhumb line support

            }

            ~Ellipsoid()throws()
            {
            }

            double majorAxisLength() const throws()
            {
               return _major;
            }

            double minorAxisLength() const throws()
            {
               return _minor;
            }

            double flattening() const throws()
            {
               return _flattening;
            }

            double secondEccentricity() const throws()
            {
               return _secondEccentricity;
            }

            double secondEccentricity2() const throws()
            {
               return _secondEccentricity2;
            }

            double eccentricity() const throws()
            {
               return _eccentricity;
            }

            double eccentricity2() const throws()
            {
               return _eccentricity2;
            }

            void wgs84CenterOffset(double& x, double& y, double& z) const throws()
            {
               x = _wgs84CenterOffset[0];
               y = _wgs84CenterOffset[1];
               z = _wgs84CenterOffset[2];
            }

            void ecefToWGS84(double* xyz) const throws()
            {
               xyz[0] -= _wgs84CenterOffset[0];
               xyz[1] -= _wgs84CenterOffset[1];
               xyz[2] -= _wgs84CenterOffset[2];
            }

            void wgs84ToECEF(double* xyz) const throws()
            {
               xyz[0] += _wgs84CenterOffset[0];
               xyz[1] += _wgs84CenterOffset[1];
               xyz[2] += _wgs84CenterOffset[2];
            }

            bool equals(const Spheroid& e) const throws()
            {
               if (this==&e) {
                  return true;
               }
               bool same = _flattening == e.flattening() && _major == e.majorAxisLength();
               if (same) {
                  double a[3],b[3];
                  wgs84CenterOffset(a[0],a[1],a[2]);
                  wgs84CenterOffset(b[0],b[1],b[2]);
                  // not clear that we should not use epsilon based comparison, but
                  // I think this is ok since these offsets will never be so close as to
                  // differ by less than an epsilon
                  same = a[0]==b[0] && a[1]==b[1] && a[2]==b[2];
               }
               return same;
            }

            double meridianRadius(double lat) const throws()
            {
               double tmp = _eccentricity * sin(lat);
               tmp = abs(1.0 - tmp * tmp);
               tmp = sqrt(tmp * tmp * tmp);
               return _major * (_1_minus_eccentricity2) / tmp;
            }

            double primeVerticalRadius(double lat) const throws()
            {
               double tmp = _eccentricity * sin(lat);
               return _major / sqrt(abs(1.0 - tmp * tmp));
            }

            inline double circleRadius(double lat) const throws()
            {
               return primeVerticalRadius(lat) * cos(lat);
            }

            double meanRadius(double lat) const throws()
            {
               double tmp = _eccentricity * sin(lat);
               tmp = abs(1.0 - tmp * tmp);
               tmp = _major / tmp;
               return tmp * _sqrt_of_1_minus_eccentricity2;
            }

            double meridianArcLength(double lat) const throws()
            {
               double res = _arcLengthSupport[0] * lat;
               res -= _arcLengthSupport[1] * sin(2 * lat);
               res += _arcLengthSupport[2] * sin(4 * lat);
               res -= _arcLengthSupport[3] * sin(6 * lat);
               res += _arcLengthSupport[4] * sin(8 * lat);
               double dist = _major * (_1_minus_eccentricity2) * res;
               return dist;
            }

            double inverseMeridianArcLength(double s) const throws()
            {
               // equations taken from Map Projection Transformatin (pp.96)
               const double R = _major * _1_minus_eccentricity2 * _arcLengthSupport[0];
               const double B0 = s / R;
               const double s0 = meridianArcLength(B0);
               const double t = tan(B0);
               const double cosB2 = sqr(cos(B0));
               const double n2 = cosB2 * _secondEccentricity2;
               const double N = _major * 1.0 / sqrt(1 - _eccentricity2 * (1 - cosB2));

               const double ds = s - s0;
               const double T1 = (1 + n2) / N;
               const double T2 = (-3.0 * t * (n2 * n2 + n2)) / (N * N);
               const double T3 = 3 * n2 * ((t * t - 1) + n2 * (6 * t * t - 2)) / ((N * N) * N);

               const double B = B0 + ds * (T1 + ds * (0.5 * T2 + ds * T3 / 6));

               return B;
            }

            double meridianArcLength(double lat1, double lat2) const throws()
            {
               double res = _arcLengthSupport[0] * (lat2 - lat1);
               res -= _arcLengthSupport[1] * (sin(2 * lat2) - sin(2 * lat1));
               res += _arcLengthSupport[2] * (sin(4 * lat2) - sin(4 * lat1));
               res -= _arcLengthSupport[3] * (sin(6 * lat2) - sin(6 * lat1));
               res += _arcLengthSupport[4] * (sin(8 * lat2) - sin(8 * lat1));

               return res * _major * _1_minus_eccentricity2;
            }

            void computeMercatorProjection(double lon0, double lat, double lon, double& x, double& y) const throws()
            {
               lon = normalizeLongitude(lon - lon0);
               lon = ::std::max(::std::min(lon,PI),-PI);

               x = _major*lon;
               y = _major*isometricLatitude(lat);
            }

            void computeInverseMercatorProjection(double lon0, double x, double y, double& lat, double& lon) const throws()
            {
               lon = x/_major + lon0;
               const double chi = 2.0 * atan(exp(y/_major)) - 0.5 * PI;
               lat = chi + sin(2 * chi) * _mercatorSupport[0] + sin(4 * chi) * _mercatorSupport[1] + sin(6 * chi)
               * _mercatorSupport[2] + sin(8 * chi) * _mercatorSupport[3];
            }

            /**
             * Compute the minimum meridian arc length between two antipodal points.
             * If the signed distance between the two latitude is negative, then
             * the azimuth in which to travel from lat1 to lat2 is 180 degrees,
             * otherwise it's 0 degrees.
             * @param lat1 a latitude
             * @param lat2 a latitude
             * @return the signed distance between lat1 and lat2.
             */
            double antipodalArcLength(double lat1, double lat2) const throws()
            {
               double d1 = meridianArcLength(lat1, PI / 2.0) + meridianArcLength(lat2, PI / 2.0);
               double d2 = meridianArcLength(lat1, -PI / 2.0) + meridianArcLength(lat2, -PI / 2.0);
               return d1 <= -d2 ? d1 : d2;
            }

            double computeTrueCourse(double lat1, double lon1, double lat2, double lon2) const throws()
            {
               const double dLon = computeDeltaLon(lon1, lon2);

               lat1 = isometricLatitude(lat1);
               lat2 = isometricLatitude(lat2);
               return clampRadiansAngle(atan2(dLon, lat2 - lat1));
            }

            inline double parallelArcLength(double lat, double lonDiff) const throws()
            {
               checkLatitude(lat);
               assert(lonDiff >= -2 * PI);
               assert(lonDiff <= 2 * PI);
               return circleRadius(lat) * lonDiff;
            }

            double area(double lat) const throws()
            {
               lat = abs(lat);
               const double sinB = sin(lat);
               const double eSinB = _eccentricity * sinB;
               const double a = sinB / (2.0 - 2. * eSinB * eSinB);
               const double b = log((1. + eSinB) / (1. - eSinB)) / (4.0 * _eccentricity);
               double ar = _major * _major * (_1_minus_eccentricity2) * (a + b);
               assert(ar >= 0.);
               return ar;
            }

            double area(double lat1, double lat2) const throws()
            {
               const double sinB1 = sin(lat1);
               const double eSinB1 = _eccentricity * sinB1;
               const double a1 = sinB1 / (2.0 - 2. * eSinB1 * eSinB1);
               const double b1 = log((1. + eSinB1) / (1. - eSinB1));

               const double sinB2 = sin(lat2);
               const double eSinB2 = _eccentricity * sinB2;
               const double a2 = sinB2 / (2.0 - 2. * eSinB2 * eSinB2);
               const double b2 = log((1. + eSinB2) / (1. - eSinB2));

               const double a = a1 - a2;
               const double b = (b1 - b2) / (4.0 * _eccentricity);
               return _major * _major * (_1_minus_eccentricity2) * abs(a + b);
            }

            double geocentricLatitude(double lat) const throws()
            {
               return atan(_1_minus_eccentricity2 * tan(lat));
            }

            double parametricLatitude(double lat) const throws()
            {
               return atan(_sqrt_of_1_minus_eccentricity2 * tan(lat));
            }

            double isometricLatitude(double lat) const throws()
            {
               const double eSinB = _eccentricity * sin(lat);
               assert(eSinB >= -1.0 && eSinB <= 1.0);
               const double tan_a = tan(0.25 * PI + 0.5 * lat);
               const double b = (1. - eSinB) / (1. + eSinB);
               assert(b >= 0.0);
               const double tmp = tan_a * pow(b, .5 * _eccentricity);
               assert(tmp >= 0.0 && "Numerical error; calculation is correct, but may have round-off error");
               const double res = log(tmp);
               return res;
            }

            double
            computeLoxodromeImpl(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws()
            {
               // compute the true course
               double dLon = lon2 - lon1;
               if (dLon == 0) {
                  if (lat1 < lat2) {
                     az12 = 0;
                     az21 = PI;
                  }
                  else {
                     az12 = PI;
                     az21 = 0;
                  }
                  return abs(meridianArcLength(lat1, lat2));
               }
               if (lat1 == lat2) {
                  if (dLon > 0) {
                     if (dLon > PI) {
                        dLon = PI * 2.0 - dLon;
                        az21 = PI / 2.0;
                        az12 = az21 + PI;
                     }
                     else {
                        az12 = PI / 2.0;
                        az21 = az12 + PI;
                     }
                  }
                  else {
                     if (dLon < -PI) {
                        dLon = dLon + PI * 2.0;
                        az12 = PI / 2.0;
                        az21 = az12 + PI;
                     }
                     else {
                        dLon = -dLon;
                        az21 = PI / 2.0;
                        az12 = az21 + PI;
                     }
                  }
                  assert(dLon > 0.0);
                  return abs(parallelArcLength(lat1, dLon));
               }

               az12 = computeTrueCourse(lat1, lon1, lat2, lon2);
               az21 = clampRadiansAngle(az12 + PI);
               double s = abs(meridianArcLength(lat1, lat2) / cos(az12));
               return s;
            }

            double
            computeLoxodrome(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws()
            {
               double r = computeLoxodromeImpl(checkLatitude(lat1), checkLongitude(lon1), checkLatitude(lat2),
                     checkLongitude(lon2), az12, az21);
               if (abs(lat1) == PI / 2) {
                  az12 = lon2;
               }
               if (abs(lat2) == PI / 2) {
                  az21 = lon1;
               }
               return r;
            }

            /**
             * Travel along the loxodrome. If the starting latitude is at one of the poles,
             * then the resulting longitude is undefined.
             */
            void
            travelAlongLoxodromeImpl(double lat1, double lon1, double az, double s, double& lat2, double& lon2) const throws()
            {
               double cos_az = cos(az);
               if (abs(cos_az) < 1E-6) {
                  if (az > PI) {
                     s = -s;
                  }
                  lon2 = lon1 + s / circleRadius(lat1);
                  lat2 = lat1;
               }
               else {
                  double dArcLat = cos_az * s;
                  double s2 = dArcLat + meridianArcLength(lat1);
                  lat2 = inverseMeridianArcLength(s2);
                  lon2 = lon1 + tan(az) * (isometricLatitude(lat2) - isometricLatitude(lat1));
               }
               lon2 = clampLongitude(lon2);
            }

            void travelAlongMeridianArc(double lat1, double lon1, double m, double d, double& lat2, double& lon2) const throws ()
            {
               assert(((lat1 == PI / 2) || (lat1 == -PI / 2) || lon1 == m) && "Latitude is not one of the poles");

               // travel along the geodesic, but replace the end longitude if we start at one
               // of the poles (always travel north)
               travelAlongGeodesicImpl(checkLatitude(lat1), checkLongitude(lon1), 0.0, d, lat2, lon2);
               // since the longitude is undefined if the start point is at a pole, we just simply
               // define it to be m or
               if (abs(lat1) == PI / 2) {
                  lon2 = d >= 0 ? m : clampRadiansAngle(m - PI);
               }
               if (abs(lat2) == PI / 2) {
                  lon2 = lon1;
               }
            }

            void travelAlongLoxodrome(double lat1, double lon1, double az, double d, double& lat2, double& lon2) const throws()
            {
               if (lat1 > -PI / 2 && lat1 < PI / 2) {
                  assert(d >= 0);
                  travelAlongLoxodromeImpl(lat1, checkLongitude(lon1), az, d, lat2, lon2);
               }
               else if (abs(lat1) == PI / 2) {
                  travelAlongMeridianArc(lat1, checkLongitude(lon1), clampLongitude(az), d, lat2, lon2);
               }
               else {
                  assert("Invalid latitude" == 0);
               }
            }

            /**
             * Travel along the loxodrome. If the starting latitude is at one of the poles,
             * then the resulting longitude is undefined.
             * @note ellipsoid algorithm due to T. Vincenty
             */
            void
            travelAlongGeodesicImpl(double lat1, double lon1, double az, double s, double& lat2, double& lon2) const throws()
            {
               const Int MAX_ITERATION_COUNT = 20;

               const double cosAZ = cos(az);
               const double sinAZ = sin(az);
               const double tanU1 = (1. - _flattening) * tan(lat1);
               const double sigma1 = atan2(tanU1, cosAZ);
               const double u1 = atan(tanU1);
               const double cosU1 = cos(u1);
               const double sinU1 = sin(u1);
               const double sinAlpha = cosU1 * sinAZ;
               const double sinAlpha2 = sqr(sinAlpha);
               const double cosAlpha2 = 1. - sinAlpha2;

               const double u2 = cosAlpha2 * _secondEccentricity2;
               const double a = 1.0 + u2 / 16384. * (4096. + u2 * (-768. + u2 * (320. - 175. * u2))); // eqn  3
               const double b = u2 / 1024. * (256. + u2 * (-128. + u2 * (74. - 47. * u2))); // eqn  4
               double cos2SigmaM, cos2SigmaM2;
               double sinSigma, cosSigma;

               double sigma = s / (_minor * a);
               double sigma2;
               Int iterationsLeft = MAX_ITERATION_COUNT;
               do {
                  sinSigma = sin(sigma);
                  cosSigma = cos(sigma);
                  cos2SigmaM = cos(2.0 * sigma1 + sigma);
                  cos2SigmaM2 = sqr(cos2SigmaM);

                  const double tmp_eqn6_1 = cosSigma * (2. * cos2SigmaM2 - 1.0);
                  const double tmp_eqn6_2 = (4. * sqr(sinSigma) - 3.) * (4. * cos2SigmaM2 - 3.);
                  const double tmp_eqn6_3 = tmp_eqn6_1 - b / 6. * cos2SigmaM * tmp_eqn6_2;
                  const double deltaSigma = b * sinSigma * (cos2SigmaM + b / 4. * tmp_eqn6_3); // eqn  6
                  sigma2 = sigma;
                  sigma = s / (_minor * a) + deltaSigma;
               }while (iterationsLeft-- > 0 && abs(sigma2 - sigma) > EPS);

               const double cosU1cosS = cosU1 * cosSigma;
               const double sinU1sinS = sinU1 * sinSigma;
               const double sinU1cosS = sinU1 * cosSigma;
               const double cosU1sinS = cosU1 * sinSigma;

               const double tmp1 = sinU1cosS + cosU1sinS * cosAZ;
               const double tmp2 = (1. - _flattening) * sqrt(abs(sinAlpha2 + sqr(sinU1sinS - cosU1cosS * cosAZ)));
               lat2 = atan2(tmp1, tmp2);
               const double tmp3 = sinSigma * sinAZ;
               const double tmp4 = cosU1cosS - sinU1sinS * cosAZ;
               const double lambda = atan2(tmp3, tmp4);

               const double tmp_eqn10 = 4. + _flattening * (4. - 3. * cosAlpha2);
               const double c = (_flattening / 16.) * cosAlpha2 * tmp_eqn10; // eqn 10
               const double tmp_eqn11_1 = c * cosSigma * (2. * cos2SigmaM2 - 1.0);
               const double tmp_eqn11_2 = c * sinSigma * (cos2SigmaM + tmp_eqn11_1);
               lon2 = lon1 + lambda - (1.0 - c) * _flattening * sinAlpha * (sigma + tmp_eqn11_2); // eqn 11

               // ensure that the longitude is not less than -180 or more than +180 degrees
               lon2 = clampLongitude(lon2);
            }

            void travelAlongGeodesic(double lat1, double lon1, double az, double d, double& lat2, double& lon2) const throws ()
            {
               if (lat1 > -PI / 2 && lat1 < PI / 2) {
                  assert(d >= 0.0);
                  travelAlongGeodesicImpl(lat1, checkLongitude(lon1), az, d, lat2, lon2);
               }
               else if (abs(lat1) == PI / 2) {
                  travelAlongMeridianArc(lat1, checkLongitude(lon1), clampLongitude(az), d, lat2, lon2);
               }
               else {
                  assert("Invalid latitude" == 0);
               }
               if (abs(lat2) == PI / 2) {
                  lon2 = lon1;
               }
            }

            double
            computeGeodesicImpl(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws()
            {
               const Int MAX_ITERATION_COUNT = 20;

               double L = lon2 - lon1;
               if (abs(abs(L) - PI) < EPS) {
                  double d = antipodalArcLength(lat1, lat2);
                  if (d <= 0) {
                     az21 = az12 = PI;
                     d = -d;
                  }
                  else {
                     az21 = az12 = 0;
                  }
                  return d;
               }

               L = normalizeLongitude(L,0);

               if (L == 0 && (lat1 == lat2)) {
                  az12 = az21 = 0;
                  return 0;
               }
               double u = atan((1. - _flattening) * tan(lat1));
               const double cosU1 = cos(u);
               const double sinU1 = sin(u);
               u = atan((1. - _flattening) * tan(lat2));
               const double cosU2 = cos(u);
               const double sinU2 = sin(u);

               const double cosU1sinU2 = cosU1 * sinU2;
               const double sinU1cosU2 = sinU1 * cosU2;
               const double sinU1sinU2 = sinU1 * sinU2;
               const double cosU1cosU2 = cosU1 * cosU2;

               double sinLambda, cosLambda;
               double sinSigma, cosSigma, sinSigma2, cos2SigmaM;
               double cosAlpha2, cos2SigmaM2;
               double sigma;
               double lambda = L;
               double lambda2;
               Int iterationsLeft = MAX_ITERATION_COUNT;
               do {
                  sinLambda = sin(lambda);
                  cosLambda = cos(lambda);

                  sinSigma2 = sqr(cosU2 * sinLambda) + sqr(cosU1sinU2 - sinU1cosU2 * cosLambda); // eqn 14
                  sinSigma = sqrt(sinSigma2);
                  cosSigma = sinU1sinU2 + cosU1cosU2 * cosLambda; // eqn 15
                  sigma = atan2(sinSigma, cosSigma); // eqn 16

                  const double sinAlpha = cosU1cosU2 * sinLambda / sinSigma; // eqn 17
                  cosAlpha2 = 1.0 - sqr(sinAlpha);
                  if (sinU1sinU2 == 0.0) {
                     cos2SigmaM = cosSigma;
                  }
                  else {
                     cos2SigmaM = cosSigma - 2. * sinU1sinU2 / cosAlpha2; // eqn 18
                  }

                  cos2SigmaM2 = sqr(cos2SigmaM);

                  const double tmp_eqn10 = 4. + _flattening * (4. - 3. * cosAlpha2);
                  const double c = (_flattening / 16.) * cosAlpha2 * tmp_eqn10; // eqn 10

                  const double tmp_eqn11_1 = c * cosSigma * (2. * cos2SigmaM2 - 1.0);
                  const double tmp_eqn11_2 = c * sinSigma * (cos2SigmaM + tmp_eqn11_1);
                  lambda2 = lambda;
                  // for some reason, the next line begin with L+ and not L- as is originally written
                  lambda = L + (1.0 - c) * _flattening * sinAlpha * (sigma + tmp_eqn11_2); // eqn 11
               }while (iterationsLeft-- > 0 && abs(lambda2 - lambda) > EPS);

               const double u2 = cosAlpha2 * _secondEccentricity2;
               const double a = 1.0 + u2 / 16384. * (4096. + u2 * (-768. + u2 * (320. - 175. * u2))); // eqn  3
               const double b = u2 / 1024. * (256. + u2 * (-128. + u2 * (74. - 47. * u2))); // eqn  4

               const double tmp_eqn6_1 = cosSigma * (2. * cos2SigmaM2 - 1.0);
               const double tmp_eqn6_2 = (4. * sinSigma2 - 3.) * (4. * cos2SigmaM2 - 3.);
               const double tmp_eqn6_3 = tmp_eqn6_1 - b / 6. * cos2SigmaM * tmp_eqn6_2;
               const double deltaSigma = b * sinSigma * (cos2SigmaM + b / 4. * tmp_eqn6_3); // eqn  6

               double s = _minor * a * (sigma - deltaSigma); // eqn 19
               if (::std::isnan(s)) {
                  Log("terra.Spheroid").warn("distance is not-a-number");
               }

               az12 = atan2(cosU2 * sinLambda, cosU1sinU2 - sinU1cosU2 * cosLambda);
               az21 = atan2(cosU1 * sinLambda, cosU1sinU2 * cosLambda - sinU1cosU2) - PI;
               az12 = clampRadiansAngle(az12);
               az21 = clampRadiansAngle(az21);
               return s;
            }

            double computeGeodesic(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws()
            {
               double r = computeGeodesicImpl(checkLatitude(lat1), checkLongitude(lon1), checkLatitude(lat2),
                     checkLongitude(lon2), az12, az21);
               if (abs(lat1) == PI / 2) {
                  az12 = lon2;
               }
               if (abs(lat2) == PI / 2) {
                  az21 = lon1;
               }
               return r;
            }

            double computeApproximateDistance(double lat1, double lon1, double lat2, double lon2) const throws()
            {
               const double sinG = sin(0.5 * (lat1 - lat2));
               const double sinG2 = sinG * sinG;
               const double cosG2 = 1.0 - sinG2;

               const double sinF = sin(0.5 * (lat1 + lat2));
               const double sinF2 = sinF * sinF;
               const double cosF2 = 1.0 - sinF2;

               const double sinL = sin(0.5 * (lon1 - lon2));
               const double sinL2 = sinL * sinL;
               const double cosL2 = 1.0 - sinL2;

               const double s = sinG2 * cosL2 + cosF2 * sinL2;
               const double c = cosG2 * cosL2 + sinF2 * sinL2;
               const double w = atan2(sqrt(s), sqrt(c));

               if (w == 0.0) {
                  // this can happen if the two points are the same
                  return 0.0;
               }
               const double r = sqrt(s * c) / w;
               const double h1 = (3.0 * r - 1.0) / (2.0 * c);
               const double h2 = (3.0 * r + 1.0) / (2.0 * s);
               const double e = h1 * sinF2 * cosG2 - h2 * cosF2 * sinG2;
               const double d = 2.0 * w * _major;

               const double dist = d * (1. + e * _flattening);
               if (::std::isnan(dist)) {
                  LogEntry("terra.Spheroid").warn() << "approximate distance is not-a-number (c=" << c << ", s=" << s
                  << ")" << doLog;
               }

               return dist;
            }

            void computeECEF(double lat, double lon, double alt, double x[3]) const throws()
            {
               const double n = primeVerticalRadius(lat);
               const double cosLat = cos(lat);
               const double t = (n + alt) * cosLat;
               x[0] = t * cos(lon);
               x[1] = t * sin(lon);
               x[2] = (sqr(_minor / _major) * n + alt) * sin(lat);
            }

            void computeGeodetic(const double x[3], double& lat, double& lon, double& alt) const throws()
            {
	       const double p = sqrt(sqr(x[0]) + sqr(x[1]));
               const double theta = atan2(x[2] * _major, p * _minor);
               const double cosT = cos(theta);
               const double sinT = sin(theta);
	       
               const double y = x[2] + _secondEccentricity2 * _minor * sinT * sqr(sinT);
               const double z = p - _eccentricity2 * _major * cosT * sqr(cosT);
               lat = atan2(y, z);

	       const double coslat = cos(lat);
	       const double n = primeVerticalRadius(lat);
	       if (abs(coslat) < 1E-8) {
		 alt = abs(x[2]) - n*sqr(_minor/_major);
	       }
	       else {
		 alt = (p / coslat) - n;
	       }
               lon = atan2(x[1], x[0]);
            } 

            const double _major;
            const double _flattening;
            double _minor;
            double _eccentricity, _eccentricity2;
            double _secondEccentricity, _secondEccentricity2;
            double _1_minus_eccentricity2;
            double _sqrt_of_1_minus_eccentricity2;
            double _arcLengthSupport[5];
            double _mercatorSupport[4];
            double _wgs84CenterOffset[3];
            Int _hash;
      };

      struct Sphere : public Ellipsoid
      {
            Sphere(double r, double dx, double dy, double dz) :
               Ellipsoid(r, 0, dx, dy, dz)
            {
            }
            ~Sphere()throws()
            {
            }
            inline double radius() const throws()
            {
               return this->_major;
            } // from parent class
            double meridianRadius(double) const throws()
            {
               return radius();
            }
            double primeVerticalRadius(double) const throws()
            {
               return radius();
            }
            double meanRadius(double) const throws()
            {
               return radius();
            }
            double meridianArcLength(double lat) const throws()
            {
               return radius() * lat;
            }
            double meridianArcLength(double lat1, double lat2) const throws()
            {
               return radius() * (lat2 - lat1);
            }
            double area(double lat) const throws()
            {
               return sqr(radius()) * sin(lat);
            }
            double area(double lat1, double lat2) const throws()
            {
               return sqr(radius()) * abs(sin(lat1) - sin(lat2));
            }
            double geocentricLatitude(double lat) const throws()
            {
               return lat;
            }
            double parametricLatitude(double lat) const throws()
            {
               return lat;
            }
            double isometricLatitude(double lat) const throws()
            {
               return log(tan(.25 * PI + .5 * lat));
            }
            double
            computeLoxodrome(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws()
            {
               const double iso1 = Sphere::isometricLatitude(lat1);
               const double iso2 = Sphere::isometricLatitude(lat2);
               const double dPhi = iso2 - iso1;
               double q;
               if (abs(lat2 - lat1) < EPS) {
                  q = cos(lat1);
               }
               else {
                  q = (lat2 - lat1) / dPhi;
               }
               const double dLon = computeDeltaLon(lon1, lon2);

               az12 = clampRadiansAngle(atan2(dLon, dPhi));
               az21 = clampRadiansAngle(PI + az12);
               double d = sqrt(sqr(lat2 - lat1) + sqr(q * (dLon)));
               return d * radius();
            }
            // not very accurate it seems
            void computeMercatorProjection(double lon0, double lat, double lon, double& x, double& y) const throws()
            {
               lon = normalizeLongitude(lon - lon0,1E-8);
               lon = ::std::max(::std::min(lon,PI),-PI);

               x = radius()*lon;
               y = radius()*(log(tan(0.25 * PI + 0.5 * lat)));
            }

            void computeInverseMercatorProjection(double lon0, double x, double y, double& lat, double& lon) const throws()
            {
               lon = x/radius() + lon0;
               lat = 2.0 * atan(exp(y/radius())) - 0.5 * PI;
            }

            void travelAlongGeodesic(double lat1, double lon1, double az, double s, double& lat2, double& lon2) const throws()
            {
               s /= radius();
               const double sinD = sin(s);
               const double cosD = cos(s);
               const double cosAz = cos(az);
               const double sinAz = sin(az);
               const double sinLat1 = sin(lat1);
               const double cosLat1 = cos(lat1);
               const double sinDcosLat1 = sinD * cosLat1;
               const double sinLat2 = clampUnitInterval(sinLat1 * cosD + sinD * cosLat1 * cosAz);
               const double dLon = atan2(sinAz * sinDcosLat1, cosD - sinLat1 * sinLat2);
               lon2 = clampLongitude(lon1 + dLon);
               lat2 = asin(sinLat2);
            }

            double computeGeodesic(double lat1, double lon1, double lat2, double lon2, double& az12, double& az21) const throws()
            {
               const double sinLat1 = sin(lat1);
               const double sinLat2 = sin(lat2);
               const double cosLat1 = cos(lat1);
               const double cosLat2 = cos(lat2);
               const double cosDL = cos(lon1 - lon2);
               const double cosD = clampUnitInterval(sinLat1 * sinLat2 + cosLat1 * cosLat2 * cosDL);
               const double sinD = sqrt(1. - sqr(cosD));
               const double d = acos(cosD);
               double denom = sinD * cosLat1;
               if (denom == 0.0) {
                  az12 = 0;
               }
               else {
                  az12 = acos(clampUnitInterval((sinLat2 - sinLat1 * cosD) / denom));
                  if (sin(lon2 - lon1) < 0) {
                     az12 = 2.0 * PI - az12;
                  }
               }

               denom = sinD * cosLat2;
               if (denom == 0.0) {
                  az21 = 0;
               }
               else {
                  az21 = acos(clampUnitInterval((sinLat1 - sinLat2 * cosD) / denom));
                  if (sin(lon1 - lon2) < 0) {
                     az21 = 2.0 * PI - az21;
                  }
               }
               az12 = clampRadiansAngle(az12);
               az21 = clampRadiansAngle(az21);
               assert(!::std::isnan(d));
               assert(!::std::isnan(az12));
               assert(!::std::isnan(az21));
               return d * radius();
            }

            double computeApproximateDistance(double lat1, double lon1, double lat2, double lon2) const throws()
            {
               const double sinLat1 = sin(lat1);
               const double sinLat2 = sin(lat2);
               const double cosLat1 = cos(lat1);
               const double cosLat2 = cos(lat2);
               const double cosDL = cos(lon1 - lon2);
               const double cosD = sinLat1 * sinLat2 + cosLat1 * cosLat2 * cosDL;
               return radius() * acos(clampUnitInterval(cosD));
            }
      };

   }

   ::timber::Reference< Spheroid> Spheroid::getEPSG7030()
   throws()
   {
      return createEPSG7030Spheroid();
   }

   Reference< Spheroid> Spheroid::create(ReferenceSpheroid re)
   throws()
   {
      return new Ellipsoid(SPHEROIDS[re][0], SPHEROIDS[re][1], SPHEROIDS[re][2], SPHEROIDS[re][3], SPHEROIDS[re][4]);
   }

   Reference< Spheroid> Spheroid::createSphere(double a, double dx, double dy, double dz)
   throws()
   {
      return new Sphere(a,dx,dy,dz);
   }

   Reference< Spheroid> Spheroid::createEllipsoid(double a, double flat, double dx, double dy, double dz)
   throws()
   {
      if (flat == 0.0) {
         return new Sphere(a,dx,dy,dz);
      }
      else {
         return new Ellipsoid(a, flat,dx,dy,dz);
      }
   }

   Spheroid::Spheroid()
   throws()
   {
   }

   Spheroid::~Spheroid()
   throws()
   {
   }

   void Spheroid::setDefaultSpheroid(const Reference< Spheroid>& e)
   throws()
   {
      Log("terra.Spheroid").warn("setDefaultSpheroid is not implemented");
   }

   void Spheroid::convertGeodeticLocation(const Spheroid& toSpheroid, double& lat, double& lon, double& alt) const
   throws()
   {
      if (this!=&toSpheroid) {
         double xyz[3];
         computeECEF(lat,lon,alt,xyz);
         ecefToWGS84(xyz);
         toSpheroid.wgs84ToECEF(xyz);
         toSpheroid.computeGeodetic(xyz,lat,lon,alt);
      }
   }

   void Spheroid::convertECEFLocation(const Spheroid& toSpheroid, double* xyz) const
throws()
{
   if (this!=&toSpheroid) {
      ecefToWGS84(xyz);
      toSpheroid.wgs84ToECEF(xyz);
   }
}

}

