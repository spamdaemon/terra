#include <terra/terra.h>
#include <terra/GeodeticDatum.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/Loxodrome.h>
#include <terra/Geodesic.h>
#include <iostream>
#include <cmath>
#include <cctype>
#include <vector>
#include <cstring>

using namespace ::std;
using namespace ::timber;

namespace terra {
   namespace {
      template<class T>
      static void skipWS(const T& s, size_t& off) throws ()
      {
         // no need to test for end of string, because we know it will
         // be terminated by a character
         while (isspace(s[off])) {
            ++off;
         }
      }

      template<class T>
      static Int parseInt(const T& s, size_t& off, size_t n) throws (exception)
      {
         // no need to test for end of string, because we know it will
         // be terminated by a character
         Int v = 0;
         for (size_t i = 0; i < n; ++i) {
            if (isdigit(s[off])) {
               v = v * 10 + (s[off] - '0');
               ++off;
            }
            else {
               break;
            }
         }
         return v;
      }

      template<class T>
      static double parseFraction(const T& s, size_t& off) throws (exception)
      {
         // no need to test for end of string, because we know it will
         // be terminated by a character
         Int v = 0;
         Long t = 1;

         while (isdigit(s[off])) {
            v = v * 10 + (s[off] - '0');
            t *= 10;
            ++off;
         }

         return static_cast< double> (v) / static_cast< double> (t);
      }

      template<class T>
      static double parseLatitude(const T& s, size_t off, size_t n) throws (exception)
      {
         const size_t end = off + n;
         double deg(0), min(0), sec(0), frac(0);

         skipWS(s, off);
         deg = parseInt(s, off, 2);
         skipWS(s, off);
         if (isdigit(s[off])) {
            min = parseInt(s, off, 2);
            skipWS(s, off);
            if (isdigit(s[off])) {
               sec = parseInt(s, off, 2);
               if (s[off] == '.') {
                  ++off;
                  frac = parseFraction(s, off);
               }
            }
         }
         if (sec > 59 || min > 59 || deg > 90) {
            throw runtime_error("Could not parse latitude string");
         }
         if (deg == 90 && (min != 0 || sec != 0 || frac != 0)) {
            throw runtime_error("Could not parse latitude string");
         }
         skipWS(s, off);
         double res = (min + (sec + frac) / 60.0) / 60.0 + deg;
         assert(res <= 90);
         const char x(tolower(s[off++]));
         if (off == end) {
            if (x == 'n') {
               return res;
            }
            if (x == 's') {
               return -res;
            }
         }
         throw runtime_error("Could not parse latitude string");
      }

      template<class T>
      static double parseLongitude(const T& s, size_t off, size_t n) throws (exception)
      {
         const size_t end = off + n;
         double deg(0), min(0), sec(0), frac(0);

         skipWS(s, off);
         deg = parseInt(s, off, 3);
         skipWS(s, off);
         if (isdigit(s[off])) {
            min = parseInt(s, off, 2);
            skipWS(s, off);
            if (isdigit(s[off])) {
               sec = parseInt(s, off, 2);
               if (s[off] == '.') {
                  ++off;
                  frac = parseFraction(s, off);
               }
            }
         }
         if (sec > 59 || min > 59 || deg > 180) {
            throw runtime_error("Could not parse longitude string");
         }
         if (deg == 180 && (min != 0 || sec != 0 || frac != 0)) {
            throw runtime_error("Could not parse longitude string");
         }
         skipWS(s, off);

         double res = (min + (sec + frac) / 60.0) / 60.0 + deg;
         assert(res <= 180);

         const char x(tolower(s[off++]));
         if (off == end) {
            if (x == 'e') {
               return res;
            }
            if (x == 'w') {
               return -res;
            }
         }
         throw runtime_error("Could not parse longitude string");
      }

      template<class T>
      static GeodeticCoordinates parseLocation(const T& s, const size_t off, const size_t n, const Reference<
            GeodeticDatum>& d) throws (exception)
      {
         size_t start = off;
         const size_t end = off + n;

         size_t begin;
         Int latStart(0), latLen(0), lonStart(0), lonLen(0);
         begin = start;
         while (start < end) {
            const char x(tolower(s[start]));
            if (x == 'n' || x == 's') {
               if (latLen != 0) {
                  throw runtime_error("Cannot parse location");
               }
               latStart = begin;
               latLen = start + 1 - begin;
               begin = start + 1;
            }
            else if (x == 'e' || x == 'w') {
               if (lonLen != 0) {
                  throw runtime_error("Cannot parse location");
               }
               lonStart = begin;
               lonLen = start + 1 - begin;
               begin = start + 1;
            }
            ++start;
         }
         if (latLen != 0 && lonLen != 0) {
            double lat = parseLatitude(s, latStart, latLen);
            double lon = parseLongitude(s, lonStart, lonLen);
            return GeodeticCoordinates::fromDegrees(lat, lon, 0, d);
         }

         throw runtime_error("Could not parse location");
      }

   }

   double clampDegreeAngle(double a) throws()
   {
      while (a >= 360) {
         a -= 360;
      }
      while (a < 0) {
         a += 360;
      }
      return a;
   }

   double clampRadiansAngle(double a) throws()
   {
      double tmp = fmod(a, 2.0 * PI);
      if (tmp < 0) {
         tmp +=  2.0 * PI;
      }
      return tmp;
   }

   double clampLongitude(double lon, double eps) throws()
   {
      if (eps>0) {
         if (lon > PI) {
            if ( lon < PI+eps) {
               return PI;
            }
         }
         else if (lon < -PI) {
            if (lon > -(PI+eps)) {
               return -PI;
            }
         }
      }

      double tmp = fmod(lon, 2.0 * PI);
      double offset = (tmp > PI) ? -2.0 * PI : 0.0;
      offset = (tmp < -PI) ? 2.0 * PI : offset;
      lon = tmp + offset;
      return lon;
   }

   GeodeticCoordinates parseGeodeticCoordinates(const string& s, size_t off, size_t n,
         const Reference< GeodeticDatum>& d) throws (exception)
   {
      return parseLocation< string> (s, off, n, d);
   }

   GeodeticCoordinates parseGeodeticCoordinates(const char* s, size_t off, size_t n, const Reference< GeodeticDatum>& d)
         throws(exception)
   {
      return parseLocation< const char*> (s, off, n, d);
   }

   GeodeticCoordinates parseGeodeticCoordinates(const string& s, const Reference< GeodeticDatum>& d) throws (exception)
   {
      return parseGeodeticCoordinates(s.c_str(), 0, s.length(), d);
   }

   GeodeticCoordinates parseGeodeticCoordinates(const char* s, const Reference< GeodeticDatum>& d) throws (exception)
   {
      return parseGeodeticCoordinates(s, 0, strlen(s), d);
   }

   bool intersectLoxodromes(const GeodeticCoordinates* a, const GeodeticCoordinates* b, GeodeticCoordinates& res) throws()
   {
      assert(a[0].datum()->equals(*a[1].datum()));
      assert(b[0].datum()->equals(*b[1].datum()));
      assert(b[0].datum()->equals(*a[0].datum()));

      double u1[2], u2[2], v1[2], v2[2];

      a[0].mercatorProjection(u1[0], u1[1]);
      a[1].mercatorProjection(u2[0], u2[1]);
      b[0].mercatorProjection(v1[0], v1[1]);
      b[1].mercatorProjection(v2[0], v2[1]);

      const double u[2] = { u2[0] - u1[0], u2[1] - u1[1] };
      const double v[2] = { v2[0] - v1[0], v2[1] - v1[1] };

      return false;
   }

   size_t generateGeodesicPoints(const GeodeticCoordinates& a, const GeodeticCoordinates& b, double err, void(*cb)(
         const GeodeticCoordinates&)) 
   {
      vector< GeodeticCoordinates> X;
      X.push_back(b);
      X.push_back(a);
      Int count = 0;
      Int pointCount = 0;
      while (X.size() > 1 && ++count < 100) {
         GeodeticCoordinates p = X.back();
         X.pop_back();
         GeodeticCoordinates q = X.back();

         const Geodesic G(p, q);
         const Loxodrome L(p, q);
         if (::std::isnan(G.bearing())) {
            cerr << " P : " << p << '\n' << "Q : " << q << endl;
            cerr << " LAT : " << (p.latitude() == q.latitude()) << endl;
            cerr << " LON : " << (p.longitude() == q.longitude()) << endl;
         }
         assert(!::std::isnan(G.range()));
         assert(!::std::isnan(G.bearing()));
         assert(!::std::isnan(L.range()));
         assert(!::std::isnan(L.bearing()));
         if (abs(G.range() - L.range()) <= err) {
            cb(q);
            ++pointCount;
         }
         else {
            const Geodesic geo(G.range() / 2, G.bearing());
            const GeodeticCoordinates pq = geo.move(p);
            const Geodesic Gp(p, pq);
            const Geodesic Gq(pq, q);
            if (::std::isnan(Gp.bearing()) || ::std::isnan(Gq.bearing())) {
               cerr << "P : " << p << '\n' << "PQ: " << pq << '\n' << "Q : " << q << endl;

            }
            X.push_back(pq);
            X.push_back(p);
         }
      }
      assert(count < 100);
      return pointCount;
   }

}
