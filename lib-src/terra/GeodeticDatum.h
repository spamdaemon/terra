#ifndef _TERRA_GEODETICDATUM_H
#define _TERRA_GEODETICDATUM_H

#ifndef _TERRA_DATUM_H
#include <terra/Datum.h>
#endif

#ifndef _TERRA_SPHEROID_H
#include <terra/Spheroid.h>
#endif

namespace terra {

   /**
    * This class is used to describe the coordinate frame associated with
    * geodetic coordinates or coordinates related to geodetic coordinates.
    */
   class GeodeticDatum : public Datum
   {
         GeodeticDatum(const GeodeticDatum&);
         GeodeticDatum&operator=(const GeodeticDatum&);

         /**
          * Create a new geodetic datum using the given spheroid.
          * @param s the spheroid for this datum
          */
      protected:
         GeodeticDatum(const ::timber::Reference< Spheroid>& s)throws();

         /** Destructor */
      public:
         ~GeodeticDatum()throws();

         /**
          * Create a default frame.
          * @return a WGS84 frame
          * @return a geodetic datum
          */
      public:
         static ::timber::Reference< GeodeticDatum> create()throws();

         /**
          * Create a wgs84 frame. This is also known as <a href="http://www.epsg-registry.org/">EPSG:6326</a>.
          * @return a wgs 84 frame
          * @return the EPSG6326 datum.
          */
      public:
         inline static ::timber::Reference< GeodeticDatum> createWGS84()throws() { return createEPSG6326(); }


         /**
          * Create the datum registered as <a href="http://www.epsg-registry.org/">EPSG:6326</a>. This is just an alias
          * for createWGS84().
          * @return the EPSG6326 datum.
          */
      public:
         static ::timber::Reference< GeodeticDatum> createEPSG6326()throws();


         /**
          * Create a frame with the specified spheroid.
          * @param s a spheroid
          * @todo needs a horizontal and vertical datum
          * @return a geodetic datum
          */
      public:
         static ::timber::Reference< GeodeticDatum> create(const ::timber::Reference< Spheroid>& s)throws();

         /**
          * Get the spheroid or ellipsoid used by this datum.
          * @return the spheroid
          */
      public:
         inline const Spheroid& spheroid() const throws()
         {  return *_spheroid;}

         /**
          * Test if two datums are the same. This test must take the spheroid into account.
          * @param e another frame
          * @return true if e and this are the same frame
          */
      public:
         bool equals(const Datum& e) const throws();

         /** The spheroid */
      private:
         const ::timber::Reference< Spheroid> _spheroid;
   };
}

#endif
