#ifndef _TERRA_GEODETICCOORDINATES_H
#define _TERRA_GEODETICCOORDINATES_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif 

#ifndef _TERRA_LATLON_H
#include <terra/LatLon.h>
#endif 

#ifndef _TERRA_GeodeticDatum_H
#include <terra/GeodeticDatum.h>
#endif 

#ifndef _TERRA_COORDINATERFERENCESSYSTEM_H
#include <terra/CoordinateReferenceSystem.h>
#endif

#include <iosfwd>

namespace terra {
   /** ECEF coordinates */
   class ECEFCoordinates;

   /**
    * A GeodeticCooordinate is used to represent a location on the Earth in a coordinates
    * system based on a spheroidal approximation of the Earth. A coordinates consists of
    * three components:
    * <ol>
    * <li>Latitude between -90 and +90 degree. Positive latitude is used to indicate a position
    * on the northern hemisphere, a negative latitude indicates a location on the southern hemisphere.
    * <li>Longitude between -180 and 180 degree. Positive longitudes indicate locations east of the prime
    * meridian, and negative ones indiate locations west of the prime meridian.
    * <li>Height above or below the Spheroid
    * </ol>
    * The origin of a geodetic coordinates system is the intersection of the designated prime meridian
    * (usually the one through Greenwich, UK) and the equator.
    * @note All angles are specified in <em>radians</em> and <em>not degrees</em>.
    */
   class GeodeticCoordinates
   {
         /** An error value */
      private:
         static constexpr double LOCATION_ERROR = 1E-8;

         /**
          * Create a default GeodeticCoordinates. The GeodeticCoordinates will
          * be (0,0) in the current default SphereModel.
          */
      public:
         inline GeodeticCoordinates()throws()
         : _height(0),_datum(GeodeticDatum::createWGS84())
         {
            _latlon[0] = _latlon[1] = 0;
         }

         /**
          * Create geodetic coordinates from earth-center,earth-fixed coordinates.
          * @param ecef ECEF coordinates
          */
      public:
         GeodeticCoordinates(const ECEFCoordinates& ecef)throws();

         /**
          * Create a GeodeticCoordinates specified by latitude and longitude using
          * the current datum. Positive GeodeticCoordinates indicate the northern
          * hemisphere, and positive longitudes indicate the eastern hemisphere.
          *
          * @param latlon the latitude and longitude in radians
          * @pre REQUIRE_RANGE(latlon[0],-PI/2,PI/2)
          * @pre REQUIRE_RANGE(latlon[1],-PI,PI)
          */
      private:
         inline GeodeticCoordinates(const LatLon& latlon)throws()
         : _height(0),_datum(GeodeticDatum::create())

         {
            _latlon[0] = checkLatitude(latlon[0]);
            _latlon[1] = checkLongitude(latlon[1]);
         }

         /**
          * Create a GeodeticCoordinates specified by latitude and longitude using
          * the EPSG4979 coordinate reference system.
          *
          * @param epsg4979 an EPSG4979 location
          * @pre REQUIRE_RANGE(latlon[0],-PI/2,PI/2)
          * @pre REQUIRE_RANGE(latlon[1],-PI,PI)
          */
      public:
         inline GeodeticCoordinates(const CoordinateReferenceSystem::EPSG4979Coordinates& epsg)throws()
         : _height(epsg.alt),_datum(GeodeticDatum::createEPSG6326())
         {
            _latlon[0] = checkLatitude(epsg.lat);
            _latlon[1] = checkLongitude(epsg.lon);
         }

         /**
          * Create a GeodeticCoordinates specified by latitude and longitude using
          * the current datum. Positive GeodeticCoordinates indicate the northern
          * hemisphere, and positive longitudes indicate the eastern hemisphere.
          *
          * @param lat the latitude in radians
          * @param lon the longitude in radians
          * @pre REQUIRE_RANGE(lat,-PI,PI)
          * @pre REQUIRE_RANGE(lon,-PI/2,PI)
          */
      public:
         inline GeodeticCoordinates(double lat, double lon)throws()
         : _height(0),_datum(GeodeticDatum::createEPSG6326())
         {
            _latlon[0] = checkLatitude(lat);
            _latlon[1] = checkLongitude(lon);
         }

         /**
          * Create a GeodeticCoordinates specified by latitude and longitude using
          * the current datum. Positive GeodeticCoordinates indicate the northern
          * hemisphere, and positive longitudes indicate the eastern hemisphere.
          *
          * @param lat the latitude in radians
          * @param lon the longitude in radians
          * @param h the altitude above the spheroid
          * @pre REQUIRE_RANGE(lat,-PI,PI)
          * @pre REQUIRE_RANGE(lon,-PI/2,PI)
          */
      public:
         inline GeodeticCoordinates(double lat, double lon, double h)throws()
         : _height(h),_datum(GeodeticDatum::createEPSG6326())
         {
            _latlon[0] = checkLatitude(lat);
            _latlon[1] = checkLongitude(lon);
         }

         /**
          * Create a GeodeticCoordinates specified by latitude and longitude using
          * the current datum. Positive GeodeticCoordinates indicate the northern
          * hemisphere, and positive longitudes indicate the eastern hemisphere.
          *
          * @param lat the latitude in degree
          * @param lon the longitude in degree
          * @param h the altitude above the spheroid
          * @param s a reference spheroid
          * @pre REQUIRE_RANGE(lat,-PI,PI)
          * @pre REQUIRE_RANGE(lon,-PI/2,PI)
          * @pre REQUIRE_NON_ZERO(s)
          */
      public:
         inline GeodeticCoordinates(double lat, double lon, double h, const ::timber::Reference< GeodeticDatum>& s)throws()
         : _height(h),_datum(s)
         {
            _latlon[0] = checkLatitude(lat);
            _latlon[1] = checkLongitude(lon);
         }

         /**
          * Create a GeodeticCoordinates specified by latitude and longitude using
          * the current datum. Positive GeodeticCoordinates indicate the northern
          * hemisphere, and positive longitudes indicate the eastern hemisphere.
          *
          * @param lat the latitude in degree
          * @param lon the longitude in degree
          * @param s a reference spheroid
          * @pre REQUIRE_RANGE(lat,-PI,PI)
          * @pre REQUIRE_RANGE(lon,-PI/2,PI)
          * @pre REQUIRE_NON_ZERO(s)
          */
      public:
         inline GeodeticCoordinates(double lat, double lon, const ::timber::Reference< GeodeticDatum>& s)throws()
         : _height(0),_datum(s)
         {
            _latlon[0] = checkLatitude(lat);
            _latlon[1] = checkLongitude(lon);
         }

         /**
          * Create a new geodetic coordinats object with coordinates specified in degrees
          * @param lat the latitude in degrees
          * @param lon the longitude in degrees
          * @param alt the altitude above the spheroid
          * @return geodetic coordinates
          */
      public:
         static GeodeticCoordinates fromDegrees(double lat, double lon, double alt)throws();

         /**
          * Create a new geodetic coordinats object with coordinates specified in degrees
          * @param lat the latitude in degrees
          * @param lon the longitude in degrees
          * @param alt the altitude above the spheroid
          * @param s a spheroid
          * @return geodetic coordinates
          */
      public:
         static GeodeticCoordinates fromDegrees(double lat, double lon, double alt,
               const ::timber::Reference< GeodeticDatum>& s)throws();

         /**
          * Create a new geodetic coordinats object with coordinates specified in degrees
          * @param lat the latitude in degrees
          * @param lon the longitude in degrees
          * @return geodetic coordinates
          */
      public:
         static inline GeodeticCoordinates fromDegrees(double lat, double lon)throws()
         {  return fromDegrees(lat,lon,0);}

         /**
          * Create a new geodetic coordinats object with coordinates specified in degrees
          * @param lat the latitude in degrees
          * @param lon the longitude in degrees
          * @param s a spheroid
          * @return geodetic coordinates
          */
      public:
         static inline GeodeticCoordinates fromDegrees(double lat, double lon,
               const ::timber::Reference< GeodeticDatum>& s)throws()
         {  return fromDegrees(lat,lon,0,s);}

         /**
          * Create a new geodetic coordinats object with coordinates specified in degrees
          * @param latlon the latitude and longitude in degrees
          * @return geodetic coordinates
          */
      public:
         static inline GeodeticCoordinates fromDegrees(const LatLon& latlon)throws()
         {  return fromDegrees(latlon[0],latlon[1]);}

         /**
          * Create a geodetic coordinates object from an inverse mercator projection
          * @param x x-coordinate of a point in the Mercator projection
          * @param y y-coordinate of a point in the Mercator projection
          * @param d the datum
          * @return a geodetic coordinates object
          */
      public:
         static GeodeticCoordinates inverseMercatorProjection(double x, double y,
               const ::timber::Reference< GeodeticDatum>& d)throws();

         /**
          * Change the datum of these coordinates. The conversion may modify the
          * latitude, longitude, and altitude to represent ensure that the location
          * remains the same. For example, if the location represents the location of the Eiffel Tower,
	  * then the updated location should also be that of the Eiffel Tower.
          * @param newDatum the new datum
          */
      public:
         void changeDatum(const ::timber::Reference<GeodeticDatum>& newDatum) throws();


         /*
          * Get the corresponding ecef coordinates.
          * @return ECEFCoordinates(*this)
          */
      public:
         ECEFCoordinates ecefCoordinates() const throws();

         /**
          * Get the latitude in degrees. A positive value
          * indicates an northern latitude.
          * @return the latitude in radians
          */
      public:
         inline double latitude() const throws() {return _latlon[0];}

         /**
          * Get the longitude in degrees. A positive value
          * indicates a longitude east of the model's prime meridian
          * @return the longitude in radians
          */
      public:
         inline double longitude() const throws() {return _latlon[1];}

         /**
          * Get the height above the spheroid
          * @return the height above the spheroid
          */
      public:
         inline double height() const throws() {return _height;}

         /**
          * Get the latitude in degrees
          * @return the latitude in degrees
          */
      private:
         inline double degreesLat() const throws() {return ::timber::toDegrees(latitude());}

         /**
          * Get the longitude in degrees
          * @return the longitude in degrees
          */
      private:
         inline double degreesLon() const throws() {return ::timber::toDegrees(longitude());}

         /**
          * Get the spheroid.
          * @return the spheroid
          */
      public:
         inline const ::timber::Reference< GeodeticDatum>& datum() const throws() {return _datum;}

         /**
          * Get the point that is antipodal.
          * @return the antipodal point
          */
      public:
         GeodeticCoordinates antipodalCoordinates() const throws();

         /**
          * Compare two GeodeticCoordinates. This method does not test if
          * two points are actually the same point on the spheroid. For instance,
          * <code>GeodeticCoordinates(0,-180).equals(GeodeticCoordinates(0,180))==false</code>.
          * Use <code>isSameLocation()</code> to test if two coordinates are the same.
          * @param pt a coordinates object
          * @return true if the coordinates are exactly the same.
          */
      public:
         bool equals(const GeodeticCoordinates& pt) const throws();

         /**
          * Compare if two coordinates are actually the same point.
          * @param pt a coordinates object
          * @return true if this point and p are really the same location
          */
      public:
         bool isSameLocation(const GeodeticCoordinates& pt) const throws();

         /**
          * Compare two GeodeticCoordinates.
          */
      public:
         inline bool operator==(const GeodeticCoordinates& pt) const throws()
         {  return equals(pt);}

         /**
          * Compare two GeodeticCoordinates.
          */
      public:
         inline bool operator!=(const GeodeticCoordinates& pt) const throws()
         {  return !equals(pt);}

         /**
          * Print these coordinates.
          * @param out an output stream
          * @return out
          */
      public:
         ::std::ostream& print(::std::ostream& out) const;

         /**
          * Ensure that the latitude and longitude are valid.
          * @param lat a latitude in radians
          * @param lon a longitude in radians
          * @return true if the coordinates are ok
          */
      public:
         static constexpr inline bool verifyCoordinatesRadians(double lat, double lon)
         {
            return -PI <= lon && lon <= PI && -PI / 2 <= lat && lat <= PI / 2;
         }

         /**
          * Ensure that the latitude and longitude are valid.
          * @param lat a latitude in degrees
          * @param lon a longitude in degrees
          * @return true if the coordinates are ok
          */
      public:
         static constexpr inline bool verifyCoordinatesDegree(double lat, double lon)
         {
            return -180 <= lon && lon <= 180 && -90 <= lat && lat <= 90;
         }

         /**
          * Check the validity of a latitude. A small error is used
          * to ensure that values close to the edge are no out of bounds.
          * @return lat if it is between -90 and 90 (in radians)
          */
      private:
         static inline double checkLatitude(double lat)
         {
            assert(lat >= -PI / 2 - LOCATION_ERROR);
            assert(lat <= PI / 2 + LOCATION_ERROR);
            return ::std::min(PI / 2, ::std::max(-PI / 2, lat));
         }

         /**
          * Check the validity of a longitude. A small error is used
          * to ensure that values close to the edge are no out of bounds.
          * @return lat if it is between -180 and 180  (in radians)
          */
      private:
         static inline double checkLongitude(double lon)
         {
            assert(lon >= -PI - LOCATION_ERROR);
            assert(lon <= PI + LOCATION_ERROR);
            return ::std::min(PI, ::std::max(-PI, lon));
         }

         /**
          * @name Computations involving latitudes or longitudes (in radians!)
          * @{
          */

         /**
          * Compute the radius of curvature along a meridian.
          * @return the radius of curvature at the given latitude along a meridian
          */
      public:
         inline double meridianRadius() const throws()
         {  return _datum->spheroid().meridianRadius(latitude());}

         /**
          * Compute the radius of curvature along the prime vertical.
          * @return the radius of curvature along the prime vertical
          */
      public:
         inline double primeVerticalRadius() const throws()
         {  return _datum->spheroid().primeVerticalRadius(latitude());}

         /**
          * Compute the radius of a circle parallel to the equator at some latitude.
          * @return the radius of the circle parallel to the equator
          */
      public:
         inline double circleRadius() const throws()
         {  return _datum->spheroid().circleRadius(latitude());}

         /**
          * Compute the mean radius of curvature at given latitude.
          * @return return the mean radius of curvature
          */
      public:
         inline double meanRadius() const throws()
         {  return _datum->spheroid().meanRadius(latitude());}

         /**
          * Compute the arc length along a meridian from the equator to the given latitude.
          * The returned value is positive, regardless of the latitude. The result of a negative
          * latitude is <em>exactly</em> the same, that is, <code>meridianArcLength(x)==meridianArcLength(-x)</code>
          * @return the distance in meters from the equator to the given latitude
          */
      public:
         inline double meridianArcLength() const throws()
         {  return _datum->spheroid().meridianArcLength(latitude());}

         /**
          * Compute an approximation to the shortest distance between two points
          * given as latitude and longitude. Any distance along a meridian
          * (lon1==lon2) will most likely differ from the result obtained
          * from <code>meridianArcLength()</code>.
          * @param c a second point
          * @pre REQUIRE_EQUAL(datum(),c.datum())
          * @return the shortest distance between the two points on the spheroid
          */
      public:
         inline double approximateDistance(const GeodeticCoordinates& c) const throws()
         {
            assert(_datum->equals(*c._datum));
            return _datum->spheroid().computeApproximateDistance(latitude(),longitude(),c.latitude(),c.longitude());
         }

         /**
          * Compute the true course from this point to the specified point
          * @param dest the destination point
          * @return the true course (in radians)
          */
      public:
         double computeTrueCourse(const GeodeticCoordinates& dest) const throws();

         /*@}*/

         /**
          * Compute the smallest difference in latitude, longitude, and height between two points.
          * This method computes
          * <pre>
          *   diff[0] = latitude()-p.latitude();
          *   diff[1] = longitude()-p.longitude() and clamp to between -180 and 180
          *   diff[2] = height() - p.height()
          * </pre>
          * @param p a point
          * @pre REQUIRE_EQUAL(datum(),p.datum())
          * @param diff the difference vector (this - p)
          */
      public:
         void difference(const GeodeticCoordinates& p, double diff[3]) const throws();

         /**
          * Compute the mercator projection of this coordinates object.
          * @return a point
          */
      public:
         void mercatorProjection(double& x, double& y) const throws();

         /**
          * Travel along a geodesic
          * @param r the distance to travel in meters
          * @param bearing the initial bearing (azimuth) in radians
          * @return the new location
          */
      public:
         GeodeticCoordinates travelAlongGeodesic(double r, double bearing) const throws();

         /**
          * Travel along a loxodrome.
          * @param r the distance to travel in meters
          * @param bearing the bearing (azimuth) in radians
          * @return the new location
          */
      public:
         GeodeticCoordinates travelAlongLoxodrome(double r, double bearing) const throws();

         /**
          * Compute the geodesic distance to the specified point.
          * @param pt a point
          * @return the distance to the specified point if traveling along a geodesic
          * @see terra::Geodesic
          */
      public:
         double geodesicDistance(const GeodeticCoordinates& pt) const throws();

         /**
          * Compute the loxodromic distance to the specified point.
          * @param pt a point
          * @return the distance to the specified point if traveling along a loxodrome
          * @see terra::Loxodrome
          */
      public:
         double loxodromicDistance(const GeodeticCoordinates& pt) const throws();

         /** The height above the spheroid */
      private:
         double _height;

         /** The  latlon */
      private:
         LatLon _latlon;

         /** The implementation */
      private:
         ::timber::Reference< GeodeticDatum> _datum;
   };

}

/**
 * Print a GeodeticCoordinates object to a stream
 * @param out a stream
 * @param l a GeodeticCoordinates
 * @return out
 */
inline ::std::ostream& operator<<(::std::ostream& out, const ::terra::GeodeticCoordinates& l)
{
   return l.print(out);
}

#endif
