#include <terra/Geographic3DCRS.h>
#include <terra/GeodeticDatum.h>
#include <terra/terra.h>

namespace terra {

   using namespace ::timber;

   Geographic3DCRS::Geographic3DCRS()
   throws()
   {
   }

   Geographic3DCRS::~Geographic3DCRS()
   throws()
   {
   }

   Reference< Geographic3DCRS> Geographic3DCRS::create(const Reference< GeodeticDatum>& xDatum)
   throws()
   {
      struct _ : public Geographic3DCRS
      {
            _(const Reference< GeodeticDatum>& xdatum, const EpsgCode& xcode) throws()
                  : _datum(xdatum), _wgs84datum(Spheroid::getEPSG7030()), _code(xcode)
            {
            }
            ~_() throws()
            {
            }

            Reference< Datum> datum() const throws()
            {
               return _datum;
            }

            EpsgCode epsgCode() const throws()
            {
               return _code;
            }

            size_t coordinateCount() const throws()
            {
               return 2;
            }

            double distance(const double* p1, const double* p2) const throws()
            {
               // the distance on the spheroid is a geodesic arc
               const double c1[] = { toRadians(p1[0]), toRadians(p1[1]) };
               const double c2[] = { toRadians(p2[0]), toRadians(p2[1]) };

               // we're ignoring altitude

               double az[2];
               double r = _datum->spheroid().computeGeodesic(c1[0], c1[1], c2[0], c2[1], az[0], az[1]);

               return r;
            }

            bool toEPSG4979(const double* coords, EPSG4979Coordinates& wgs84) const throws()
            {
               wgs84.lat = toRadians(coords[0]);
               wgs84.lon = toRadians(coords[1]);
               wgs84.alt = coords[2];
               _datum->spheroid().convertGeodeticLocation(*_wgs84datum, wgs84.lat, wgs84.lon, wgs84.alt);
               return true;
            }

            bool fromEPSG4979(const EPSG4979Coordinates& wgs84, double* coords) const throws()
            {
               coords[0] = wgs84.lat;
               coords[1] = wgs84.lon;
               coords[2] = wgs84.alt;
               _wgs84datum->convertGeodeticLocation(_datum->spheroid(), coords[0], coords[1], coords[2]);
               coords[0] = toDegrees(coords[0]);
               coords[1] = toDegrees(coords[1]);
               return true;
            }

            bool equals(const CoordinateReferenceSystem& e) const throws()
            {
               const _* other = dynamic_cast< const _*>(&e);
               return (other == this) || (other != 0 && _datum->equals(*other->_datum));
            }

            /** The wgs 84 datum */
         private:
            Reference< GeodeticDatum> _datum;

            /** The wgs 84 datum */
         private:
            Reference< Spheroid> _wgs84datum;

            /** the epsg code (if known) */
         private:
            const EpsgCode _code;
      };

      Geographic3DCRS* crs = new _(xDatum, EpsgCode());
      return Reference< Geographic3DCRS>(crs);
   }

}
