#include <terra/EpsgCode.h>
#include <sstream>

namespace terra {
   EpsgCode::EpsgCode()
   throws()
   : _code(0),_type(UNDEFINED) {}

   EpsgCode::EpsgCode(BaseType t, size_t c)
   throws()
   : _code(c), _type(t) {}

   bool EpsgCode::operator==(const EpsgCode& c) const
   throws()
   {
      if (c._type==UNDEFINED && _type==UNDEFINED) {
         return true;
      }
      else return c._code==_code && c._type==_type;
   }

   bool EpsgCode::operator<(const EpsgCode& c) const
   throws()
   {
      if (_type < c._type) {
         return true;
      }
      if (_type!=c._type || _type==UNDEFINED) {
         return false;
      }

      return _code<c._code;
   }

   ::std::string EpsgCode::toString() const
throws()
{
   if (_type == UNDEFINED) {
      return ::std::string();
   }
   else {
      ::std::ostringstream out;
      out << "EPSG:" << _code;
      return out.str();
   }
}

}

::std::ostream& operator<<(::std::ostream& out, const ::terra::EpsgCode& code)
{
   if (code.type() == ::terra::EpsgCode::UNDEFINED) {
      return out << "EPSG:UNDEFINED";
   }
   else {
      return out << "EPSG:" << code.code();
   }
}
