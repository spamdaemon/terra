#include <terra/TopocentricCoordinates.h>
#include <terra/ECEFCoordinates.h>

#include <iostream>
#include <cmath>

using namespace ::timber;

namespace terra {

  TopocentricCoordinates::TopocentricCoordinates() throws()
  : _frame(TopocentricFrame::create())
  { _frame->transformFromGeodetic(_frame->origin(),_x); }

  TopocentricCoordinates::TopocentricCoordinates(double px, double py, double alt) throws()
  : _frame(TopocentricFrame::create())
  { _x[0] = px; _x[1] = py; _x[2] = alt; }

  TopocentricCoordinates::TopocentricCoordinates (const GeodeticCoordinates& geo) throws()
  : _frame(TopocentricFrame::create(geo))
  { _frame->transformFromGeodetic(geo,_x); }

  TopocentricCoordinates::TopocentricCoordinates (const GeodeticCoordinates& geo,
						  double px, double py, double alt) throws()
  : _frame(TopocentricFrame::create(geo))
  { _x[0] = px; _x[1] = py; _x[2] = alt; }
  
  TopocentricCoordinates::TopocentricCoordinates (const Reference<TopocentricFrame>& f,
						  const GeodeticCoordinates& geo) throws()
  : _frame(f)
  { f->transformFromGeodetic(geo,_x); }

  TopocentricCoordinates::TopocentricCoordinates (const ECEFCoordinates& ecef) throws()
  : _frame(TopocentricFrame::create(ecef.geodeticCoordinates()))
  { _frame->transformFromECEF(ecef,_x); }
  
  TopocentricCoordinates::TopocentricCoordinates (const Reference<TopocentricFrame>& f,
						  const ECEFCoordinates& ecef) throws()
  : _frame(f)
  { f->transformFromECEF(ecef,_x); }

  TopocentricCoordinates& TopocentricCoordinates::operator= (const TopocentricCoordinates& c) throws()
  {
    if (this != &c) {
      _frame = c._frame;
      _x[0] = c._x[0];
      _x[1] = c._x[1];
      _x[2] = c._x[2];
    }
    return *this;
  }
  
  void TopocentricCoordinates::replaceFrame (const  ::timber::Reference<TopocentricFrame>& f) throws()
  { _frame = f; }

  bool TopocentricCoordinates::equals (const TopocentricCoordinates& pt) const throws()
  { 
    if (&pt==this) {
      return true;
    }
    return _frame->origin().equals(pt._frame->origin()) && 
      x()==pt.x() && 
      y()==pt.y() && 
      z()==pt.z();
  }
  
  double TopocentricCoordinates::distance (const TopocentricCoordinates& c) const throws()
  {
    assert(_frame->origin()==(c._frame->origin()));
    double tmp = (x()-c.x()) * (x()-c.x());
    tmp += (y()-c.y()) * (y()-c.y());
    tmp += (z()-c.z()) * (z()-c.z());
    return ::std::sqrt(tmp); 
  }

  GeodeticCoordinates TopocentricCoordinates::geodeticCoordinates() const throws()
  {
    double coords[3];
    TopocentricFrame::transformToGeodetic(*this,coords);
    return GeodeticCoordinates(coords[0],coords[1],coords[2],_frame->origin().datum());
  }


  ECEFCoordinates TopocentricCoordinates::ecefCoordinates() const throws()
  {
    double coords[3];
    TopocentricFrame::transformToECEF(*this,coords);
    return ECEFCoordinates(coords[0],coords[1],coords[2],_frame->origin().datum());
  }
  
  ::std::ostream& TopocentricCoordinates::print(::std::ostream& out) const 
  {
    return out << "[ " << x() <<" " << y() << " " << z() << " ]";
  }

  double TopocentricCoordinates::elevation() const throws()
  {
    return ::std::atan2(z(),groundRange());
  }

  TopocentricCoordinates TopocentricCoordinates::travel (double dx, double dy, double dz) const throws()
  {
    TopocentricCoordinates res(*this);
    res._x[0] += dx;
    res._x[1] += dy;
    res._x[2] += dz;
    return res;
  }

  TopocentricCoordinates TopocentricCoordinates::travelRangeBearing (double r, double headingAz) const throws()
  {
    TopocentricCoordinates res(*this);
    res._x[0] += r * ::std::sin(headingAz);
    res._x[1] += r * ::std::cos(headingAz);
    return res;
  }

  TopocentricCoordinates TopocentricCoordinates::travelRangeBearingElevation (double r, double headingAz, double elevAngle) const throws()
  {
    TopocentricCoordinates res(*this);
    double gr = r * ::std::cos(elevAngle);
    res._x[0] += gr * ::std::sin(headingAz);
    res._x[1] += gr * ::std::cos(headingAz);
    res._x[2] += r * ::std::sin(elevAngle);
    return res;
  }


}
