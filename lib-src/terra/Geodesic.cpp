#include <terra/Geodesic.h>
#include <terra/GeodeticCoordinates.h>
#include <iostream>
#include <cmath>

namespace terra {
   using namespace ::timber;

   namespace {
      static double dummy(0);
   }

   ::std::ostream& Geodesic::print(::std::ostream& out) const
   {
      return out << "(r=" << range() << " ,az=" << toDegrees(bearing()) << ")";
   }

   Geodesic::Geodesic(double r, double az) throws()
         : _range(::std::abs(r)), _bearing(clampRadiansAngle(r < 0 ? az - PI : az))
   {
   }

   Geodesic::Geodesic(const GeodeticCoordinates& from, const GeodeticCoordinates& to) throws()
   {
      assert(from.datum()->equals(*to.datum()));
      _range = from.datum()->spheroid().computeGeodesic(from.latitude(), from.longitude(), to.latitude(),
            to.longitude(), _bearing, dummy);
   }

   GeodeticCoordinates Geodesic::move(const GeodeticCoordinates& p) const throws ()
   {
      double lat, lon;
      p.datum()->spheroid().travelAlongGeodesic(p.latitude(), p.longitude(), bearing(), range(), lat, lon);
      return GeodeticCoordinates(lat, lon, p.height(), p.datum());
   }
}
