#ifndef _TERRA_GEOGRAPHIC3DCRS_H
#define _TERRA_GEOGRAPHIC3DCRS_H

#ifndef _TERRA_GEODETICCRS_H
#include <terra/GeodeticCRS.h>
#endif

namespace terra {
   class GeodeticDatum;

   /**
    * A coordinate reference system provides information about Location objects. The only method
    * functionality provided by this class is the ability to produce a GeoLocation object in the
    * EPSG4326 reference system.
    * More about EPSG can be found <a href="http://www.epsg-registry.org/">here</a>.
    */
   class Geographic3DCRS : public GeodeticCRS
   {
      private:
      Geographic3DCRS(const Geographic3DCRS&);
      Geographic3DCRS&operator=(const Geographic3DCRS&);

         /**
          * Create a new frame.
          * @param s the frame
          */
      protected:
      Geographic3DCRS()throws();

         /** Destructor */
      public:
         ~Geographic3DCRS()throws();

         /**
          * Create a geographic crs using the specified datum.
          * @param datum a datum
          * @return a geographic crs
          */
      public:
         ::timber::Reference<Geographic3DCRS> create(const ::timber::Reference<GeodeticDatum>& datum) throws();

   };
}

#endif
