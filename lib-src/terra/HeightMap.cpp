#include <terra/HeightMap.h>
#include <terra/terra.h>
#include <terra/GeodeticDatum.h>
#include <cmath>

using namespace ::timber;

namespace terra {

   HeightMap::HeightMap()
   throws() {}
   HeightMap::~HeightMap()
   throws() {}

   Reference< HeightMap> HeightMap::createConstantHeightMap(const Reference< GeodeticDatum>& f, double hae, double err)
throws()
{
   struct Impl : public HeightMap {
      Impl (const Reference<GeodeticDatum>& xs, double xhae, double xerr)
      : _frame(xs),_heightError(xerr), _hae(xhae) {}

      ~Impl() throws() {}

      Reference<GeodeticDatum> datum() const throws()
      {  return _frame;}

      double heightError (double lat, double ) const throws()
      {
         return checkLatitude(lat) ? _heightError : 0;
      }

      inline bool checkLatitude (double lat) const throws()
      {  return -PI/2 <= lat && lat <= PI/2;}

      double heightAboveEllipsoid(double lat, double, bool& isValid ) const throws ()
      {
         isValid = checkLatitude(lat);
         return _hae;
      }

      /** The frame */
      private:
      const ::timber::Reference<GeodeticDatum> _frame;

      /** The height error */
      private:
      const double _heightError;

      private:
      const double _hae;
   };

   HeightMap* map = new Impl(f,hae,err);
   return map;
}

}

