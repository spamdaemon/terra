#ifndef _TERRA_GEOGRAPHIC2DCRS_H
#define _TERRA_GEOGRAPHIC2DCRS_H

#ifndef _TERRA_GEODETICCRS_H
#include <terra/GeodeticCRS.h>
#endif

namespace terra {
   class GeodeticDatum;

   /**
    * A coordinate reference system provides information about Location objects. The only method
    * functionality provided by this class is the ability to produce a GeoLocation object in the
    * EPSG4326 reference system.
    * More about EPSG can be found <a href="http://www.epsg-registry.org/">here</a>.
    */
   class Geographic2DCRS : public GeodeticCRS
   {
      private:
         Geographic2DCRS(const Geographic2DCRS&);
         Geographic2DCRS&operator=(const Geographic2DCRS&);

         /**
          * Create a new frame.
          * @param s the frame
          */
      protected:
         Geographic2DCRS()throws();

         /** Destructor */
      public:
         ~Geographic2DCRS()throws();

         /**
          * Create a geographic crs using the specified datum.
          * @param datum a datum
          * @return a geographic crs
          */
      public:
         ::timber::Reference<Geographic2DCRS> create(const ::timber::Reference<GeodeticDatum>& datum) throws();

   };
}

#endif
