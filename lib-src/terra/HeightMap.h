#ifndef _TERRA_HEIGHTMAP_H
#define _TERRA_HEIGHTMAP_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace terra {
  class GeodeticDatum;

  /** 
   * The HeightMap allows for conversion of positions on the heightmap into absolute
   * coordinates with respect to some ellipsoid. The primary function is thus compute
   * the height above the ellipsoid for some point on the height map.
   */
  class HeightMap : public ::timber::Counted {
    /** No copying */
  private:
    HeightMap(const HeightMap&);
    HeightMap&operator=(const HeightMap&);

    /** Default constructor */
  protected:
    HeightMap() throws();

    /** Destructor */
  public:
    ~HeightMap() throws();

    /**
     * Create a default height map. This height map will
     * produce the height for every point on the ellisoid.
     * @param frame the frame over which the heightmap is defined
     * @param hae the height above the ellipsoid
     * @param err the height error
     * @return a height map
     */
  public:
    static ::timber::Reference<HeightMap> createConstantHeightMap(const ::timber::Reference<GeodeticDatum>& frame, double hae, double err=0.0) throws();

    
    /**
     * Get the underlying datum. All height queries return height
     * values with respect to this frame.
     * @return the datum
     */
  public:
    virtual ::timber::Reference<GeodeticDatum> datum() const throws() = 0;
    
    /**
     * Get the height above the ellipsoid for a point on this height map.
     * 
     * @param lat the latitude of a point in radians
     * @param lon the longitude of a point in radians
     * @param isValidHeight this is set to false if the height value cannot be computed
     * @return the height above the ellipsoid (in meters)
     */
  public:
    virtual double heightAboveEllipsoid(double lat, double lon, bool& isValidHeight) const throws () = 0;

    /**
     * Get the error in height that is expected at the specified latitude and longitude. If the
     * latitude and longitude error is not known, then a default maybe assumed for the given 
     * spherod. For example, on the earth, the maximum height error is roughl 12000 meters.
     * @param lat the latitude of a point in radians
     * @param lon the longitude of a point in radians
     * @return the 1-sigma error in height above the ellipsoid, or 0 if no height error is known
     */
  public:
    virtual double heightError (double lat, double lon) const throws() = 0;
  };
}

#endif
