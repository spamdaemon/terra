#ifndef _TERRA_ECEFCOORDINATES_H
#define _TERRA_ECEFCOORDINATES_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif 

#ifndef _TERRA_GeodeticDatum_H
#include <terra/GeodeticDatum.h>
#endif 

#include <iosfwd>

namespace terra {
  
  /** Geodetic coordinates */
  class GeodeticCoordinates;
    
  /**
   * This class is an implementation of coordinates in an Earth-Centered, Earth-Fixed
   * Cartesian coordinate system. 
   */
  class ECEFCoordinates {
    /**
     * Create a default ECEFCoordinates. 
     */
  public:
    inline ECEFCoordinates () throws()
      : _datum(GeodeticDatum::create())
      { _x[0] = _x[1] = _x[2] = 0; }
      
    /**
     * Create  earth-center,earth-fixed coordinantes coordinates from geodetic coordinates.
     * @param gc geodetic coordinates
     */
  public:
    ECEFCoordinates (const GeodeticCoordinates& gc) throws();

    /**
     * Create a ECEFCoordinates specified by latitude and longitude using
     * the current datum. Positive ECEFCoordinates indicate the northern
     * hemisphere, and positive longitudes indicate the eastern hemisphere.
     *
     * @param cx the x-coordinate in meters
     * @param cy the x-coordinate in meters
     * @param cz the x-coordinate in meters
     */
  public:
    inline ECEFCoordinates (double cx, double cy, double cz) throws()
      : _datum(GeodeticDatum::create())
      { _x[0] = cx; _x[1] = cy; _x[2] = cz; }
      
    /**
     * Create a ECEFCoordinates specified by latitude and longitude using
     * the current datum. Positive ECEFCoordinates indicate the northern
     * hemisphere, and positive longitudes indicate the eastern hemisphere.
     *
     * @param cx the x-coordinate in meters
     * @param cy the x-coordinate in meters
     * @param cz the x-coordinate in meters
     * @param s a reference frame 
     */
  public:
    inline ECEFCoordinates (double cx, double cy, double cz, const ::timber::Reference<GeodeticDatum>& s) throws()
      : _datum(s)
    { _x[0] = cx; _x[1] = cy; _x[2] = cz; }
	
    /**
     * Copy operator.
     * @param c a coordinates object
     * @return *this
     */
  public:
    inline ECEFCoordinates operator= (const ECEFCoordinates& c) throws()
      {
	if (this != &c) {
	  _datum = c._datum;
	  _x[0] = c._x[0];
	  _x[1] = c._x[1];
	  _x[2] = c._x[2];
	}
	return *this;
      }

    /**
     * Change the datum of these coordinates. The conversion may modify the
     * latitude, longitude, and altitude to represent ensure that the location
     * remains the same.
     * @param newDatum the new datum
     */
 public:
    void changeDatum(const ::timber::Reference<GeodeticDatum>& newDatum) throws();

   /**
     * Get the x-coordinate.
     * @return the x-coordinate
     */
  public:
    inline double x () const throws() { return _x[0]; }

    /**
     * Get the y-coordinate.
     * @return the y-coordinate
     */
  public:
    inline double y () const throws() { return _x[1]; }

    /**
     * Get the z-coordinate.
     * @return the z-coordinate
     */
  public:
    inline double z () const throws() { return _x[2]; }

    /**
     * Get one of the coordinates.
     * @param i a coordinate index
     * @pre REQUIRE_RANGE(i,0,2)
     * @return a reference to the coordinates
     */
  public:
    inline double operator() (size_t i) const throws() 
    {
      assert(i<3);
      return _x[i];
    }
      
    /**
     * Get the coordinates themselves.
     * @return a reference to the coordinates
     */
  public:
    inline const double* coordinates() const throws() { return _x; }

    /**
     * Get the equivalent geodetic coordinates.
     * @return GeodeticCoordinates(*this)
     */
  public:
    GeodeticCoordinates geodeticCoordinates() const throws();

    /**
     * Get the reference frame.
     * @return the reference frame
     */
  public:
    inline const ::timber::Reference<GeodeticDatum>& datum () const throws() { return _datum; }

    /**
     * Compare two ECEFCoordinates. 
     * It is very likely that two ECEFCoordinates are different,
     * because they are represented as float values.
     */
  public:
    bool equals (const ECEFCoordinates& pt) const throws();

    /**
     * Compare two ECEFCoordinates.
     * @param pt a ECEF coordinate
     * @return true if <code>this->equals(pt)</code>
     */
  public:
    inline bool operator== (const ECEFCoordinates& pt) const throws()
    { return equals(pt); }

    /**
     * Compare two ECEFCoordinates.
     * @param pt a ECEF coordinate
     * @return true if <code>!this->equals(pt)</code>
     */
  public:
    inline bool operator!= (const ECEFCoordinates& pt) const throws()
    { return !equals(pt); }

    /**
     * Compute the distance between two ECEFCoordinates. This distance
     * may not be close the real distance on the spheroid.
     * @param c an ecef coordinate object
     * @pre REQUIRE_EQUAL(c.datum(),datum())
     * @return distance the distance between two ECEF coordinates.
     */
  public:
    double distance (const ECEFCoordinates& c) const throws();

    /**
     * Print these coordinates.
     * @param out an output stream
     * @return out
     */
  public:
    ::std::ostream& print ( ::std::ostream& out) const;

    /** The cartesian coordinates */
  private:
    double _x[3];

    /** The datum */
  private:
    ::timber::Reference<GeodeticDatum> _datum;
  };

}

/**
 * Print a ECEFCoordinates object to a stream
 * @param out a stream
 * @param l a ECEFCoordinates
 * @return out
 */
inline ::std::ostream& operator<< (::std::ostream& out, const ::terra::ECEFCoordinates& l)
{ return l.print(out); }

#endif
