#include <terra/dted/Tile.h>
#include <terra/GeodeticDatum.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/terra.h>
#include <timber/logging.h>

#include <cmath>
#include <iostream>
#include <sstream>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::timber::logging;

namespace terra {
   namespace dted {
      namespace {

         static const char stringUHL1[] = { 'U', 'H', 'L', '1' };
         static const char stringDSI[] = { 'D', 'S', 'I' };
         static const char stringACC[] = { 'A', 'C', 'C' };

         static void match(istream& stream, const char* str, size_t n)
         {
            string matched;
            matched.reserve(n);
            for (size_t i = 0; i < n; ++i) {
               char c = stream.get();
               matched += c;
               if (c != str[i]) {
                  ::std::string xstr(str, str + n);
                  throw ::std::runtime_error("Expected " + xstr + " got " + matched);
               }
            }
         }

         static void syncUHL1(istream& stream)
         {
            // this function depends on the search string
            // being "UHL1", where a suffix of UHL1 is NOT
            // a prefix!
            size_t matched = 0;
            while (matched != sizeof(stringUHL1)) {
               char c = stream.get();
               if (stream.fail()) {
                  throw ::std::runtime_error("UHL1 token not found");
               }
               if (stringUHL1[matched] == c) {
                  ++matched;
               }
               else {
                  // reset matched
                  matched = 0;
               }
            }
         }

         static double parseDDDMMSSH(const char* str)
         {
            istringstream ddd(string(str, 3));
            istringstream mm(string(str + 3, 2));
            istringstream ss(string(str + 5, 2));

            double deg, min, sec;
            ddd >> deg;
            mm >> min;
            ss >> sec;

            double x = deg + (min + (sec / 60)) / 60;
            if (str[7] == 'W' || str[7] == 'S') {
               x = -x;
            }
            return toRadians(x);
         }

         struct UHL
         {
               inline double west() const
               {
                  return parseDDDMMSSH(_longitude);
               }
               inline double south() const
               {
                  return parseDDDMMSSH(_latitude);
               }

               inline double verticalAccuracy() const
               {
                  string acc(_absVerticalAccuracy, sizeof(_absVerticalAccuracy));
                  if (acc[2] == 'N' && acc[3] == 'A') {
                     return 0;
                  }
                  istringstream in(acc);
                  double x;
                  in >> x;
                  if (in.fail()) {
                     throw ::std::runtime_error("Could not parse vertical accuracy");
                  }
                  return x;
               }

               inline double longitudeInterval() const
               {
                  istringstream in(string(_longitudeInterval, sizeof(_longitudeInterval)));
                  double x;
                  in >> x;
                  if (in.fail()) {
                     throw ::std::runtime_error("Could not parse longitude interval");
                  }
                  return toRadians(x / 36000.0);
               }

               inline double latitudeInterval() const
               {
                  istringstream in(string(_latitudeInterval, sizeof(_latitudeInterval)));
                  double x;
                  in >> x;
                  if (in.fail()) {
                     throw ::std::runtime_error("Could not parse latitude interval");
                  }
                  return toRadians(x / 36000.0);
               }

               char _longitude[8];
               char _latitude[8];
               char _longitudeInterval[4];
               char _latitudeInterval[4];
               char _absVerticalAccuracy[4];
               char _security[3];
               char _referenceNo[12];
               char _nLongitudeLines[4];
               char _nLatitudePosts[4];
               char _multipleAccuracy[1];
               char _reserved[24];
         };
         struct DSI
         {
               char _security[1];
               char _securityControl[2];
               char _securityHandling[27];
               char _reserved1[26];
               char _dtedLevel[5];
               char _referenceNo[15];
               char _reserved2[8];
               char _dataEdition[2];
               char _mergeVersion[1];
               char _maintenanceDate[4];
               char _matchDate[4];
               char _maintenanceDescription[4];
               char _producerCode[8];
               char _reserved3[16];
               char _productSpec[9];
               char _productChangeNumber[2];
               char _productSpecData[4];
               char _verticalData[3];
               char _horizontalDatum[5];
               char _collectionSystem[10];
               char _compilationDate[4];
               char _reserved4[22];
               char _latitude[9];
               char _longitude[10];
               char _swLatitude[7];
               char _swLongitude[8];
               char _nwLatitude[7];
               char _nwLongitude[8];
               char _neLatitude[7];
               char _neLongitude[8];
               char _seLatitude[7];
               char _seLongitude[8];
               char _azDataOrientation[9];
               char _latitudeInterval[4];
               char _longitudeInterval[4];
               char _nLatitudeLines[4];
               char _nLongitudeLines[4];
               char _partialCellIndicator[2];
               char _nimaUse[101];
               char _reserved5[100];
               char _comments[156];
         };
         struct ACCSubRegionOutline
         {
               char _latitude[9];
               char _longitude[10];
         };
         struct ACCSubRegion
         {
               char _horizontalASubRegionccuracy[4];
               char _verticalSubRegionAccuracy[4];
               char _relativeHorizontalSubRegionAccuracy[4];
               char _relativeVerticalSubRegionAccuracy[4];
               char _multipleSubRegionAccuracyOutline[2];
               ACCSubRegionOutline _outline[14];
         };

         struct ACC
         {
               char _horizontalAccuracy[4];
               char _verticalAccuracy[4];
               char _relativeHorizontalAccuracy[4];
               char _relativeVerticalAccuracy[4];
               char _reserved1[4];
               char _NIMAUse[1];
               char _reserved2[31];
               char _multipleAccuracyOutline[2];
               ACCSubRegion _subRegions[9];
               char _NIMAUse2[18];
               char _reserved3[69];
         };

         struct DataRecord
         {
            private:
               DataRecord(const DataRecord&);
               DataRecord&operator=(const DataRecord&);
            public:
               DataRecord(const UHL& uhl)
                     : _west(uhl.west()), _south(uhl.south()), _deltaLat(uhl.latitudeInterval()), _deltaLon(
                           uhl.longitudeInterval())
               {
                  {
                     istringstream sin(string(uhl._nLongitudeLines, sizeof(uhl._nLongitudeLines)));
                     sin >> _nLongitudes;
                  }
                  {
                     istringstream sin(string(uhl._nLatitudePosts, sizeof(uhl._nLatitudePosts)));
                     sin >> _nLatitudes;
                  }
                  // allocate an extra line of data at the top and bottom of the tiles
                  _elevation = new float[(_nLongitudes + 1) * (_nLatitudes + 1)];
               }

               ~DataRecord() throws()
               {
                  delete[] _elevation;
               }

               void read(istream& stream, const HeightMap& msl) throws(::std::exception)
               {
                  for (size_t lon = 0; lon < _nLongitudes; ++lon) {
                     const double lonRad = _west + lon * _deltaLon;

                     char ch = stream.get();
                     if (ch != static_cast< char>(0xaa)) {
                        throw ::std::runtime_error("Invalid recognition sentinel for data record : ");
                     }
                     char datablockCount[3];
                     stream.read(reinterpret_cast< char*>(&datablockCount), 3);
                     if (stream.fail()) {
                        throw ::std::runtime_error("Error reading a data block count");
                     }

                     UInt16 lonCount, latCount;
                     stream.read(reinterpret_cast< char*>(&lonCount), 2);
                     if (stream.fail()) {
                        throw ::std::runtime_error("Error reading a data record: longitude count");
                     }
                     stream.read(reinterpret_cast< char*>(&latCount), 2);
                     if (stream.fail()) {
                        throw ::std::runtime_error("Error reading a data record: latitude count");
                     }
                     for (size_t lat = 0; lat < _nLatitudes; ++lat) {
                        const double latRad = _south + lat * _deltaLat;

                        Int32 elHi, elLo;
                        elHi = static_cast< UInt8>(stream.get());
                        if (stream.fail()) {
                           throw ::std::runtime_error("Could not read elevation value");
                        }
                        elLo = static_cast< UInt8>(stream.get());
                        if (stream.fail()) {
                           throw ::std::runtime_error("Could not read elevation value");
                        }
                        float el;
                        if (elHi == 0xff && elLo == elHi) {
                           // a nul value; what to do?
                           Log("terra.dted.Tile").warn("DTED tile contains null elevation; setting elevation to 0");
                           el = 0;
                        }
                        else {
                           if (elHi & 0x80) {
                              el = ((elHi & 0x007f) << 8) | elLo;
                              el = -el;
                           }
                           else {
                              el = (elHi << 8) | elLo;
                           }
                        }
                        bool isValidMSL;
                        double mslHeight = msl.heightAboveEllipsoid(latRad, lonRad, isValidMSL);
                        if (isValidMSL) {
                           el += mslHeight;
                        }

                        // make sure all values of the tile have valid values
                        // this ensures that we won't have degeneracies at the left edge
                        // and top edge of the tile
                        _elevation[lat * (_nLongitudes + 1) + lon] = el;
                        _elevation[(lat + 1) * (_nLongitudes + 1) + lon] = el;
                        _elevation[(lat + 1) * (_nLongitudes + 1) + (lon + 1)] = el;
                        _elevation[lat * (_nLongitudes + 1) + (lon + 1)] = el;
                     }

                     Int32 chksum;
                     stream.read(reinterpret_cast< char*>(&chksum), 4);
                     if (stream.fail()) {
                        throw ::std::runtime_error("Could not read checksum");
                     }
                  }
               }

               size_t _nLongitudes;
               size_t _nLatitudes;
               double _west;
               double _south;
               double _deltaLat;
               double _deltaLon;
               float* _elevation;
         };
      }

      Tile::Tile() throws()
            : _minHeight(0), _maxHeight(0), _avgHeight(0), _heightError(0), _datum(GeodeticDatum::createWGS84()), _west(
                  0), _east(0), _south(0), _north(0), _deltaLat(0), _deltaLon(0), _nLongitudePosts(0), _nLatitudePosts(
                  0), _elevation(0)
      {
      }

      Tile::~Tile() throws()
      {
         delete[] _elevation;
      }

      Reference< Tile> Tile::read(istream& stream, const Reference< HeightMap>& msl) throws (exception)
      {
         UHL uhl;
         DSI dsi;
         ACC acc;
         assert(sizeof(UHL) == 80 - 4);
         assert(sizeof(DSI) == 648 - 3);
         assert(sizeof(ACC) == 2700 - 3);
         // search for "UHL1" token
         syncUHL1(stream);
         stream.read((char*) (&uhl), sizeof(UHL));
         if (stream.fail() || stream.gcount() != sizeof(UHL)) {
            throw ::std::runtime_error("Could not read UHL header");
         }
         match(stream, stringDSI, sizeof(stringDSI));
         stream.read((char*) (&dsi), sizeof(DSI));
         if (stream.fail() || stream.gcount() != sizeof(DSI)) {
            throw ::std::runtime_error("Could not read DSI header");
         }

         match(stream, stringACC, sizeof(stringACC));
         stream.read((char*) (&acc), sizeof(ACC));
         if (stream.fail() || stream.gcount() != sizeof(ACC)) {
            throw ::std::runtime_error("Could not read ACC header");
         }

         DataRecord data(uhl);
         data.read(stream, *msl);

         Reference< Tile> tile(new Tile());
         tile->_elevation = data._elevation;
         data._elevation = 0;
         tile->_nLatitudePosts = data._nLatitudes;
         tile->_nLongitudePosts = data._nLongitudes;
         tile->_minHeight = tile->_elevation[0];
         tile->_maxHeight = tile->_minHeight;
         tile->_avgHeight = tile->_minHeight;
         tile->_heightError = uhl.verticalAccuracy();
         tile->_deltaLat = uhl.latitudeInterval();
         tile->_deltaLon = uhl.longitudeInterval();
         tile->_west = uhl.west();
         tile->_south = uhl.south();
         tile->_east = tile->_west + tile->_nLongitudePosts * tile->_deltaLon;
         tile->_north = tile->_south + tile->_nLatitudePosts * tile->_deltaLat;

         // careful: we've got an extra top and right row of data, so the number
         // of points is increased!
         const size_t nPoints = (data._nLatitudes + 1) * (data._nLongitudes + 1);
         for (float* el = tile->_elevation; el != tile->_elevation + nPoints; ++el) {
            if (*el < tile->_minHeight) {
               tile->_minHeight = *el;
            }
            else if (*el > tile->_maxHeight) {
               tile->_maxHeight = *el;
            }
            tile->_avgHeight += *el;
         }
         // since the top and right rows are artificial, we cannot use them
         // in the average, so use the real number of points
         tile->_avgHeight /= data._nLatitudes * data._nLongitudes;

         LogEntry("terra.dted.Tile").debugging() << "Created dted tile " << "samples= (" << tile->_nLatitudePosts
               << ", " << tile->_nLongitudePosts << ") " << endl << "spacing = (" << toDegrees(tile->_deltaLat) << ", "
               << toDegrees(tile->_deltaLon) << ") " << endl << "sw = (" << toDegrees(tile->_south) << ", "
               << toDegrees(tile->_west) << ") " << endl << "ne = (" << toDegrees(tile->_north) << ", "
               << toDegrees(tile->_east) << ") " << endl << "initial sw elevation " << tile->_elevation[0] << endl
               << doLog;

         return tile;
      }

      Reference< GeodeticDatum> Tile::datum() const throws()
      {
         return _datum;
      }

      double Tile::heightAboveEllipsoid(double lat, double lon, bool& isValid) const throws ()
      {
         lon = clampLongitude(lon);
         // shift the point so we don't have to account for the southwest point
         // in the subsequent calculations
         lat -= _south;
         lon -= _west;

         if (lat < 0 || lon < 0) {
            isValid = false;
            return 0;
         }
         // compute s and w, which is the closest post (south-west of the given point)
         // we also compute dNS and dEW, which is a value between 0 and 1 and represents
         // the distance of the given point from the southwest point
         size_t s = static_cast< size_t>(lat / _deltaLat);
         size_t w = static_cast< size_t>(lon / _deltaLon);

         // check for validity of the region
         if (s >= _nLatitudePosts || w >= _nLongitudePosts) {
            isValid = false;
            return 0;
         }

         const double dNS = lat / _deltaLat - static_cast< double>(s);
         const double dEW = lon / _deltaLon - static_cast< double>(w);

         // get the heights of the four posts around the given point
         // because we've made the elevation array larger, we don't have to
         // worry about accessing invalid memory
         const float* sw = _elevation + s * (_nLongitudePosts + 1) + w;
         const float* nw = sw + (_nLongitudePosts + 1);
         const float* se = sw + 1;
         const float* ne = nw + 1;

         // do bilinear interpolation, first across west->east, then south->north
         double xnorth = (1 - dEW) * (*nw) + (dEW) * (*ne);
         double xsouth = (1 - dEW) * (*sw) + (dEW) * (*se);
         double center = (1 - dNS) * xsouth + (dNS) * xnorth;
         isValid = true;
         return center;
      }

      double Tile::heightError(double, double) const throws()
      {
         return _heightError;
      }

      double Tile::postHeight(size_t latPostIndex, size_t lonPostIndex) const throws()
      {
         assert(latPostIndex < _nLatitudePosts);
         assert(lonPostIndex < _nLongitudePosts);
         // the width of the elevation grid is +1
         return _elevation[latPostIndex * (_nLongitudePosts + 1) + lonPostIndex];
      }

      bool Tile::isPointInside(double lat, double lon) const throws()
      {
         lon = clampLongitude(lon);
         return _south <= lat && lat <= _north && _west <= lon && lon <= _east;
      }

   }
}
