#ifndef _TERRA_DTED_TERRAIN_H
#define _TERRA_DTED_TERRAIN_H

#ifndef _TERRA_HEIGHTMAP_H
#include <terra/HeightMap.h>
#endif

#ifndef _TERRA_BOUNDS_H
#include <terra/Bounds.h>
#endif

namespace terra {
  namespace dted {
    /** 
     * The Terrain class is container for DTED tiles. The tiles will be retrieved from 
     * structure layed out in the defined DTED directory structure. For details,
     * see the DTED manual.
     */
    class Terrain : public HeightMap {
      /** No copying */
    private:
      Terrain(const Terrain&);
      Terrain&operator=(const Terrain&);

      /** Default constructor */
    protected:
      Terrain() throws();

      /** Destructor */
    public:
      ~Terrain() throws();

      /**
       * Create a new terrain instance. The terrain is loaded
       * from standard dted distribution based at the given 
       * directory. The geoid should be a WGS84 spheroid,
       * but the code will work without such a spheroid. However, 
       * the data will not be accurate.
       * <p>
       * The bounds for the terrain should be slightly larger than the
       * area requested due to round-off errors in the conversion 
       * from degrees to radians. 
       *
       * @param bounds the terrain bounds
       * @param geoid the underlying geoid
       * @param dtedDir the dted base directory.
       * @return a terrain instance
       * @throws ::std::invalid_argument if the geoid and bounds have incompatible spheroids
       */
    public:
      static ::timber::Reference<Terrain> create (const Bounds& bounds,
						  const ::timber::Reference<HeightMap>& geoid,
						  const ::std::string& dtedDir) throws (::std::exception);

    };
  }
}
#endif
