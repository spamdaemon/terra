#include <terra/dted/Terrain.h>
#include <terra/Spheroid.h>
#include <terra/dted/Tile.h>
#include <terra/terra.h>
#include <timber/logging.h>
#include <canopy/fs/FileSystem.h>

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::timber::logging;

namespace terra {
   namespace dted {
      namespace {
         static double DEFAULT_HEIGHT_ERROR = 12000;

         static Pointer< Tile> loadTile(int lat, int lon, const Reference< HeightMap>& geoid, const string& dtedDir)
         {
            string lonStr, latStr;
            ostringstream stream;

            stream << (lon < 0 ? 'w' : 'e') << setw(3) << setfill('0') << ::std::abs(lon);
            lonStr = stream.str();
            stream.str("");
            stream << (lat < 0 ? 's' : 'n') << setw(2) << setfill('0') << ::std::abs(lat);
            latStr = stream.str();

            ::canopy::fs::FileSystem fs;
            string file = dtedDir + fs.directorySeparator() + lonStr + fs.directorySeparator() + latStr + ".dt0";
            try {
               if (fs.exists(file)) {
                  Log("terra.dted.Terrain").info("Loading tile from file '" + file + "'");
                  ifstream fileStream(file.c_str(), ios::binary);
                  return Tile::read(fileStream, geoid);
               }
            }
            catch (...) {
               Log("terra.dted.Terrain").warn("Could not load terrain from file '" + file + "'");
            }
            return Pointer< Tile> ();
         }

         struct TerrainImpl : public Terrain
         {
               TerrainImpl(const Reference< GeodeticDatum>& S, const vector< Reference< Tile> >& tiles)throws()
               : _datum(S)
               {
                  // initialize the bounds of the terrain from the first tile
                  {
                     const Tile& tile = *tiles[0];
                     _south = _north = static_cast<int>(toDegrees((tile.north()+tile.south())/2));
                     _east = _west = static_cast<int>(toDegrees((tile.east()+tile.west())/2));
                  }

                  // find the bounds of the terrain by expanding those of the first tile
                  for (size_t i=1;i<tiles.size();++i) {
                     const Tile& tile = *tiles[i];
                     _south = ::std::min(_south,static_cast<int>(toDegrees((tile.north()+tile.south())/2)));
                     _north = ::std::max(_north,static_cast<int>(toDegrees((tile.north()+tile.south())/2)));
                     _west = ::std::min(_west,static_cast<int>(toDegrees((tile.west()+tile.east())/2)));
                     _east = ::std::max(_east,static_cast<int>(toDegrees((tile.west()+tile.east())/2)));
                  }
                  // allocate the amount of tiles we need
                  _tiles.resize((_north-_south+1)*(_east-_west+1));

                  // now, set each tile
                  for (size_t i=0;i<tiles.size();++i) {
                     const Tile& tile = *tiles[i];
                     int lat = static_cast<int>(toDegrees((tile.north()+tile.south())/2)-_south);
                     int lon = static_cast<int>(toDegrees((tile.west()+tile.east())/2)-_west);
                     assert(lat>=0 && lon>=0);
                     size_t j = checked_cast<size_t>(lat * (_east-_west+1) +lon);
                     assert(j<_tiles.size());
                     _tiles[j] = tiles[i];
                  }
               }

               ~TerrainImpl()throws()
               {
               }

               Reference< GeodeticDatum> datum() const throws()
               {  return _datum;}

               double heightAboveEllipsoid(double lat, double lon, bool& isValidHeight) const throws ()
               {
                  int latIndex = static_cast<int>(toDegrees(lat)-_south);
                  int lonIndex = static_cast<int>(toDegrees(lon)-_west);
                  if (latIndex>=0 && lonIndex>=0) {
                     size_t j = checked_cast<size_t>(latIndex * (_east-_west+1) + lonIndex);
                     if (_tiles[j]) {
                        return _tiles[j]->heightAboveEllipsoid(lat,lon,isValidHeight);
                     }
                  }
                  isValidHeight = false;
                  return 0;
               }

               double heightError(double lat, double lon) const throws()
               {
                  int latIndex = static_cast<int>(toDegrees(lat)-_south);
                  int lonIndex = static_cast<int>(toDegrees(lon)-_west);
                  if (latIndex>=0 && lonIndex>=0) {
                     size_t j = static_cast<size_t>(latIndex * (_east-_west+1) + lonIndex);
                     if (_tiles[j]) {
                        return _tiles[j]->heightError(lat,lon);
                     }
                  }
                  return DEFAULT_HEIGHT_ERROR;
               }

            private:
               const Reference< GeodeticDatum> _datum;
               vector< Pointer< Tile> > _tiles;
               int _south, _west, _north, _east;
         };
      }

      Terrain::Terrain()
      throws() {}
      Terrain::~Terrain()
      throws() {}

      Reference< Terrain> Terrain::create(const Bounds& bounds, const Reference< HeightMap>& geoid,
            const string& dtedDir)
      throws (::std::exception)
      {
         if (!geoid->datum()->equals(*bounds.datum())) {
            throw ::std::invalid_argument("Geoid and Bounds have incompatible datums");
         }

         // check that the director exists
         ::canopy::fs::FileSystem fs;
         if (!fs.exists(dtedDir) || !fs.isDirectory(dtedDir)) {
            throw ::std::runtime_error("Directory not found : "+dtedDir);
         }

         vector< Reference< Tile> > tiles;
         // load all tiles
         double west = floor(toDegrees(bounds.west()));
         double east = floor(toDegrees(bounds.east()));
         double south = floor(toDegrees(bounds.south()));
         double north = floor(toDegrees(bounds.north()));

for      (double lat = south;lat <= north; lat += 1) {
         for (double lon=west;lon <= east; lon += 1) {
            Pointer<Tile> t = loadTile(static_cast<int>(lat),static_cast<int>(lon),geoid,dtedDir);
            if (t) {
               tiles.push_back(t);
            }
         }
      }
      LogEntry("terra.dted.Terrain").info() << "Loaded " << tiles.size() << " dted tiles" << doLog;
      return new TerrainImpl(bounds.datum(),tiles);
   }

}

}
