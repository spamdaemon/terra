#ifndef _TERRA_DTED_TILE_H
#define _TERRA_DTED_TILE_H

#ifndef _TERRA_HEIGHTMAP_H
#include <terra/HeightMap.h>
#endif

#include <iosfwd>
#include <cassert>

namespace terra {
  namespace dted {

    /**
     * A single dted tile implements the HeightMap interface over a rectangular
     * region. By spec, all heights contained in a DTED file are with respect
     * to mean-sea level. 
     */
    class Tile : public HeightMap {
      /** Copying disabled */
      Tile(const Tile&);
      Tile&operator=(const Tile&);

      /**
       * Constructor is private.
       */
    private:
      Tile() throws();

      /**
       * Destructor
       */
    public:
      ~Tile() throws();

      /**
       * Read a new tile from an input stream containing a single
       * tile at the current stream position.
       * @param stream an input stream
       * @param msl a height map defininig the mean-sea-level.
       * @return a tile
       * @throws ::std::exception if some kind of error occurred
       */
    public:
      static ::timber::Reference<Tile> read(::std::istream& stream, const ::timber::Reference<HeightMap>& msl) throws (::std::exception);
      
      /**
       * Determine if the specified point falls within the bounds of this tile.
       * @param lat latitude in radians
       * @param lon longitude in radians
       * @return true if the the point falls within this tile
       */
    public:
      bool isPointInside (double lat, double lon) const throws();

      /**
       * Get the minimum height above the ellipsoid within this tile.
       * @return the minimum height
       */
    public:
      inline double minHeightAboveEllipsoid() const throws()
      { return _minHeight; }

      /**
       * Get the minimum height above the ellipsoid within this tile.
       * @return the maximum height
       */
    public:
      inline double maxHeightAboveEllipsoid() const throws()
      { return _maxHeight; }

      /**
       * Get the average height above the ellipsoid within this tile.
       * @return the average height
       */
    public:
      inline double avgHeightAboveEllipsoid() const throws()
      { return _avgHeight; }

      /**
       * Get the longitude of the western tile edge.
       * @return the longitude of western tile edge
       */
    public:
      inline double west() const throws()
      { return _west; }

      /**
       * Get the longitude of the eastern tile edge.
       * @return the longitude of eastern tile edge
       */
    public:
      inline double east() const throws()
      { return _east; }

      /**
       * Get the latitude of the northern tile edge.
       * @return the latitude of northern tile edge
       */
    public:
      inline double north() const throws()
      { return _north; }

      /**
       * Get the latitude of the southern tile edge.
       * @return the latitude of southern tile edge
       */
    public:
      inline double south() const throws()
      { return _south; }

      /**
       * Get the number of posts in latitude.
       * @return the number of latitude points per longitude
       */
    public:
      inline size_t numLatitudePosts() const throws()
      {return _nLatitudePosts; }

      /**
       * Get the number of posts in longitude.
       * @return the number of longitude posts
       */
    public:
      inline size_t numLongitudePosts() const throws()
      {return _nLongitudePosts; }
      
      /**
       * Get the elevation values for a given post.
       * @param latPostIndex the index of a latitude post
       * @param lonPostIndex the index of a longitude post
       * @return the height at the specified post
       * @pre latPostIndex < numLatitudePosts()
       * @pre lonPostIndex < numLongitudePosts()
       */
    public:
      double postHeight (size_t latPostIndex, size_t lonPostIndex) const throws();
      
      /**
       * Get the underlying datum. All height queries return height
       * values with respect to this datum.
       * @return the datum
       */
    public:
      ::timber::Reference<GeodeticDatum> datum() const throws();
      
      /**
       * Get the height above the ellipsoid for a point on this height map.
       * 
       * @param lat the latitude of a point in radians
       * @param lon the longitude of a point in radians
       * @param isValid true if the return height is valid, false otherwise
       * @return the height above the ellipsoid (in meters)
       * @throws ::std::exception if point is outside this heightmap or lat is invalid
       */
    public:
      double heightAboveEllipsoid(double lat, double lon, bool& isValid) const throws ();

      /**
       * Get the error in height that is expected at the specified latitude and longitude.
       * @param lat the latitude of a point in radians
       * @param lon the longitude of a point in radians
       * @return the 1-sigma error in height above the ellipsoid, or 0 if the location is an invalid one
       */
    public:
      double heightError (double lat, double lon) const throws();

      /** The min, max, and average heights */
    private:
      double _minHeight,_maxHeight,_avgHeight;

      /** The height error */
    private:
      double _heightError;

      /** The frame */
    private:
      ::timber::Reference<GeodeticDatum> _datum;

      /** The western longitude (degrees) */
    private:
      double _west;

      /** The eastern longitude (degrees) */
    private:
      double _east;

      /** The south latitude (degrees) */
    private:
      double _south;

      /** The north latitude (degrees) */
    private:
      double _north;

      /** The size of a grid cell in latitude degrees */
    private:
      double _deltaLat;
      
      /** The the size */
    private:
      double _deltaLon;

      /** The number of points in longitude */
    private:
      size_t _nLongitudePosts;

      /** The number of points in latitude */
    private:
      size_t _nLatitudePosts;

      /** The elevation data (float precision is sufficient) */
    private:
      float* _elevation;
    };

  }
}

#endif
