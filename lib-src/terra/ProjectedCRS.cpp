#include <terra/ProjectedCRS.h>
#include <terra/GeodeticDatum.h>
#include <terra/Spheroid.h>

#include <cmath>

namespace terra {

   ProjectedCRS::ProjectedCRS()
   throws()
   {}

   ProjectedCRS::~ProjectedCRS()
   throws()
   {}

   ::timber::Reference< ProjectedCRS> ProjectedCRS::createEPSG3857()
throws()
{
   struct _ : public ProjectedCRS {
      _() throws() : _datum(GeodeticDatum::createEPSG6326()),_sphere(Spheroid::createSphere(6378137,0,0,0)) {}
      ~_() throws() {}

      ::timber::Reference<Datum> datum() const throws()
      {
         return _datum;
      }
      EpsgCode epsgCode() const throws() {return EpsgCode(EpsgCode::CRS,3857);}

      size_t coordinateCount() const throws() {return 2;}

      double distance (const double* p1, const double* p2) const throws()
      {
         const double p[] = { p1[0]-p2[0], p1[1]-p2[1] };
         return ::std::sqrt(p[0]*p[0] + p[1]*p[1]);
      }


      bool toEPSG4979(const double* coords, EPSG4979Coordinates& wgs84) const throws()
      {
         // we just do an inverse mercator and then treat the result as if it were with respect
         // to the EPSG:7030 ellipsoid; kind of bogus
         _sphere->computeInverseMercatorProjection(0,coords[0],coords[1],wgs84.lat,wgs84.lon);
         wgs84.alt =0;
         return validateEPSG4979Coordinates(wgs84);
      }

      bool fromEPSG4979(const EPSG4979Coordinates& wgs84, double* coords) const throws()
      {
         // treat the coordinates as if they were provided in this reference frame's datum.
         _sphere->computeMercatorProjection(0,wgs84.lat,wgs84.lon,coords[0],coords[1]);
         return true;
      }

      bool equals (const CoordinateReferenceSystem& e) const throws()
      {  return dynamic_cast<const _*>(&e) != 0;}

      /** The wgs 84 datum */
      private:
      ::timber::Reference<GeodeticDatum> _datum;

      /** The spheroid used for projection */
      private:
      ::timber::Reference<Spheroid> _sphere;
   };

   ProjectedCRS* crs = new _();
   return ::timber::Reference< CoordinateReferenceSystem>(crs);
}

}
