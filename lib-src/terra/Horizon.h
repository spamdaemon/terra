#ifndef _TERRA_HORIZON_H
#define _TERRA_HORIZON_H

#ifndef _TERRA_TOPOCENTRICCOORDINATES_H
#include <terra/TopocentricCoordinates.h>
#endif

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

namespace terra {

  /**
   * This class can represent the visible horizon of an observer.
   */
  class Horizon {
    
    /**
     * The default constructor
     */
  public:
    Horizon() throws();

    /**
     * Create a new horizon
     * @param obs the location of the observer
     */
  public:
    Horizon (const GeodeticCoordinates& obs) throws();

    /**
     * Destructor
     */
  public:
    ~Horizon() throws();

    /**
     * Get the point.
     * @return the observer point
     */
  public:
    const GeodeticCoordinates& observer() const throws();

    /**
     * Get the distance to horizon travelling in the specified direction
     * If the observer is below the spheroid, then 0 is returned.
     * If the distance could not be computed for some reason, then -1 is returned.
     * @param az an angle in radians
     * @return the distance in meters to the horizon 
     * @note the current implementation utilizes an iterative approach; future versions may utilize a different approach.
     * @throws ::std::exception if the distance could not be computed
     */
  public:
    double distance (double az) const throws (::std::exception);

    /**
     * Compute a point on the horizon in the specified direction.
     * @param az the look direction in radians
     * @return a point on the horizon
     * @throws ::std::exception if the point could not be computed
     */
  public:
    GeodeticCoordinates point (double az) const throws (::std::exception);


    /** The location */
  private:
    TopocentricCoordinates _observer;
  };
}

#endif
