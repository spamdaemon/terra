#ifndef _TERRA_PROJECTEDCRS_H
#define _TERRA_PROJECTEDCRS_H

#ifndef _TERRA_COORDINATERFERENCESSYSTEM_H
#include <terra/CoordinateReferenceSystem.h>
#endif

namespace terra {

   /**
    * A coordinate reference system provides information about Location objects. The only method
    * functionality provided by this class is the ability to produce a GeoLocation object in the
    * EPSG4326 reference system.
    * More about EPSG can be found <a href="http://www.epsg-registry.org/">here</a>.
    */
   class ProjectedCRS : public CoordinateReferenceSystem
   {
         ProjectedCRS(const ProjectedCRS&);
         ProjectedCRS&operator=(const ProjectedCRS&);

         /**
          * Default constructor.
          */
      protected:
         ProjectedCRS()throws();

         /** Destructor */
      public:
         ~ProjectedCRS()throws();

         /**
          * Create the EPSG:3857 projected coordinate reference system. This CRS is also known as EPSG:900913.
          * This is the coordinate reference system used by <a href="http://www.openstreetmap.org/">OpenStreetMap</a> and
          * <a href="maps.google.com">Google Maps</a> and others.
          * <p>
          * This CRS is special and a little bit bogus. Basically, the CRS is a Mercator projection with respect to
          * a sphere, not ellipsoid. But the geographic coordinates are treated as if they were with respect to WGS84.
          * In effect this means that the datum for projection is different from the datum when not projected.<br>
          * This is general with this kind of projection, as can be seen
          * <a href="http://proj.maptools.org/faq.html#sphere_as_wgs84">here</a>.
          * <p>
          * The returned CRS instance will have as its datum the WGS84 ellipsoid and <em>NOT</em> the one
          * specified by EPSG.
          * @return the EPSG3857 coordinate system
          */
      public:
         static ::timber::Reference<ProjectedCRS> createEPSG3857() throws();

   };
}

#endif
