#ifndef _TERRA_LOCATION_H
#define _TERRA_LOCATION_H

#ifndef _TERRA_COORDINATERFERENCESSYSTEM_H
#include <terra/CoordinateReferenceSystem.h>
#endif 

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

#ifndef _TERRA_CONVERSIONFAILURE_H
#include <terra/ConversionFailure.h>
#endif

#include <iosfwd>

namespace terra {

   /**
    * Location is the baseclass for point location objects.
    */
   class Location
   {
         /**
          * Create the location (0,0,0) in the WGS84 coordinate system.
          */
      public:
         Location()throws();

         /**
          * Copy constructor.
          */
      public:
         Location(const Location& src)throws();

         /**
          * Create a default location in a given coordinate reference system. The size of the
          * coords array must of course equal the number of coordinates required by the coordinate system.
          * @param coords the coordinates
          * @param crs the coordinate reference system
          */
      public:
         Location(const double* coords, const ::timber::Reference< CoordinateReferenceSystem>& crs)throws();

         /**
          * Create a default location in a given coordinate reference system. The size of the
          * coords array must of course equal the number of coordinates required by the coordinate system.
          * <p>
          * The meanings of the values of x1,x2 depend on the CRS.
          * @param x1 the first axis
          * @param x2 the second axis
          * @param crs the coordinate reference system
          * @pre crs->coordinateCount()==2
          */
      public:
         Location(double x1, double x2, const ::timber::Reference< CoordinateReferenceSystem>& crs)throws();

         /**
          * Create a default location in a given coordinate reference system. The size of the
          * coords array must of course equal the number of coordinates required by the coordinate system.
          * <p>
          * The meanings of the values of x1,x2,x3 depend on the CRS.
          * @param x1 the first axis
          * @param x2 the second axis
          * @param x3 the third axis
          * @param crs the coordinate reference system
          * @pre crs->coordinateCount()==3
          */
      public:
         Location(double x1, double x2, double x3, const ::timber::Reference< CoordinateReferenceSystem>& crs)throws();

         /**
          * Create a default location in a given coordinate reference system.
          * @param crs the coordinate reference system
          */
      private:
         Location(const ::timber::Reference< CoordinateReferenceSystem>& crs)throws();

         /**
          * Create a location object corresponding to the specified
          * geodetic coordinates.
          * @param coords the geodetic coordinates
          */
      public:
         Location(const GeodeticCoordinates& coords)throws();

         /**
          * Create a location for the specified coordinates.
          * @param epsg the epsg4979 coordinates
          */
      public:
         Location(const CoordinateReferenceSystem::EPSG4979Coordinates& epsg)throws();

         /** Destructor */
      public:
         ~Location()throws();

         /**
          * Copy operator.
          */
      public:
         Location& operator=(const Location& src)throws();

         /**
          * Copy operator.
          */
      public:
         inline Location& operator=(const CoordinateReferenceSystem::EPSG4979Coordinates& epsg)throws()
         {
            setEpsg4979Coordinates(epsg);
            return *this;
         }

         /**
          * Convert this location to the specified crs.
          * @param crs the target crs
          * @return a new location
          * @throws ConversionFailure if the conversion fails
          */
      public:
         Location convert(const ::timber::Reference< CoordinateReferenceSystem>& crs) const
         throws (ConversionFailure);

         /**
          * Get the location in terms of its epsg coordinates.
          * @param epsg the output coordinates
          */
      public:
         void epsg4979Coordinates(CoordinateReferenceSystem::EPSG4979Coordinates& epsg) const throws();

         /**
          * Replace this location with the given epsg coordinates. This method
          * replaces both the CRS and the coordinates.
          * @param epsg the new coordinates
          */
      public:
         void setEpsg4979Coordinates(const CoordinateReferenceSystem::EPSG4979Coordinates& epsg)throws();

         /**
          * Get the geodetic coordinates for this location with respect to the EPSG 7030 spheroid.
          * @return the GeodeticCoordinates in EPSG4979.
          * @see Spheroid#getEPSG7030
          */
      public:
         GeodeticCoordinates epsg4979Coordinates() const throws();

         /**
          * Update this location with the specified coordinates. If the coordinates fall
          * outside the bounds of the coordinate frame, then false is returned and the point
          * is not updated.
          * @param coords the new coordinates
          * @return true if this location was updated, false otherwise.
          */
      public:
         bool setPosition(const double* coords)throws();

         /**
          * Update this location from the specified wgs84 coordinates. If the wgs84 coordinates
          * cannot be mapped into this location's coordinate reference system, then this method
          * returns false. This method is equivalent to  <tt>setPosition(Location(geo))</tt>.
          * @param geo geodetic coordinates
          * @return true if this location was updated, false otherwise.
          */
      public:
         bool setPosition(const GeodeticCoordinates& geo) throws();

         /**
          * Update this location from specified location. This method does not change the
          * CRS of this location. It is possible that the specified location cannot be representing
          * in this location's coordinate system, in which case this method returns false.
          * @param geo geodetic coordinates
          * @return true if this location was updated, false otherwise.
          */
      public:
         bool setPosition(const Location& loc) throws();

         /**
          * Update this location by converting it to the specified coordinate system. If
          * the conversion fails, then this method returns false.
          * @param newCrs the new coordinate reference system
          * @return true if this location object was updated, false otherwise
          */
      public:
         bool setCRS(const ::timber::Reference< CoordinateReferenceSystem>& newCrs) throws();

         /**
          * Get the coordinate reference system for this location.
          * @return the spheroid
          */
      public:
         inline const ::timber::Reference< CoordinateReferenceSystem>& crs() const throws() {return _crs;}

         /**
          * Compute the distance from these coordinates to the specified coordinates. This
          * method is implemented via the CoordinateReferenceSystem::distance() method.
          * @param p another coordinates object
          * @return a distance metric (-1 if coordinate reference systems are incompatible)
          */
      public:
         double distance(const Location& loc) const throws();

         /**
          * Get the number of values representing this location.
          * @return the number of coordinates needed to represent this location.
          */
      public:
         inline size_t coordinateCount() const throws()
         {  return _crs->coordinateCount();}

         /**
          * Get the i'th coordinate of this location. What the value actually means
          * depends on the CoordinateReferenceSystem.
          * @param i a coordinate index between or 0 and <tt>coordinateCount()-1</tt> inclusively.
          * @return the coordinate
          */
      public:
         double coordinate(size_t i) const throws()
         {
            assert(i<_crs->coordinateCount());
            return _coords[i];
         }

         /**
          * Access the i'th coordinate.
          * @param i the coordinate index
          * @return a coordinate
          */
      public:
         inline double operator[](size_t i) const throws() {return coordinate(i);}

         /**
          * Compare two Locations. Two locations are only equal if their coordinate
          * reference systems are equal and the coordinates are equal.
          * @param pt a coordinates object
          * @return true if the coordinates are exactly the same.
          */
      public:
         bool equals(const Location& pt) const throws();

         /**
          * Compare if two coordinates are actually the same point.  Two locations are always equal to each if
          * their geodetic coordinates refer to the same location.
          * @param pt a coordinates object
          * @return true if this point and p are really the same location
          */
      public:
         bool isSameLocation(const Location& pt) const throws();

         /**
          * Compare two Location.
          */
      public:
         inline bool operator==(const Location& pt) const throws()
         {  return equals(pt);}

         /**
          * Compare two Location.
          */
      public:
         inline bool operator!=(const Location& pt) const throws()
         {  return !equals(pt);}

         /** The coordinates */
      private:
         double* _coords;

         /** The crs */
      private:
         ::timber::Reference< CoordinateReferenceSystem> _crs;
   };

}

/**
 * Print a Location object to a stream
 * @param out a stream
 * @param l a Location
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::terra::Location& l);

#endif
