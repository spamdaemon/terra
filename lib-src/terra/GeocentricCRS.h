#ifndef _TERRA_GEOCENTRICCRS_H
#define _TERRA_GEOCENTRICCRS_H

#ifndef _TERRA_GEODETICCRS_H
#include <terra/GeodeticCRS.h>
#endif

namespace terra {
   /**
    * A coordinate reference system provides information about Location objects. The only method
    * functionality provided by this class is the ability to produce a GeoLocation object in the
    * EPSG4326 reference system.
    * More about EPSG can be found <a href="http://www.epsg-registry.org/">here</a>.
    */
   class GeocentricCRS : public GeodeticCRS
   {
      private:
      GeocentricCRS(const GeocentricCRS&);
      GeocentricCRS&operator=(const GeocentricCRS&);

         /**
          * Create a new frame.
          * @param s the frame
          */
      protected:
      GeocentricCRS()throws();

         /** Destructor */
      public:
         ~GeocentricCRS()throws();
   };
}

#endif
