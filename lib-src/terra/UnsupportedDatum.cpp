#include <terra/UnsupportedDatum.h>
#include <string>
#include <sstream>

namespace terra {

   namespace {
      static ::std::string getExceptionString(const ::timber::Reference< Datum>& datum,
            const ::std::vector< ::timber::Reference< Datum> >& s = ::std::vector< ::timber::Reference< Datum> >())
      {
         ::std::ostringstream out;
         out << "Datum unsupported : " << typeid(*datum).name() << ". ";
         bool printHeader = true;
         for (auto c = s.begin(); c != s.end(); ++c) {
            // it it's an undefined CRS, then there isn't much point in printing that
            if (printHeader) {
               printHeader = false;
               out << "Supported datums include: [";
            }
            out << ' ' << typeid(*c).name();
         }
         if (!printHeader) {
            out << " ] ";
         }
         return out.str();
      }
   }

   UnsupportedDatum::UnsupportedDatum(const ::timber::Reference< Datum>& datum,
         ::std::vector< ::timber::Reference< Datum> >& s) :
      ::std::runtime_error(getExceptionString(datum, s)), _unsupported(datum), _supported(s)
   {
   }

   UnsupportedDatum::UnsupportedDatum(const ::timber::Reference< Datum>& datum) :
      ::std::runtime_error(getExceptionString(datum)), _unsupported(datum)
   {
   }

   UnsupportedDatum::UnsupportedDatum(const ::timber::Reference< Datum>& datum, const ::timber::Reference< Datum>& s) :
      ::std::runtime_error(getExceptionString(datum)), _unsupported(datum), _supported(1, s)
   {
   }

   UnsupportedDatum::~UnsupportedDatum() throw()
   {
   }

}
