#include <terra/Horizon.h>
#include <terra/Geodesic.h>
#include <cmath>

#include <timber/logging.h>

using namespace ::timber;
using namespace ::timber::logging;

namespace terra {

  Horizon::Horizon() throws()
  : _observer(GeodeticCoordinates())
  {}

  Horizon::Horizon (const GeodeticCoordinates& obs) throws()
  : _observer(obs)
  {}

  Horizon::~Horizon() throws()
  {}

  const GeodeticCoordinates& Horizon::observer() const throws()
  { return _observer.frame()->origin(); }

  double Horizon::distance (double az) const throws(::std::exception)
  {
    const GeodeticCoordinates& obs = _observer.frame()->origin();
    if (obs.height()<=0.0) {
      return 0.0;
    }
    double tmp[2];
    
    GeodeticCoordinates p1 = obs;
    GeodeticCoordinates p2 = obs.antipodalCoordinates();
    double d1 =0;
    double d2 = obs.datum()->spheroid().computeGeodesic(obs.latitude(),obs.longitude(),p2.latitude(),p2.longitude(),tmp[0],tmp[1]);

    //  this is a bisection approach 
    double r=0;
    for (size_t i=0;i<48;++i) {
      // compute a point between p1 and p2
      r = (d1+d2)/2;
      
      double lat,lon;
      obs.datum()->spheroid().travelAlongGeodesic(obs.latitude(),obs.longitude(),az,r,lat,lon);
      GeodeticCoordinates p(lat,lon,obs.datum());
      TopocentricCoordinates loc(p);

      TopocentricCoordinates tc(loc.frame(),obs);
      if (::std::abs(tc.z()) < .1) {
	return r;
      }
      if (tc.z() < 0) {
	p2 = p;
	d2 = r;
      }
      else {
	p1 = p;
	d1 = r;
      }
    }
    // exceeded the number of iterations that we want
    throw ::std::runtime_error("Could not compute distance to horizon : number of iterations exeeded limit");
  }

  GeodeticCoordinates Horizon::point (double az) const throws(::std::exception)
  {
    // first, compute the distance to the point
    const double d = distance(az);

    const GeodeticCoordinates& obs = observer();

    double lat,lon;
    if (::std::abs(::std::abs(obs.latitude())-PI/2) < 1E-6) {
      if (obs.latitude() < 0 ) { // southpole
	obs.datum()->spheroid().travelAlongMeridianArc(-PI/2,0,clampLongitude(az),d,lat,lon);
      }
      else { // northpole
	obs.datum()->spheroid().travelAlongMeridianArc(PI/2,0,clampLongitude(az),d,lat,lon);
      }
    }
    else {
      // any other location
      obs.datum()->spheroid().travelAlongGeodesic(obs.latitude(),obs.longitude(),az,d,lat,lon);
    }
    return GeodeticCoordinates(lat,lon,0,obs.datum());
  }

 
}
