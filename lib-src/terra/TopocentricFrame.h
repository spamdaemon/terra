#ifndef _TERRA_TOPOCENTRICFRAME_H
#define _TERRA_TOPOCENTRICFRAME_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif 

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif 

#ifndef _TERRA_PROJECTEDCRS_H
#include <terra/ProjectedCRS.h>
#endif

#ifndef _NEWTON_MATRIX_H
#include <newton/Matrix.h>
#endif 

namespace terra {
   class TopocentricCoordinates;
   class ECEFCoordinates;

   /**
    * The topocentric frame is a generalized form of an earth-centered, earth-fixed
    * Cartesian coordinate frame. The origin of the topocentric frame is an arbitrary point
    * with respect to the surface of the spheroid. The topocentric frame is not a projection
    * and can thus represent all points on the surface of the earth. The topocentric frame
    * may be though of as a plane tangent to the spheroid at some point, such that the positive x-axis points
    * due east, the positive  y-axis points due north, and the positive z-axis points up. This is also known as a NEU
    * (north-east-up) frame. If the plane's tangent point is at the north pole, then the local z axis coincides
    * with the z-axis of the ECEF frame, the y-axis is parallel to y-axis of the ecef frame, as is the x-axis.
    * If the plane's tangent point is a the south pole, then the local z-axis coincides with the negative of the
    * ecef z-axis (x and y axis are also reversed).<p>
    * TODO: implement the CRS properly, i.e. the coordinate frame of the center point is really used and not necessarily wgs84.
    */
   class TopocentricFrame : public ProjectedCRS
   {
         TopocentricFrame&operator=(const TopocentricFrame&);
         TopocentricFrame(const TopocentricFrame&);

         /**
          * Constructor
          * @param ctr the center of the coordinate frame
          */
      private:
         TopocentricFrame(const GeodeticCoordinates& ctr)throws();

         /** destructor */
      public:
         ~TopocentricFrame()throws();

         /**
          * Get the datum for this CRS.
          * @return the datum of the center point
          */
      public:
         ::timber::Reference<Datum> datum() const  throws();

         /**
          * Compute the distance between two points on this frame.
          * @param p1 a point
          * @param p2 a point
          * @return the Euclidean distance between p1 and p2
          */
      public:
         double distance (const double* p1, const double* p2) const throws();

         /**
          * Test if two CRS are the same.
          * @param e another frame
          * @return true if e and this are the same frame
          */
      public:
         bool equals(const CoordinateReferenceSystem& e) const throws();

         /**
          * Create a new topocentric frame at GeodeticCoordinates().
          * @return a topocentric frame
          */
      public:
         static ::timber::Reference< TopocentricFrame> create()throws();

         /**
          * Create a new topocentric frame.
          * @param ctr the coordinate frame center
          * @return a topocentric frame
          */
      public:
         static ::timber::Reference< TopocentricFrame> create(const GeodeticCoordinates& ctr)throws();

         /**
          * Get the origin of this frame
          * @return the origin
          */
      public:
         inline const GeodeticCoordinates& origin() const throws()
         {  return _center;}

         /**
          * @name These methods are meant to be used from TopocentricCoordinates().
          * @{
          */

         /**
          * Transform the specified ecef point into a point in topocentric coordinates
          * @param ecef a point in ecef coordinates
          * @param res an output vector of size 3
          */
      public:
         void transformFromECEF(const ECEFCoordinates& ecef, double* res) const throws();

         /**
          * Transform the specified ecef point into a point in topocentric coordinates
          * @param geo a point in geodetic coordinates
          * @param res an output vector of size 3
          */
      public:
         void transformFromGeodetic(const GeodeticCoordinates& geo, double* res) const throws();

         /**
          * Transform the specified tcs point into a point in ecef coordinates
          * @param tcs a point in tcs coordinates
          * @param res an output vector of size 3 (x,y,z)
          */
      public:
         static void transformToECEF(const TopocentricCoordinates& tcs, double* res)throws();

         /**
          * Transform the specified tcs point into a point in geodetic coordinates
          * @param tcs a point in tcs coordinates
          * @param res an output vector of size 3 (lat,lon,alt)
          */
      public:
         static void transformToGeodetic(const TopocentricCoordinates& tcs, double* res)throws();

         /**
          * Transform the specified tcs point into a point in ecef coordinates
          * @param tcs a point in tcs coordinates
          * @param res an output vector of size 3 (x,y,z)
          */
      public:
         void transformToECEF(const double* tcs, double* res) const throws();

         /**
          * Transform the specified tcs point into a point in geodetic coordinates
          * @param tcs a point in tcs coordinates
          * @param res an output vector of size 3 (lat,lon,alt)
          */
      public:
         void transformToGeodetic(const double* tcs, double* res) const throws();

         /*@}*/

         /**
          * Get the rotation matrix that rotates a vector in the topocentric frame
          * into a vector in the ECEF frame.
          * @return a matrix
          */
      public:
         ::newton::Matrix< double> getRotationToECEF() const throws();

         /**
          * Get the rotation that rotates a point in ecef into this frame
          * @return a rotation matrix
          */
      public:
         ::newton::Matrix< double> getRotationFromECEF() const throws();

         /**
          * Overrides
          */
      public:
         size_t coordinateCount() const throws();
         bool toEPSG4979(const double* coords, EPSG4979Coordinates& wgs84) const throws();
         bool fromEPSG4979(const EPSG4979Coordinates& wgs84, double* coords) const throws();
         EpsgCode epsgCode() const throws();

         /**
          * Convert from a geodetic coordinate frame.
          * @param geo the geodetic coordinates
          * @param tcs the tcs coordinates
          */
      private:
         void transformFromGeodetic(const double* geo, double* tcs) const throws();

         /**
          * Convert from a geodetic coordinate frame.
          * @param geo the geodetic coordinates
          * @param tcs the tcs coordinates
          */
      private:
         void transformFromECEF(const double* ecef, double* tcs) const throws();

      private:
         const GeodeticCoordinates _center;

         /** The meridian radius at the center */
      private:
         const double _meridianRadius;

         /** The circle radius at the center */
      private:
         const double _circleRadius;

         /** The circle * center.longitude() */
      private:
         double _centerXcircleRadius;

         /** The meridian radius * center.latitude() */
      private:
         double _centerXmeridianRadius;

         /** The ecef coordinates of the geodetic coordinates */
      private:
         double _ecefOrigin[3];
         /**
          * The 3x3 rotation matrix that transform an ECEF point into TCS;
          * the inverse of this matrix is actually its transpose!
          */
      private:
         double _ecef2tcs[9];
   };

}

#endif
