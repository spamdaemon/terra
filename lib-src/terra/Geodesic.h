#ifndef _TERRA_GEODESIC_H
#define _TERRA_GEODESIC_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif

#include <iosfwd>

namespace terra {
  
  /** Forward declare geodesic coordinates */
  class GeodeticCoordinates;
  
  /**
   * This class is used to represent a geodesic path between two geodetic locations.
   * A geodesic is specified by a range and an intitial bearing. Travel along the
   * geodesic will lead to the closest point on the spheroid, but the bearing at
   * the destination may be different from the initial bearing. If an arc of
   * constant bearing is required, a Loxodrome should be used.
   */
  class Geodesic {
      
    /**
     * Create a new geodesic arc.
     * @param r the range value (in meters)
     * @param az the initial bearing (in radians)
     */
  public:
    Geodesic (double r, double az) throws();

    /**
     * Create a default geodesic. This equivalent to <code>Geodesic(0,0)</code>.
     */
  public:
    inline Geodesic () throws()
      : _range(0),_bearing(0) {}

    /**
     * Compute the shortest geodesic between two points.
     * @param from the start point
     * @param to the end point
     * @pre REQUIRE_EQUALS(to.datum(),from.datum())
     */
  public:
    Geodesic (const GeodeticCoordinates& from, const GeodeticCoordinates& to) throws();

    /**
     * Get the range. The range is always positive.
     * @return the range in meters.
     */
  public:
    inline double range () const throws() { return _range; }

    /**
     * Get the initial bearing.
     * @return the initial bearing in radians
     */
  public:
    inline double bearing () const throws() { return _bearing; }

    /**
     * Move along this geodesic from the specified starting point.
     * @param p the start point
     * @return the point at which this geodesic ends if starting at p
     */
  public:
    GeodeticCoordinates move (const GeodeticCoordinates& p) const throws ();

    /**
     * Print this geodesic to a stream
     * @param out an output stream
     * @return out
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;

    /** The range */
  private:
    double _range;

    /** The initial bearing in radians */
  private:
    double _bearing;
  };
}

/**
 * Print a geodesic  to an output stream
 * @param out an output stream
 * @param rb a geodesic
 * @return out
 */
inline ::std::ostream& operator<< (::std::ostream& out, const ::terra::Geodesic& rb)  
{ rb.print(out); return out; }

#endif
