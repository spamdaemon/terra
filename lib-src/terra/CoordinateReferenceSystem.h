#ifndef _TERRA_COORDINATEREFERENCESYSTEM_H
#define _TERRA_COORDINATEREFERENCESYSTEM_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TERRA_EPSGCODE_H
#include <terra/EpsgCode.h>
#endif

namespace terra {
   class Datum;

   /**
    * A coordinate reference system provides information about Location objects. The only method
    * functionality provided by this class is the ability to produce a GeoLocation object in the
    * EPSG4326 reference system.
    */
   class CoordinateReferenceSystem : public ::timber::Counted
   {
         CoordinateReferenceSystem(const CoordinateReferenceSystem&);
         CoordinateReferenceSystem&operator=(const CoordinateReferenceSystem&);

         /** A WGS84 coordinate struct. Note the use of radians units for latitude and longitude. */
      public:
         struct EPSG4979Coordinates
         {
               /** The latitude in radians */
               double lat;

               /** The longitude in radians */
               double lon;

               /** The altitude in meters */
               double alt;
         };

         /**
          * Create a new frame.
          * @param s the frame
          */
      protected:
         CoordinateReferenceSystem()throws();

         /** Destructor */
      public:
         ~CoordinateReferenceSystem()throws();

         /**
          * Create the EPSG4979 coordinate reference system. This is a built-in coordinate system
          * corresponding to <a href="">EPSG:4979</a> which is a geographic 3D coordinate reference system.
          * @return the coordinate reference system for <tt>EPSG:4979</tt>.
          */
      public:
         static ::timber::Reference< CoordinateReferenceSystem> getEPSG4979()throws();

         /**
          * Create the EPSG4326 coordinate reference system. This is a built-in coordinate system
          * corresponding to <a href="">EPSG:4326</a> which is a geographic 2D coordinate reference system.
          * @return the coordinate reference system for <tt>EPSG:4326</tt>.
          */
      public:
         static ::timber::Reference< CoordinateReferenceSystem> getEPSG4326()throws();

         /**
          * Create the EPSG3395 coordinate reference system. This is a built-in coordinate system
          * corresponding to <a href="">EPSG:3395</a> which is a projected coordinate reference system.
          * @return the coordinate reference system for <tt>EPSG:3395</tt>.
          */
      public:
         static ::timber::Reference< CoordinateReferenceSystem> getEPSG3395()throws();

         /**
          * Check the validity of a EPSG4979Coordinates object. If the values are just outside the
          * valid bounds the coordinates are modified to fit.
          * @param wgs84 EPSG4979Coordinates
          * @return true if the lat and lon are within the proper bounds
          */
      public:
         static bool validateEPSG4979Coordinates(EPSG4979Coordinates& wgs84) throws();

         /**
          * Get the epsg code for this coordinate reference system. This method must return
          * a code of type CRS.
          * @return an EPSG code or an undefined code if not known.
          */
      public:
         virtual EpsgCode epsgCode() const throws() = 0;

         /**
          * Get the datum on which this CRS is based.
          * @return a datum
          */
      public:
         virtual ::timber::Reference<Datum> datum() const  throws() = 0;

         /**
          * Test if two coordinate reference systems are the same.
          * @param e another crs
          * @return true if e and this are the same coordinate reference system
          */
      public:
         virtual bool equals(const CoordinateReferenceSystem& e) const throws() = 0;

         /**
          * The number of coordinates needed to represent a location in this reference system.
          * This method typically returns 2, but if altitude is available it may return 3.
          * @return the number of coordinates needed for a location.
          */
      public:
         virtual size_t coordinateCount() const throws() = 0;

         /**
          * Compute the distance between two points in this coordinate reference system. The distance is undefined if
          * the locations are not on the ground.
          * @param p1 a point
          * @param p2 a point
          * @return the distance in meters
          */
      public:
         virtual double distance (const double* p1, const double* p2) const throws() = 0;

         /**
          * Convert the coordinates in the specified coordinate frame into a location in
          * PSG4979 coordinates. This method must never fail. If this coordinate reference system
          * does not support altitude, then it must set the altitude parameter to 0 and return false.
          * @param coords the coordinates of a location in this frame
          * @param epsg4979 the output coordinates
          * @return true if the coordinate could be converted, false otherwise
          */
      public:
         virtual bool toEPSG4979(const double* coords, EPSG4979Coordinates& epsg4979) const throws() = 0;
         /**
          * Convert a point in PSG4979 coordinates to a point in this coordinate frame.
          * @param epsg4979 the coordinates to be converted
          * @param coords the output coordinates
          * @return true if the conversion was successful, false otherwise
          */
      public:
         virtual bool fromEPSG4979(const EPSG4979Coordinates& epsg4979, double* coords) const throws()= 0;
   };
}

#endif
