#ifndef _TERRA_MERIDIANARC_H
#define _TERRA_MERIDIANARC_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif

#include <iosfwd>

namespace terra {
  
  /** Forward declare meridianArc coordinates */
  class GeodeticCoordinates;

  /**
   * This class is used to represent a meridianArc path between two geodetic locations.
   * A meridianArc is specified by a range and an intitial bearing. Travel along the
   * meridianArc will lead to the closest point on the spheroid, but the bearing at
   * the destination may be different from the initial bearing. If an arc of
   * constant bearing is required, a Loxodrome should be used.
   */
  class MeridianArc {
      
    /**
     * Create a new meridianArc arc. Positive ranges indicate
     * a northerly directions, and negative ranges indicate a
     * southerly direction.
     * @param r the signed range value (in meters)
     */
  public:
    MeridianArc (double r) throws();
      
    /**
     * Create a default meridianArc. This equivalent to <code>MeridianArc(0,0)</code>.
     */
  public:
    inline MeridianArc () throws()
      : _range(0),_bearing(0) {}
      
    /**
     * Compute the shortest meridianArc between the latitudes of two points.
     * @param from the start point
     * @param to the end point
     * @pre REQUIRE_EQUALS(to.datum(),from.datum())
     */
  public:
    MeridianArc (const GeodeticCoordinates& from, const GeodeticCoordinates& to) throws();

    /**
     * Get the range. The range is always positive.
     * @return the range in meters.
     */
  public:
    inline double range () const throws() { return _range; }

    /**
     * Get the initial bearing.
     * @return the initial bearing in radians
     */
  public:
    inline double bearing () const throws() { return _bearing; }

    /**
     * Move along this meridianArc from the specified starting point.
     * @param p the start point
     * @return the point at which this meridianArc ends if starting at p
     */
  public:
    GeodeticCoordinates move (const GeodeticCoordinates& p) const throws ();

    /**
     * Print this meridianArc to a stream
     * @param out an output stream
     * @return out
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;

    /** The range */
  private:
    double _range;

    /** The initial bearing in radians */
  private:
    double _bearing;
  };
}

/**
 * Print a meridianArc  to an output stream
 * @param out an output stream
 * @param rb a meridianArc
 * @return out
 */
inline ::std::ostream& operator<< (::std::ostream& out, const ::terra::MeridianArc& rb)  
{ rb.print(out); return out; }

#endif
