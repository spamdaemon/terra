#ifndef _TERRA_BOUNDS_H
#define _TERRA_BOUNDS_H

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif

#ifndef _TERRA_H
#include <terra/terra.h>
#endif

#include <iosfwd>

namespace terra {
  class GeodeticDatum;

  /**
   * Bounds of a path, area, or region are defined by 4 corner points. The altitude is not
   * considered for these bounds.
   */
  class Bounds {

    /** A bounds edge */
  public:
    enum Edge {
      NORTH,
      EAST,
      SOUTH,
      WEST
    };

    /**
     * Create whole world bounds in the default datum.
     */
  public:
    Bounds() throws();
      
    /**
     * Create whole world bounds in the given datum
     * @param d a spheroid
     */
  public:
    Bounds(const ::timber::Reference<GeodeticDatum>& d) throws();
      
    /**
     * Create new bounds.
     * @param lat0 a latitude (in radians)
     * @param leftLon the longitude of the left side (in radians)
     * @param lat1 a latitude (in radians)
     * @param rightLon the longitude of the right side (in radians)
     * @param s a spheroid
     */
  public:
    Bounds (double lat0, double leftLon, double lat1, double rightLon, const ::timber::Reference<GeodeticDatum>& s) throws();
      
    /**
     * Create new bounds.
     * @param leftSide the top-left point
     * @param rightSide the bottom-right corner point
     * @pre REQUIRE_EQUAL(leftSide.datum(),rightSide.datum())
     */
  public:
    Bounds (const GeodeticCoordinates& leftSide, const GeodeticCoordinates& rightSide) throws();
      
    /**
     * Create new bounds.
     * @param point the point contained by the bounds
     */
  public:
    inline Bounds (const GeodeticCoordinates& point) throws()
      : _northWest(point),_southEast(point)
    {}
    
    /**
     * Create bounds from latitude and longitude values specified in degrees.
     * @param lat0 a latitude (in radians)
     * @param leftLon the longitude of the left side (in radians)
     * @param lat1 a latitude (in radians)
     * @param rightLon the longitude of the right side (in radians)
     * @param s a spheroid
     * @return bounds
     */
  public:
    static Bounds fromDegrees(double lat0, double leftLon, double lat1, double rightLon, const ::timber::Reference<GeodeticDatum>& s) throws();

    /**
     * Compute the smallest bounds defined by two points.
     * @param a a point
     * @param b a point
     * @pre REQUIRE_EQUAL(a.datum(),b.datum());
     * @return the smallest bounds 
     */
  public:
    static Bounds create (const GeodeticCoordinates& a, const GeodeticCoordinates& b) throws();

    /**
     * Get the datum
     * @return the datum
     */
  public:
    inline const ::timber::Reference<GeodeticDatum>& datum() const throws() { return _northWest.datum(); }

    /**
     * Get the center point of this bounds object
     * @return the point in the center
     */
  public:
    GeodeticCoordinates center() const throws();

    /**
     * Get the top-left corner
     * @return the top-left corner
     */
  public:
    inline GeodeticCoordinates northWest() const throws() { return _northWest; }
      
    /**
     * Get the bottom-right corner
     * @return the bottom-right corner
     */
  public:
    inline GeodeticCoordinates southEast() const throws() { return _southEast; }
    
    /**
     * Get the top-left corner
     * @return the top-left corner
     */
  public:
    GeodeticCoordinates northEast() const throws();
      
    /**
     * Get the bottom-right corner
     * @return the bottom-right corner
     */
  public:
    GeodeticCoordinates southWest() const throws();
    
    /**
     * Get the minimum latitude
     * @return the latitude of the bottom-right corner in radians
     */
  public:
    inline double south() const throws() { return _southEast.latitude(); }

    /**
     * Get the minimum latitude
     * @return the latitude of the top-left corner in radians
     */
  public:
    inline double north() const throws() { return _northWest.latitude(); }

    /**
     * Get the minimum longitude
     * @return the longitude of the top-left corner in radians
     */
  public:
    inline double west() const throws() { return _northWest.longitude(); }

    /**
     * Get the maximum longitude
     * @return the longitude of the bottom-right corner in radians
     */
  public:
    inline double east() const throws() { return _southEast.longitude(); }
     
    /**
     * Compute actual length of of an edge of these bounds. Note that the
     * length of the top and bottom edges are most likely different.
     * @param e the edge whose length is to be computed
     * @return the length of an edge in meters
     */
  public:
    double edgeLength (Edge e) const throws();
    
    /**
     * Test if the specified longitude is inside this bounding box.
     * @param lon a longitude in radians
     * @return true if the longitude range of this box contains the specified longitude
     */
  public:
    bool containsLongitude(double lon) const throws();

    /**
     * Test if the specified latitude is inside this bounding box.
     * @param lat a latitude in radians
     * @return true if the latitude range of this box contains the specified latitude
     */
  public:
    inline bool containsLatitude(double lat) const throws()
    { return _southEast.latitude() <= lat && lat <= _northWest.latitude(); }


    /**
     * Test if this bounds contains the specified point
     * @param lat a latitude in radians
     * @param lon a longitude in radians
     * @return true if <code>containsLatitude(lat) && containsLongitude(lon)</code>
     */
  public:
    inline bool contains (double lat, double lon) const throws()
    { return containsLatitude(lat) && containsLongitude(lon); }

    /**
     * Test if this bounds contains the specified point. Ensure that the
     * @param pt a point
     * @pre REQUIRE_EQUAL(datum(),pt.datum())
     * @return <code>contains(pt.latitude(),pt.longitude())</code>
     */
  public:
    inline bool contains (const GeodeticCoordinates& pt) const throws()
    {
      assert(pt.datum()->equals(*datum()));
      return contains(pt.latitude(),pt.longitude()); 
    }

    /**
     * Test if this bounds objects crosses from -180 to 180 or vice versa.
     * @return true if <code>east() < west()</code>
     */
  public:
    inline bool isCrossing () const throws()
    { return east() < west(); }

    /**
     * Test if this bounds intersects with the specified bounds
     * @param b a bounds object
     * @pre REQUIRE_EQUAL(b.datum(),datum())
     * @return true if this and b intersect
     */
  public:
    bool intersects (const Bounds& b) const throws();

    /**
     * Create new bounds by merging this bounds
     * with the specified bounds. If the two bounds do not
     * intersect, then the smaller of the two possible
     * bounding boxes is returned. <br>
     * When merging more than two bounds together, order 
     * becomes a factor if the smallest bounding box is to be
     * found
     * @param b other bounds
     * @pre REQUIRE_EQUAL(b.datum(),datum())
     * @return the bounds of the merged bounds
     */
  public:
    Bounds merge (const Bounds& b) const throws();

    /**
     * Intersect this bounds object with the specified bounds.
     * @param b other bounds
     * @param result the set of regions produced by the intersection
     * @pre REQUIRE_EQUAL(b.datum(),datum())
     * @return the number of intersection regions
     */
  public:
    Int intersect(const Bounds& b, Bounds result[3]) const throws();

    /**
     * Print this bounds object to the specified stream.
     * @param out an output stream
     * @return out
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;
      
    /** The north-west and south-east corners */
  private:
    GeodeticCoordinates _northWest,_southEast;
  };
    
}

inline ::std::ostream& operator<< (::std::ostream& out, const ::terra::Bounds& b) 
{ return b.print(out); }

#endif
