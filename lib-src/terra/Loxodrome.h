#ifndef _TERRA_LOXODROME_H
#define _TERRA_LOXODROME_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif

#include <iosfwd>

namespace terra {
  
  /** Forward declare geodesic coordinates */
  class GeodeticCoordinates;

  /**
   * This class is used to represent a loxodrome. A loxodrome is an path that intersects
   * meridians at a constant angle. If the bearing is any bearing other than 90 or 270, then
   * the loxodrome has the form of a spiral that will lead to one of the poles. Loxodromes
   * are not paths of shortest distance between two points, and if such an arc is desired,
   * a Geodesic must be usde.
   */
  class Loxodrome {
    /**
     * Create a new loxodrome arc.
     * @param r the range value (in meters)
     * @param az the initial bearing (in radians)
     */
  public:
    Loxodrome (double r, double az) throws();
      
    /**
     * Create a default loxodrome. 
     * This equivalent to <code>Loxodrome(0,0)</code>.
     */
  public:
    inline Loxodrome () throws()
      : _range(0),_bearing(0) {}
      
    /**
     * Compute the shortest loxodrome between two points.
     * @param from the start point
     * @param to the end point
     * @pre REQUIRE_EQUALS(from.datum(),to.datum())
     * @return the shortest loxodrome between the two points
     */
  public:
    Loxodrome  (const GeodeticCoordinates& from, const GeodeticCoordinates& to) throws();
      
    /**
     * Get the range. The range is always positive.
     * @return the range in meters.
     */
  public:
    inline double range () const throws() { return _range; }

    /**
     * Get the initial bearing.
     * @return the initial bearing in radians
     */
  public:
    inline double bearing () const throws() { return _bearing; }

    /**
     * Move along this loxodrome from the specified starting point.
     * @param p the start point
     * @return the point at which this loxodrome ends if starting at p
     */
  public:
    GeodeticCoordinates move (const GeodeticCoordinates& p) const throws ();

    /**
     * Print this loxodrome to a stream
     * @param out an output stream
     * @return out
     */
  public:
    ::std::ostream& print (::std::ostream& out) const;

    /** The range */
  private:
    double _range;

    /** The initial bearing */
  private:
    double _bearing;
  };
}

/**
 * Print a loxodrome  to an output stream
 * @param out an output stream
 * @param rb a loxodrome
 * @return out
 */
inline ::std::ostream& operator<< (::std::ostream& out, const ::terra::Loxodrome& rb)  
{ rb.print(out); return out; }

#endif
