#ifndef _TERRA_H
#define _TERRA_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MATH_H
#include <timber/math.h>
#endif

#include <string>

namespace terra {
   class GeodeticCoordinates;
   class GeodeticDatum;

   constexpr double PI = 3.14159265358979323846;

   using ::timber::Int;
   using ::timber::Long;
   using ::timber::ULong;
   using ::timber::Long;

   /**
    * Clamp a longitude in radians into the interval (-pi,pi). The clamping is done
    * using a <tt>lon % 360</tt> and shifting as necessary to get the desired range.
    * <p>
    * If an eps value greater than 0 is specified, then longitudes that exceed the normalized
    * range by that small amount are simply mappinto the normalize range without wrapping. For example,
    * the the value <tt>PI+0.5*eps<tt> is mapped into PI.
    * @param lon a longiude in radians.
    * @param eps a small value
    * @return a longitude in the range (-pi,pi)
    */
   double clampLongitude(double lon, double eps = 0.0) throws();
   /**
    * Clamp a longitude in radians into the interval (-pi/2,pi/2)
    * @param lat a latitude in radians
    * @return a latitude in the range (-pi/2,pi/2)
    */
   inline double clampLatitude(double lat) throws()
   {
      return lat > PI / 2 ? PI / 2 : (lat < -PI / 2 ? -PI / 2 : lat);
   }

   /**
    * Clamp an angle in degree to within the range 0 to 360.
    * @param a an angle
    * @return an angle x, s.th. 0 <= x < 360
    */
   double clampDegreeAngle(double a)
   throws();

   /**
    * Clamp an angle in radians to within the range 0 to 2*PI.
    * @param a an angle
    * @return an angle x, s.th. 0 <= x < 2*PI
    */
   double clampRadiansAngle(double a)
   throws();

   /*@}*/

   /**
    * @name Parsing geodetic coordinates
    * @{
    */

   /**
    * Parse a geodeticcoordinates from a string. This is possibly an expensive
    * operation, because there are so many different formats, and this
    * implementation may to figure out which format is used through
    * trial and error.
    * @param s a string
    * @param off the start index
    * @param n the number of characters to be parsed
    * @param d the datum
    * @return a geodeticcoordinates
    * @throws ParseException if the string is not a valid geodeticcoordinates
    */
   GeodeticCoordinates parseGeodeticCoordinates(const ::std::string& s, size_t off, size_t n,
         const ::timber::Reference< GeodeticDatum>& d)
         throws (::std::exception);

   /**
    * Parse a geodeticcoordinates from a string. This is possibly an expensive
    * operation, because there are so many different formats, and this
    * implementation may to figure out which format is used through
    * trial and error.
    * @param s a string
    * @param d the datum
    * @return a geodeticcoordinates
    * @throws ::std::exception if the string is not a valid geodeticcoordinates
    */
   GeodeticCoordinates parseGeodeticCoordinates(const ::std::string& s, const ::timber::Reference< GeodeticDatum>& d)
   throws (::std::exception);

   /**
    * Parse a geodeticcoordinates from a string.
    * @param s a string
    * @param off the start index
    * @param n the number of characters to be parsed
    * @param d the datum
    * @return a geodeticcoordinates
    * @throws ::std::exception if the string is not a valid geodeticcoordinates
    */
   GeodeticCoordinates parseGeodeticCoordinates(const char* s, size_t off, size_t n,
         const ::timber::Reference< GeodeticDatum>& d)
         throws (::std::exception);

   /**
    * Parse a geodeticcoordinates from a string. This is possibly an expensive
    * operation, because there are so many different formats, and this
    * implementation may to figure out which format is used through
    * trial and error.
    * @param s a string
    * @param d the datum
    * @return a geodeticcoordinates
    * @throws ::std::exception if the string is not a valid geodeticcoordinates
    */
   GeodeticCoordinates parseGeodeticCoordinates(const char* s, const ::timber::Reference< GeodeticDatum>& d)
   throws (::std::exception);

   /*@}*/

   /**
    * Intersect two loxodromes. If the two loxodromes do not intersect, then
    * the result object is not modified.
    * @param a a loxodrome given by its two endpoints
    * @param b a loxodrome given by its two endpoints
    * @param res the intersection point
    * @pre REQUIRE_TRUE(a[0].datum().equals(a[1].datum()))
    * @pre REQUIRE_TRUE(b[0].datum().equals(b[1].datum()))
    * @pre REQUIRE_TRUE(b[0].datum().equals(a[0].datum()))
    * @return true if the two loxodromes intersected, false otherwise.
    */
   bool intersectLoxodromes(const GeodeticCoordinates* a, const GeodeticCoordinates* b, GeodeticCoordinates& res)
   throws();

   /**
    * Generate interior points of the geodesic arc between two coordinates. When moving
    * along the implied loxodromes, the total distance travelled shall differ from the
    * geodesic distance by no more than the specified error.
    * @param p the start point
    * @param q the end point
    * @param err the distance error
    * @param cb a callback function
    * @return the number of interior points that were generated
    */
   size_t generateGeodesicPoints(const GeodeticCoordinates& p, const GeodeticCoordinates& q, double err,
         void (*cb)(const GeodeticCoordinates&));
}

#endif
