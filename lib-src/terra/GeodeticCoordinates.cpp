#include <terra/GeodeticCoordinates.h>
#include <terra/Angle.h>
#include <terra/ECEFCoordinates.h>
#include <timber/ios/Guard.h>

#include <terra/Geodesic.h>
#include <terra/Loxodrome.h>

#include <cmath>
#include <iostream>
#include <iomanip>

using namespace ::std;
using namespace ::timber;

namespace terra {
  
   constexpr double GeodeticCoordinates::LOCATION_ERROR;

   GeodeticCoordinates GeodeticCoordinates::fromDegrees(double lat, double lon, double alt)
   throws()
   {
      assert(lat >= -90);
      assert(lat <= 90);
      assert(lon >= -180);
      assert(lon <= 180);
      return GeodeticCoordinates(clampLatitude(toRadians(lat)),clampLongitude(toRadians(lon)),alt);
   }
   GeodeticCoordinates GeodeticCoordinates::fromDegrees(double lat, double lon, double alt,
         const Reference< GeodeticDatum>& s)
   throws()
   {
      assert(lat >= -90);
      assert(lat <= 90);
      assert(lon >= -180);
      assert(lon <= 180);
      return GeodeticCoordinates(clampLatitude(toRadians(lat)),clampLongitude(toRadians(lon)),alt,s);
   }

   GeodeticCoordinates::GeodeticCoordinates(const ECEFCoordinates& ecef)
   throws()
   : _datum(ecef.datum())
   {
      _datum->spheroid().computeGeodetic(ecef.coordinates(),_latlon[0],_latlon[1],_height);
   }

   void GeodeticCoordinates::changeDatum(const ::timber::Reference< GeodeticDatum>& newDatum)
   throws()
   {
      _datum->spheroid().convertGeodeticLocation(newDatum->spheroid(),_latlon[0],_latlon[1],_height);
      _datum=newDatum;
   }

   ECEFCoordinates GeodeticCoordinates::ecefCoordinates() const
   throws()
   {  return ECEFCoordinates(*this);}

   GeodeticCoordinates GeodeticCoordinates::antipodalCoordinates() const
   throws()
   {
      double lon = clampLongitude(longitude()+PI);
      double lat = -latitude();
      return GeodeticCoordinates(lat,lon,height(),datum());
   }

   bool GeodeticCoordinates::isSameLocation(const GeodeticCoordinates& pt) const
   throws()
   {
      if (latitude()!=pt.latitude() || height()!=pt.height() || !_datum->equals(*pt._datum)) {
         return false;
      }

      double x1[3];
      double x2[3];
      _datum->spheroid().computeECEF(latitude(),longitude(),height(),x1);
      _datum->spheroid().computeECEF(pt.latitude(),pt.longitude(),pt.height(),x2);

      double dx = 0;
      for (size_t i=0;i<3;++i) {
         dx = (x1[i]-x2[i])*(x1[i]-x2[i]);
      }
      return dx < 1;
   }

   bool GeodeticCoordinates::equals(const GeodeticCoordinates& pt) const
   throws()
   {
      if (!_datum->equals(*pt._datum)) {
         return false;
      }
      return latitude()==pt.latitude() && longitude()==pt.longitude() && height()==pt.height();
   }

   ::std::ostream& GeodeticCoordinates::print(::std::ostream& out) const
   {
      ::timber::ios::Guard iosguard(out);
      out << '(';
      out << ::std::setprecision(7);
      if (latitude() < 0) {
         out << -degreesLat() << "S";
      }
      else {
         out << degreesLat() << "N";
      }
      out << ' ';
      out << ::std::setprecision(7);
      if (longitude() < 0) {
         out << -degreesLon() << "W";
      }
      else {
         out << degreesLon() << "E";
      }
      out << ::std::setprecision(3);
      return out << "  " << height() << "m)";
   }

   double GeodeticCoordinates::computeTrueCourse(const GeodeticCoordinates& c) const
   throws()
   {  return _datum->spheroid().computeTrueCourse(latitude(),longitude(),c.latitude(),c.longitude());}

   void GeodeticCoordinates::difference(const GeodeticCoordinates& p, double diff[3]) const
   throws()
   {
      assert(_datum->equals(*p._datum));
      double lat = latitude()-p.latitude();
      double lon = longitude()-p.longitude();
      if (lon > PI) {
         lon = ::std::max(lon-2*PI,-PI);
      }
      else if (lon < -PI) {
         lon = ::std::min(lon+2*PI,PI);
      }
      assert (lon>=-PI);
      assert (lon<=PI);
      diff[0] = lat;
      diff[1] = lon;
      diff[2] = height()-p.height();
   }

   void GeodeticCoordinates::mercatorProjection(double& x, double& y) const
   throws()
   {
      _datum->spheroid().computeMercatorProjection(0,latitude(),longitude(),x,y);
   }

   GeodeticCoordinates GeodeticCoordinates::inverseMercatorProjection(double x, double y,
         const Reference< GeodeticDatum>& d)
   throws()
   {
      double lat,lon;
      d->spheroid().computeInverseMercatorProjection(0,x,y,lat,lon);
      lon = ::std::min(lon,PI);
      lon = ::std::max(lon,-PI);
      lat = ::std::min(lat,PI/2.0);
      lat = ::std::max(lat,-PI/2.0);

      return GeodeticCoordinates(lat,lon,0,d);
   }

   GeodeticCoordinates GeodeticCoordinates::travelAlongGeodesic(double r, double bearing) const
   throws()
   {
      if (r<0) {
         r = -r;
         bearing += PI;
      }
      GeodeticCoordinates res;
      _datum->spheroid().travelAlongGeodesic(_latlon[0],_latlon[1],bearing,r,res._latlon[0],res._latlon[1]);
      return res;
   }

   GeodeticCoordinates GeodeticCoordinates::travelAlongLoxodrome(double r, double bearing) const
   throws()
   {
      if (r<0) {
         r = -r;
         bearing += PI;
      }
      GeodeticCoordinates res;
      _datum->spheroid().travelAlongLoxodrome(_latlon[0],_latlon[1],bearing,r,res._latlon[0],res._latlon[1]);
      return res;
   }

   double GeodeticCoordinates::geodesicDistance(const GeodeticCoordinates& pt) const
   throws()
   {
      Geodesic arc(*this,pt);
      return arc.range();
   }

   double GeodeticCoordinates::loxodromicDistance(const GeodeticCoordinates& pt) const
throws()
{
   Loxodrome arc(*this,pt);
   return arc.range();
}

}
