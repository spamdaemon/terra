#include <terra/UnsupportedCRS.h>
#include <string>
#include <sstream>

namespace terra {

   namespace {
      static ::std::string getExceptionString(
            const ::timber::Reference< CoordinateReferenceSystem>& crs,
            const ::std::vector< ::timber::Reference< CoordinateReferenceSystem> >& s = ::std::vector<
                  ::timber::Reference< CoordinateReferenceSystem> >())
      {
         ::std::ostringstream out;
         out << "Coordinate system unsupported : " << crs->epsgCode() << ". ";
         bool printHeader = true;
         for (auto c = s.begin(); c != s.end(); ++c) {
            // it it's an undefined CRS, then there isn't much point in printing that
            EpsgCode code = (*c)->epsgCode();
            if (code.type() != EpsgCode::UNDEFINED) {
               if (printHeader) {
                  printHeader = false;
                  out << "Supported system include: [";
               }
               out << ' ' << code;
            }
         }
         if (!printHeader) {
            out << " ] ";
         }
         return out.str();
      }
   }

   UnsupportedCRS::UnsupportedCRS(const ::timber::Reference< CoordinateReferenceSystem>& crs,
         ::std::vector< ::timber::Reference< CoordinateReferenceSystem> >& s) :
      ::std::runtime_error(getExceptionString(crs, s)), _unsupported(crs), _supported(s)
   {
   }

   UnsupportedCRS::UnsupportedCRS(const ::timber::Reference< CoordinateReferenceSystem>& crs) :
      ::std::runtime_error(getExceptionString(crs)), _unsupported(crs)
   {
   }

   UnsupportedCRS::UnsupportedCRS(const ::timber::Reference< CoordinateReferenceSystem>& crs,
         const ::timber::Reference< CoordinateReferenceSystem>& s) :
      ::std::runtime_error(getExceptionString(crs)), _unsupported(crs), _supported(1, s)
   {
   }

   UnsupportedCRS::~UnsupportedCRS() throw()
   {
   }

}
