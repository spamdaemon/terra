#include <terra/CoordinateReferenceSystem.h>
#include <terra/Geographic2DCRS.h>
#include <terra/Geographic3DCRS.h>
#include <terra/ProjectedCRS.h>
#include <terra/GeodeticDatum.h>
#include <terra/Spheroid.h>
#include <terra/terra.h>
#include <cmath>

namespace terra {

   using namespace ::timber;

   CoordinateReferenceSystem::CoordinateReferenceSystem()
   throws()
   {
   }

   CoordinateReferenceSystem::~CoordinateReferenceSystem()
   throws()
   {
   }

   bool CoordinateReferenceSystem::validateEPSG4979Coordinates(EPSG4979Coordinates& wgs84) throws()
   {
      const double eps = 1E-8;
      if (wgs84.lat < -PI / 2) {
         if (wgs84.lat < -PI / 2 - eps) {
            return false;
         }
         wgs84.lat = -PI / 2;
      }
      else if (wgs84.lat > PI / 2) {
         if (wgs84.lat > PI / 2 + eps) {
            return false;
         }
         wgs84.lat = PI / 2;
      }

      if (wgs84.lon < -PI) {
         if (wgs84.lon < -PI - eps) {
            return false;
         }
         wgs84.lon = -PI;
      }
      else if (wgs84.lon > PI) {
         if (wgs84.lon > PI + eps) {
            return false;
         }
         wgs84.lon = PI;
      }
      // don't check altitude
      return true;
   }

   ::timber::Reference< CoordinateReferenceSystem> CoordinateReferenceSystem::getEPSG4979()
   throws()
   {
      struct epsg4979 : public Geographic3DCRS
      {
            epsg4979() throws()
                  : _datum(GeodeticDatum::createWGS84())
            {
            }
            ~epsg4979() throws()
            {
            }

            ::timber::Reference< Datum> datum() const throws()
            {
               return _datum;
            }

            EpsgCode epsgCode() const throws()
            {
               return EpsgCode(EpsgCode::CRS, 4979);
            }

            size_t coordinateCount() const throws()
            {
               return 3;
            }

            double distance(const double* p1, const double* p2) const throws()
            {
               // the distance on the spheroid is a geodesic arc
               const double c1[] = { toRadians(p1[0]), toRadians(p1[1]) };
               const double c2[] = { toRadians(p2[0]), toRadians(p2[1]) };
               double az[2];
               double r = _datum->spheroid().computeGeodesic(c1[0], c1[1], c2[0], c2[1], az[0], az[1]);

               return r;
            }

            bool toEPSG4979(const double* coords, EPSG4979Coordinates& wgs84) const throws()
            {
               wgs84.lat = toRadians(coords[0]);
               wgs84.lon = toRadians(coords[1]);
               wgs84.alt = coords[2];
               return validateEPSG4979Coordinates(wgs84);
            }

            bool fromEPSG4979(const EPSG4979Coordinates& wgs84, double* coords) const throws()
            {
               coords[0] = toDegrees(wgs84.lat);
               coords[1] = toDegrees(wgs84.lon);
               coords[2] = wgs84.alt;
               return true;
            }

            bool equals(const CoordinateReferenceSystem& e) const throws()
            {
               return dynamic_cast< const epsg4979*>(&e) != 0;
            }

            /** The wgs 84 datum */
         private:
            ::timber::Reference< GeodeticDatum> _datum;
      };

      GeodeticCRS* crs = new epsg4979();
      return ::timber::Reference< CoordinateReferenceSystem>(crs);
   }

   ::timber::Reference< CoordinateReferenceSystem> CoordinateReferenceSystem::getEPSG4326()
   throws()
   {
      struct epsg4326 : public Geographic2DCRS
      {
            epsg4326() throws()
                  : _datum(GeodeticDatum::createWGS84())
            {
            }
            ~epsg4326() throws()
            {
            }

            ::timber::Reference< Datum> datum() const throws()
            {
               return _datum;
            }

            EpsgCode epsgCode() const throws()
            {
               return EpsgCode(EpsgCode::CRS, 4326);
            }

            size_t coordinateCount() const throws()
            {
               return 2;
            }

            double distance(const double* p1, const double* p2) const throws()
            {
               // the distance on the spheroid is a geodesic arc
               const double c1[] = { toRadians(p1[0]), toRadians(p1[1]) };
               const double c2[] = { toRadians(p2[0]), toRadians(p2[1]) };
               double az[2];
               double r = _datum->spheroid().computeGeodesic(c1[0], c1[1], c2[0], c2[1], az[0], az[1]);

               return r;
            }

            bool toEPSG4979(const double* coords, EPSG4979Coordinates& wgs84) const throws()
            {
               wgs84.lat = toRadians(coords[0]);
               wgs84.lon = toRadians(coords[1]);
               wgs84.alt = 0;
               return validateEPSG4979Coordinates(wgs84);
            }

            bool fromEPSG4979(const EPSG4979Coordinates& wgs84, double* coords) const throws()
            {
               coords[0] = toDegrees(wgs84.lat);
               coords[1] = toDegrees(wgs84.lon);
               return true;
            }

            bool equals(const CoordinateReferenceSystem& e) const throws()
            {
               return dynamic_cast< const epsg4326*>(&e) != 0;
            }

            /** The wgs 84 datum */
         private:
            ::timber::Reference< GeodeticDatum> _datum;

      };

      GeodeticCRS* crs = new epsg4326();
      return ::timber::Reference< CoordinateReferenceSystem>(crs);
   }

   ::timber::Reference< CoordinateReferenceSystem> CoordinateReferenceSystem::getEPSG3395()
   throws()
   {
      struct epsg3395 : public ProjectedCRS
      {
            epsg3395() throws()
                  : _datum(GeodeticDatum::createWGS84())
            {
            }
            ~epsg3395() throws()
            {
            }

            ::timber::Reference< Datum> datum() const throws()
            {
               return _datum;
            }
            EpsgCode epsgCode() const throws()
            {
               return EpsgCode(EpsgCode::CRS, 3395);
            }

            size_t coordinateCount() const throws()
            {
               return 2;
            }

            double distance(const double* p1, const double* p2) const throws()
            {
               const double p[] = { p1[0] - p2[0], p1[1] - p2[1] };
               return ::std::sqrt(p[0] * p[0] + p[1] * p[1]);
            }

            bool toEPSG4979(const double* coords, EPSG4979Coordinates& wgs84) const throws()
            {
               _datum->spheroid().computeInverseMercatorProjection(0, coords[0], coords[1], wgs84.lat, wgs84.lon);
               wgs84.alt = 0;
               return validateEPSG4979Coordinates(wgs84);
            }

            bool fromEPSG4979(const EPSG4979Coordinates& wgs84, double* coords) const throws()
            {
               _datum->spheroid().computeMercatorProjection(0, wgs84.lat, wgs84.lon, coords[0], coords[1]);
               return true;
            }

            bool equals(const CoordinateReferenceSystem& e) const throws()
            {
               return dynamic_cast< const epsg3395*>(&e) != 0;
            }

            /** The wgs 84 datum */
         private:
            ::timber::Reference< GeodeticDatum> _datum;
      };

      ProjectedCRS* crs = new epsg3395();
      return ::timber::Reference< CoordinateReferenceSystem>(crs);
   }

}
