#include <terra/ECEFCoordinates.h>
#include <terra/GeodeticCoordinates.h>
#include <iostream>
#include <cmath>

namespace terra {
  
  
  ECEFCoordinates::ECEFCoordinates (const GeodeticCoordinates& geo) throws()
  : _datum(geo.datum())
  {
    _datum->spheroid().computeECEF(geo.latitude(),geo.longitude(),geo.height(),_x);
  }
  
  GeodeticCoordinates ECEFCoordinates::geodeticCoordinates () const throws()
  { return GeodeticCoordinates(*this); }
  
  bool ECEFCoordinates::equals (const ECEFCoordinates& pt) const throws()
  { 
    if (&pt==this) {
      return true;
    }
    return _datum->equals(*pt._datum) && 
      x()==pt.x() && 
      y()==pt.y() && 
      z()==pt.z();
  }
  
  void ECEFCoordinates::changeDatum(const ::timber::Reference< GeodeticDatum>& newDatum)
  throws()
  {
     _datum->spheroid().convertECEFLocation(newDatum->spheroid(),_x);
  }


  double ECEFCoordinates::distance (const ECEFCoordinates& c) const throws()
  {
    assert(_datum->equals(*c._datum));
    double tmp = (x()-c.x()) * (x()-c.x());
    tmp += (y()-c.y()) * (y()-c.y());
    tmp += (z()-c.z()) * (z()-c.z());
    return ::std::sqrt(tmp); 
  }
  
  ::std::ostream& ECEFCoordinates::print(::std::ostream& out) const 
  {
    return out << "[ " << x() <<" " << y() << " " << z() << " ]";
  }
}
