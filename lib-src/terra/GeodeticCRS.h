#ifndef _TERRA_GEODETICCRS_H
#define _TERRA_GEODETICCRS_H

#ifndef _TERRA_COORDINATERFERENCESSYSTEM_H
#include <terra/CoordinateReferenceSystem.h>
#endif

namespace terra {

   /**
    * A coordinate reference system provides information about Location objects. The only method
    * functionality provided by this class is the ability to produce a GeoLocation object in the
    * EPSG4326 reference system.
    * More about EPSG can be found <a href="http://www.epsg-registry.org/">here</a>.
    */
   class GeodeticCRS : public CoordinateReferenceSystem
   {
      private:
         GeodeticCRS(const GeodeticCRS&);
         GeodeticCRS&operator=(const GeodeticCRS&);

         /**
          * Create a new frame.
          * @param s the frame
          */
      protected:
         GeodeticCRS()throws();

         /** Destructor */
      public:
         ~GeodeticCRS()throws();
   };
}

#endif
