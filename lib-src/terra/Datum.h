#ifndef _TERRA_DATUM_H
#define _TERRA_DATUM_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace terra {

   /**
    * This is the base-class for datums. The only supported operation is testing
    * two datums for equality.
    */
   class Datum : public ::timber::Counted
   {
         Datum(const Datum&);
         Datum&operator=(const Datum&);

         /** Default constructor */
      protected:
         Datum()throws();

         /** Destructor */
      public:
         ~Datum()throws();

         /**
          * Test if two datums are the same.
          * @param e another datum
          * @return true if e and this are the same datum
          */
      public:
         virtual bool equals(const Datum& e) const throws() = 0;
   };
}

#endif
