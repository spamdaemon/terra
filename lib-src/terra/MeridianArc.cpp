#include <terra/MeridianArc.h>
#include <terra/GeodeticCoordinates.h>
#include <iostream>
#include <cmath>

namespace terra {
  
  ::std::ostream& MeridianArc::print (::std::ostream& out) const
  { return out << "(r="<<range() << " ,az="<< ::timber::toDegrees(bearing()) << ")"; }
  
  MeridianArc::MeridianArc (double r) throws()
  : _range(r>=0 ? r : -r),_bearing(r>=0 ? 0 : PI)  
  {}
  
  MeridianArc::MeridianArc (const GeodeticCoordinates& from, const GeodeticCoordinates& to) throws()
  {
    assert(from.datum()->equals(*to.datum()));
    double r= from.datum()->spheroid().meridianArcLength(from.latitude(),to.latitude());
    _range = r>=0 ? r : -r;
    _bearing = r>0 ? 0 : PI;
  }
  
  GeodeticCoordinates MeridianArc::move(const GeodeticCoordinates& p) const throws ()
  {
    double lat,lon;
    const double d = _bearing==0.0 ? _range : _range;
    p.datum()->spheroid().travelAlongMeridianArc(p.latitude(),p.longitude(),p.longitude(),d,lat,lon);
    return GeodeticCoordinates(lat,lon,p.height(),p.datum());
  }
}
