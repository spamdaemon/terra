#include <terra/TopocentricCoordinates.h>
#include <terra/terra.h>
#include <terra/ECEFCoordinates.h>
#include <iostream>
#include <cmath>

using namespace ::std;
using namespace ::timber;

namespace terra {

   TopocentricFrame::TopocentricFrame(const GeodeticCoordinates& geo)
   throws()
   : _center(geo),_meridianRadius(geo.meridianRadius()),_circleRadius(geo.circleRadius())
   {
      _centerXmeridianRadius = _meridianRadius * _center.latitude();
      _centerXcircleRadius = _circleRadius * _center.longitude();

      double sinL,cosL,sinF,cosF;

      double lon = geo.longitude();
      double lat= geo.latitude();
      if (::std::abs(lat-PI/2) < 1E-8) {
         lat = PI/2;
         lon = 0;
         sinL = 1;
         cosL = 0;
         sinF = 0;
         cosF = 1;
      }
      else if (::std::abs(lat + PI/2) < 1E-8) {
         lat = -PI/2;
         lon = -PI;
         sinL = -1;
         cosL = 0;
         sinF = 0;
         cosF = -1;
      }
      else {
         sinL = ::std::sin(lat);
         cosL = ::std::cos(lat);
         sinF = ::std::sin(lon);
         cosF = ::std::cos(lon);
      }

      _center.datum()->spheroid().computeECEF(lat,lon,_center.height(),_ecefOrigin);

      _ecef2tcs[0] = -sinF;
      _ecef2tcs[1] = cosF;
      _ecef2tcs[2] = 0;
      _ecef2tcs[3] = -sinL*cosF;
      _ecef2tcs[4] = -sinL*sinF;
      _ecef2tcs[5] = cosL;
      _ecef2tcs[6] = cosL*cosF;
      _ecef2tcs[7] = cosL*sinF;
      _ecef2tcs[8] = sinL;
   }

   TopocentricFrame::~TopocentricFrame()
   throws()
   {}

   ::timber::Reference< TopocentricFrame> TopocentricFrame::create()
   throws()
   {  return new TopocentricFrame(GeodeticCoordinates());}

   ::timber::Reference< TopocentricFrame> TopocentricFrame::create(const GeodeticCoordinates& ctr)
   throws()
   {  return new TopocentricFrame(ctr);}

   ::timber::Reference< Datum> TopocentricFrame::datum() const
   throws()
   {  return _center.datum();}

   double TopocentricFrame::distance(const double* p1, const double* p2) const
   throws()
   {
      const double p[] = {p1[0]-p2[0], p1[1]-p2[1],p1[2]-p2[2] };
      return ::std::sqrt(p[0]*p[0] + p[1]*p[1] + p[2]*p[2]);
   }

   bool TopocentricFrame::equals(const CoordinateReferenceSystem& e) const
   throws()
   {
      const TopocentricFrame* other = dynamic_cast<const TopocentricFrame*>(&e);
      return other!=0 && _center.equals(other->_center);
   }

   void TopocentricFrame::transformFromGeodetic(const double* geo, double* tcs) const
   throws()
   {
      double ecef[3];
      _center.datum()->spheroid().computeECEF(geo[0],geo[1],geo[2],ecef);
      return transformFromECEF(ecef,tcs);
   }

   void TopocentricFrame::transformFromECEF(const double* ecef, double* tcs) const
   throws()
   {
      double a = ecef[0]-_ecefOrigin[0];
      double b = ecef[1]-_ecefOrigin[1];
      double c = ecef[2]-_ecefOrigin[2];

      tcs[0] = _ecef2tcs[0]*a + _ecef2tcs[1]*b + _ecef2tcs[2]*c;
      tcs[1] = _ecef2tcs[3]*a + _ecef2tcs[4]*b + _ecef2tcs[5]*c;
      tcs[2] = _ecef2tcs[6]*a + _ecef2tcs[7]*b + _ecef2tcs[8]*c;
   }

   void TopocentricFrame::transformFromGeodetic(const GeodeticCoordinates& geo, double* tcs) const
   throws()
   {
      double ecef[3];
      _center.datum()->spheroid().computeECEF(geo.latitude(),geo.longitude(),geo.height(),ecef);
      return transformFromECEF(ecef,tcs);
   }

   void TopocentricFrame::transformFromECEF(const ECEFCoordinates& ecef, double* tcs) const
   throws()
   {
      transformFromECEF(ecef.coordinates(),tcs);
   }

   void TopocentricFrame::transformToECEF(const double* tcs, double* ecef) const
   throws()
   {
      ecef[0] = _ecefOrigin[0] + _ecef2tcs[0]*tcs[0] + _ecef2tcs[3]*tcs[1] + _ecef2tcs[6]*tcs[2];
      ecef[1] = _ecefOrigin[1] + _ecef2tcs[1]*tcs[0] + _ecef2tcs[4]*tcs[1] + _ecef2tcs[7]*tcs[2];
      ecef[2] = _ecefOrigin[2] + _ecef2tcs[2]*tcs[0] + _ecef2tcs[5]*tcs[1] + _ecef2tcs[8]*tcs[2];
   }

   void TopocentricFrame::transformToGeodetic(const double* tcs, double* geo) const
   throws()
   {
      double ecef[3];
      transformToECEF(tcs,ecef);
      _center.datum()->spheroid().computeGeodetic(ecef,geo[0],geo[1],geo[2]);
   }

   void TopocentricFrame::transformToECEF(const TopocentricCoordinates& tcs, double* ecef)
   throws()
   {
      double x[] = {tcs.x(),tcs.y(),tcs.z()};
      tcs.frame()->transformToECEF(x,ecef);
   }

   void TopocentricFrame::transformToGeodetic(const TopocentricCoordinates& tcs, double* geo)
   throws()
   {
      double x[] = {tcs.x(),tcs.y(),tcs.z()};
      tcs.frame()->transformToGeodetic(x,geo);
   }

   ::newton::Matrix< double> TopocentricFrame::getRotationToECEF() const
   throws()
   {
      ::newton::Matrix<double> R(3,3);
      R(0,0) = _ecef2tcs[0];
      R(1,0) = _ecef2tcs[1];
      R(2,0) = _ecef2tcs[2];
      R(0,1) = _ecef2tcs[3];
      R(1,1) = _ecef2tcs[4];
      R(2,1) = _ecef2tcs[5];
      R(0,2) = _ecef2tcs[6];
      R(1,2) = _ecef2tcs[7];
      R(2,2) = _ecef2tcs[8];
      return R;
   }

   ::newton::Matrix< double> TopocentricFrame::getRotationFromECEF() const
   throws()
   {
      ::newton::Matrix<double> R(3,3);
      R(0,0) = _ecef2tcs[0];
      R(0,1) = _ecef2tcs[1];
      R(0,2) = _ecef2tcs[2];
      R(1,0) = _ecef2tcs[3];
      R(1,1) = _ecef2tcs[4];
      R(1,2) = _ecef2tcs[5];
      R(2,0) = _ecef2tcs[6];
      R(2,1) = _ecef2tcs[7];
      R(2,2) = _ecef2tcs[8];
      return R;
   }

   size_t TopocentricFrame::coordinateCount() const
   throws() {return 3;}

   bool TopocentricFrame::toEPSG4979(const double* coords, EPSG4979Coordinates& wgs84) const
   throws()
   {
      double xyz[3];
      transformToECEF(coords,xyz);
      _center.datum()->spheroid().ecefToWGS84(xyz);
      Spheroid::getEPSG7030()->computeGeodetic(xyz,wgs84.lat,wgs84.lon,wgs84.alt);
      return true;
   }

   bool TopocentricFrame::fromEPSG4979(const EPSG4979Coordinates& wgs84, double* coords) const
   throws()
   {
      double xyz[3];
      Spheroid::getEPSG7030()->computeECEF(wgs84.lat,wgs84.lon,wgs84.alt,xyz);
      _center.datum()->spheroid().wgs84ToECEF(xyz);
      transformFromECEF(xyz,coords);
      return true;
   }
   EpsgCode TopocentricFrame::epsgCode() const
throws() {return EpsgCode();}

}
