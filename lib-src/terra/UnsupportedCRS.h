#ifndef _TERRA_UNSUPPORTEDCRS_H
#define _TERRA_UNSUPPORTEDCRS_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TERRA_COORDINATEREFERENCESYSTEM_H
#include <terra/CoordinateReferenceSystem.h>
#endif

#include <stdexcept>
#include <vector>

namespace terra {

   /**
    * This exception can be used to indicate that a specific CRS is not supported.
    * Alternative CRSs that are supported may be provided via this exception.
    */
   class UnsupportedCRS : public ::std::runtime_error
   {
         /**
          * Create an unsupported CRS exception.
          * @param crs the crs that is not supported
          * @param supported the different CRS's supported (may be empty)
          */
      public:
         UnsupportedCRS(const ::timber::Reference< CoordinateReferenceSystem>& crs,
               ::std::vector< ::timber::Reference< CoordinateReferenceSystem> >& supported);

         /**
          * Create an unsupported CRS exception and provide an alternative CRS that is supported.
          * @param crs the crs that is not supported
          * @param supported a supported CRS
          */
      public:
         UnsupportedCRS(const ::timber::Reference< CoordinateReferenceSystem>& crs,
               const ::timber::Reference< CoordinateReferenceSystem>& supported);

         /**
          * Create an unsupported CRS exception and provide an alternative CRS that is supported.
          * @param crs the crs that is not supported
          */
      public:
         UnsupportedCRS(const ::timber::Reference< CoordinateReferenceSystem>& crs);

         /** Destructor */
      public:
         ~UnsupportedCRS() throw();

         /**
          * Get the CRS that caused this exception.
          * @return the CRS that is unsupported by the object that has thrown this exception
          */
      public:
         inline const ::timber::Reference< CoordinateReferenceSystem>& cause() const throw()
         {
            return _unsupported;
         }

         /**
          * Get a CRS that are supported. The list of reference systems returned in the vector is not
          * necessarily an exhaustive list.
          * @return a vector of supported CRS's
          */
      public:
         inline const ::std::vector< ::timber::Reference< CoordinateReferenceSystem> >& supportedCRS() const throw()
         {
            return _supported;
         }

         /** The crs that is not supported */
      private:
         ::timber::Reference< CoordinateReferenceSystem> _unsupported;

         /** The crs that are supported */
      private:
         ::std::vector< ::timber::Reference< CoordinateReferenceSystem> > _supported;
   };

}

#endif
