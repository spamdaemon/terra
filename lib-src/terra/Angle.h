#ifndef _TERRA_ANGLE_H
#define _TERRA_ANGLE_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <cmath>

namespace terra {
  
  /**
   * A convencience allow clients to easily work with angles.
   */
  class Angle {
    /** The PI */
  public:
    static constexpr double PI = 3.14159265358979323846;
    
    /**
     * Create an angle of 0 degree and 0 radians. Azimuth is 90 degree.
     */
  public:
    inline constexpr Angle () : _radians(0) {}
    
    /** 
     * Create a new angle
     * @param rad the angle in radians
     */
  private:
    inline constexpr Angle (double r) : _radians(r) {}
    
    /** 
     * Copy constructor.
     */
  public:
    inline constexpr Angle (const Angle& a) : _radians(a._radians) {}
    
    /**
     * Create an angle in radians.
     * @param phi an angle in radians
     * @return an angle
     */
  public:
    inline static Angle fromRadians (double phi) throws() { return Angle(phi); }

    /**
     * Create an angle in degree.
     * @param phi an angle in degree
     * @return an angle
     */
  public:
    inline static Angle fromDegrees (double phi) throws() { return Angle((PI/180.0)*phi); }

    /**
     * Create an angle in radians azimuth.
     * @param az an azimuth angle in radians
     * @return an angle
     */
  public:
    inline static Angle fromRadiansAzimuth (double az) throws() { return Angle(PI/2.0-az); }

    /**
     * Create an angle in degree azimuth.
     * @param az an azimuth angle in degree
     * @return an angle
     */
  public:
    inline static Angle fromDegreesAzimuth (double az) throws() { return Angle(PI/2.0-(PI/180)*az); }

    /**
     * Get the angle in radians.
     * @return the angle in radians
     */
  public:
    inline double radians () const throws() { return _radians; }

    /**
     * Get the angle in degrees
     * @return the angle in degrees
     */
  public:
    inline double degrees () const throws() { return (180/PI)*_radians; }

    /**
     * Get the angle in azimuth.
     * @return the angle in azimuth radians
     */
  public:
    inline double azimuthRadians () const throws() { return PI/2.0-_radians; }

    /**
     * Get the angle in azimuth.
     * @return the angle in azimuth radians
     */
  public:
    inline double azimuthDegree () const throws() { return 180/PI*(PI/2.0-_radians); }
      
    /**
     * Get the cos for this angle
     * @return the cos of this angle
     */
  public:
    inline double cos () const throws() { return ::std::cos(_radians); }

    /**
     * Get the sin for this angle
     * @return the sin of this angle
     */
  public:
    inline double sin () const throws() { return ::std::sin(_radians); }

    /**
     * Get the tan for this angle
     * @return the tan of this angle
     */
  public:
    inline double tan () const throws() { return ::std::tan(_radians); }

    /** The angle in radians */
  private:
    double _radians;
  };
}

#endif
