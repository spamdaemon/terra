#include <terra/GeodeticDatum.h>

namespace terra {
  
  GeodeticDatum::GeodeticDatum (const ::timber::Reference<Spheroid>& s) throws()
  : _spheroid(s)
  {}
  
  GeodeticDatum::~GeodeticDatum() throws()
  {}

  ::timber::Reference<GeodeticDatum> GeodeticDatum::create() throws()
  {
    return new GeodeticDatum(Spheroid::create());
  }
  ::timber::Reference<GeodeticDatum> GeodeticDatum::createEPSG6326() throws()
  {
    return new GeodeticDatum(Spheroid::getEPSG7030());
  }
  ::timber::Reference<GeodeticDatum> GeodeticDatum::create(const ::timber::Reference<Spheroid>& s) throws()
  { return new GeodeticDatum(s); }


  bool GeodeticDatum::equals (const Datum& e) const throws()
  {
    const GeodeticDatum* f = dynamic_cast<const GeodeticDatum*>(&e);
    return f!=0 && _spheroid->equals(*f->_spheroid);
  }


}
