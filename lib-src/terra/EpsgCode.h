#ifndef _TERRA_EPSGCODE_H
#define _TERRA_EPSGCODE_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

#include <cstdint>
#include <string>

namespace terra {

   /**
    * This class is used to represent an EPSG code. For more information about
    * EPSG Codes, visit the <a href="http://www.epsg-registry.org/">EPSG Registry</a>.
    */
   class EpsgCode
   {

         /** The EPSG base type */
      public:
         enum BaseType
         {
            /** This represents the undefined epsg code */
            UNDEFINED,

            /** A CRS type */
            CRS
         };

         /** The code itself is just an unsigned integer */
      public:
         typedef ::std::uint32_t Code;

         /**
          * Create the null code.
          */
      public:
         EpsgCode()throws();

         /**
          * Create a new code. No test is performed that the given code is a valid code.
          * @param type the base type
          * @param code the epsg code
          */
      public:
         EpsgCode(BaseType type, size_t code)throws();

         /**
          * Get the base type.
          * @return the base type
          */
      public:
         inline BaseType type() const throws() {return _type;}

         /**
          * Get the code
          * @return the epsg code
          */
      public:
         inline Code code() const throws() {return _code;}

         /**
          * Compare two codes.
          */
      public:
         bool operator==(const EpsgCode& c) const throws();

         /**
          * Compare two codes.
          */
      public:
         bool operator<(const EpsgCode& c) const throws();

         /**
          * Get this code as a string. If the code is undefined , this method returns an empty string.
          * @return a string of the form <tt>EPSG:&lt;code&gt;</tt>.
          */
      public:
         ::std::string toString() const throws();

         /** The code */
      private:
         Code _code;

         /** The type */
      private:
         BaseType _type;
   };
}

/**
 * Print a representation of the code to the specified output stream. This prints the string <tt>EPSG:&ltcode()&gt;</tt>.
 * @param out an output stream
 * @param code a code
 * @return out
 */
::std::ostream& operator<<(::std::ostream& out, const ::terra::EpsgCode& code);

#endif
