#include <terra/ConversionFailure.h>

namespace terra {
   ConversionFailure::ConversionFailure() :
      ::std::runtime_error("Conversion failure")
   {
   }


   ConversionFailure::~ConversionFailure() throw()
   {
   }

}
