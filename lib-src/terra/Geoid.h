#ifndef _TERRA_GEOID_H
#define _TERRA_GEOID_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

namespace terra {
  
    /**
     * Instances of this class define the various geoids
     * used to approximate the true geoid. According to the
     * <a href="www.nasa.gov">NASA</a>, the geoid is:
     * <p><center>
     * the baseline figure of the Earth, considered as a sea-level 
     * surface including local gravitational effects, without 
     * accounting for topographic features, and extended over 
     * the entire Earth's surface.
     * </center></p>>
     */
  class Geoid : public ::timber::Counted {
    /**
     * Access the default geoid.
     */
  protected:
    Geoid () throws();
    
    /**
     * Destroy this geoid reference.
     */
  public:
    ~Geoid () throws();
  };
}

#endif
