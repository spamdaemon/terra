#include <terra/Extent.h>
#include <terra/Bounds.h>
#include <terra/GeodeticCoordinates.h>
#include <iostream>

namespace terra {

   using namespace ::timber;

   Extent::Extent()
   throws()
         : _minx(-PI), _miny(-PI / 2), _maxx(PI), _maxy(PI / 2), _width(2 * PI), _height(PI)
   {
   }

   Extent::Extent(double lat, double lon)
   throws()
         : _minx(lon), _miny(lat), _maxx(lon), _maxy(lat), _width(0), _height(0)
   {
      if (lat > PI / 2) {
         lat = PI / 2;
      }
      else if (lat < -PI / 2) {
         lat = -PI / 2;
      }
      _miny = _maxy = lat;
   }

   Extent::Extent(double minLat, double minLon, double maxLat, double maxLon)
   throws()
         : _minx(minLon), _miny(minLat), _maxx(maxLon), _maxy(maxLat)
   {
      if (minLat < -PI / 2) {
         _miny = -PI / 2;
      }
      else if (minLat > PI / 2) {
         _miny = PI / 2;
      }

      if (maxLat > PI / 2) {
         _maxy = PI / 2;
      }
      else if (maxLat < -PI / 2) {
         _maxy = -PI / 2;
      }
      if (_miny > _maxy) {
         double t = _miny;
         _miny = _maxy;
         _maxy = t;
      }
      _width = _maxx - _minx;
      _height = _maxy - _miny;
   }

   Extent::Extent(const Bounds& bnds)
   throws()
   {
      const GeodeticCoordinates tl = bnds.northWest();
      const GeodeticCoordinates br = bnds.southEast();
      double maxLat = tl.latitude();
      double minLat = br.latitude();
      double minLon = tl.longitude();
      double maxLon = br.longitude();
      if (maxLon < minLon) {
         maxLon += 2.0 * PI;
      }
      _minx = minLon;
      _miny = minLat;
      _maxx = maxLon;
      _maxy = maxLat;
      _width = maxLon - minLon;
      _height = maxLat - minLat;
   }

   Extent::Extent(const GeodeticCoordinates& c)
   throws()
         : _minx(c.longitude()), _miny(c.latitude()), _maxx(c.longitude()), _maxy(c.latitude()), _width(0), _height(0)
   {
   }

   Extent Extent::fromDegrees(double lat0, double leftLon, double lat1, double rightLon)
   throws()
   {
      return Extent(toRadians(lat0), toRadians(leftLon), toRadians(lat1), toRadians(rightLon));
   }

   Extent Extent::createMinimumExtent(const GeodeticCoordinates& c1, const GeodeticCoordinates& c2)
   throws()
   {
      double minLat, maxLat;
      if (c1.latitude() < c2.latitude()) {
         minLat = c1.latitude();
         maxLat = c2.latitude();
      }
      else {
         minLat = c2.latitude();
         maxLat = c1.latitude();
      }

      double minLon, maxLon;
      if (c1.longitude() < c2.longitude()) {
         minLon = c1.longitude();
         maxLon = c2.longitude();
      }
      else {
         minLon = c2.longitude();
         maxLon = c1.longitude();
      }
      if (maxLon - minLon > PI) {
         minLon += 2.0 * PI;
         ::std::swap(minLon, maxLon);
      }
      return Extent(minLat, minLon, maxLat, maxLon);
   }

   Extent Extent::createMaximumExtent(const GeodeticCoordinates& c1, const GeodeticCoordinates& c2)
   throws()
   {
      double minLat, maxLat;
      if (c1.latitude() < c2.latitude()) {
         minLat = c1.latitude();
         maxLat = c2.latitude();
      }
      else {
         minLat = c2.latitude();
         maxLat = c1.latitude();
      }

      double minLon, maxLon;
      if (c1.longitude() < c2.longitude()) {
         minLon = c1.longitude();
         maxLon = c2.longitude();
      }
      else {
         minLon = c2.longitude();
         maxLon = c1.longitude();
      }
      if (maxLon - minLon < PI) {
         maxLon -= 2.0 * PI;
         ::std::swap(minLon, maxLon);
      }
      return Extent(minLat, minLon, maxLat, maxLon);
   }

   Extent Extent::equivalentExtent() const throws()
   {
      double minLat = south();
      double minLon = west();
      double maxLat = north();
      double maxLon = east();

      if ((maxLon - minLon) >= 2.0 * PI) {
         minLon = -PI;
         maxLon = +PI;
      }
      else if (minLon < -PI) {
         double shift = clampLongitude(minLon) - minLon;
         minLon += shift;
         maxLon += shift;
      }
      else if (minLon > PI) {
         double shift = clampLongitude(minLon) - minLon;
         minLon -= shift;
         maxLon -= shift;
      }
      return Extent(minLat, minLon, maxLat, maxLon);
   }

   Bounds Extent::getBounds(const ::timber::Reference< GeodeticDatum>& datum) const throws()
   {
      double minLat = south();
      double minLon = west();
      double maxLat = north();
      double maxLon = east();

      if (minLon < -PI) {
         double shift = clampLongitude(minLon) - minLon;
         minLon += shift;
         maxLon += shift;
      }
      else if (minLon > PI) {
         double shift = clampLongitude(minLon) - minLon;
         minLon -= shift;
         maxLon -= shift;
      }
      if ((maxLon - minLon) >= 2.0 * PI) {
         minLon = -PI;
         maxLon = +PI;
      }
      else {
         maxLon = clampLongitude(maxLon);
      }

      GeodeticCoordinates tl = GeodeticCoordinates(maxLat, minLon, datum);
      GeodeticCoordinates br = GeodeticCoordinates(minLat, maxLon, datum);
      return Bounds(tl, br);
   }

   bool Extent::shiftTowards(const Extent& e)
   throws()
   {
      // check really quickly if the latitudes even overlap
      if ((e.south() > north()) || (e.north() < south())) {
         return false;
      }

      if (east() < e.west()) {
         // shift this extent eastward
         double k = 0;
         double minLon, maxLon;
         do {
            ++k;
            maxLon = east() + (2 * k) * PI;
         } while (maxLon < e.west());
         minLon = west() + (2 * k) * PI;

         if (minLon < e.east()) {
            _minx = minLon;
            _maxx = maxLon;
            _width = _maxx - _minx;
            return true;
         }
      }
      else if (west() > e.east()) {
         // shift this extent eastward
         double k = 0;
         double minLon, maxLon;
         do {
            ++k;
            minLon = west() - (2 * k) * PI;
         } while (minLon > e.east());

         maxLon = east() - (2 * k) * PI;

         if (maxLon > e.west()) {
            _minx = minLon;
            _maxx = maxLon;
            _width = _maxx - _minx;
            return true;
         }
      }
      else {
         return true;
      }
      return false;
   }

   bool Extent::contains(double lat, double lon) const throws()
   {
      return _minx <= lon && lon <= _maxx && _miny <= lat && lat <= _maxy;
   }

   bool Extent::contains(double lat, double lon, double err) const throws()
   {
      assert(err >= 0.0);
      return (_minx - err) <= lon && lon <= (_maxx + err) && (_miny - err) <= lat && lat <= (_maxy + err);
   }

   bool Extent::intersects(const Extent& b) const throws()
   {
      if (_minx > b._maxx) {
         return false;
      }
      if (_miny > b._maxy) {
         return false;
      }
      if (_maxx < b._minx) {
         return false;
      }
      if (_maxy < b._miny) {
         return false;
      }
      return true;
   }

   bool Extent::encloses(const Extent& b) const throws()
   {
      if (_minx <= b._minx && b._maxx <= _maxx) {
         if (_miny <= b._miny && b._maxy <= _maxy) {
            return true;
         }
      }
      return false;
   }

   Extent& Extent::merge(const Extent& b)
   throws()
   {
      _minx = ::std::min(_minx, b._minx);
      _maxx = ::std::max(_maxx, b._maxx);
      _miny = ::std::min(_miny, b._miny);
      _maxy = ::std::max(_maxy, b._maxy);
      _width = _maxx - _minx;
      _height = _maxy - _miny;
      return *this;
   }

   ::std::ostream& Extent::print(::std::ostream& out) const
   {
      out << "Extent ( " << _minx << ", " << _miny << ") -> ( " << _maxx << ", " << _maxy << ")";
      return out;
   }

}

