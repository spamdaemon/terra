#include <terra/geoid/Geoid.h>
#include <terra/GeodeticDatum.h>
#include <terra/GeodeticCoordinates.h>
#include <terra/terra.h>
#include <timber/logging.h>

#include <cmath>
#include <iostream>
#include <sstream>

using namespace ::std;
using namespace ::canopy;
using namespace ::timber;
using namespace ::timber::logging;

namespace terra {
  namespace geoid {
    namespace {

    }

    Geoid::Geoid() throws()
    : _heightError(0),
      _datum(GeodeticDatum::createWGS84()),
      _west(0),_east(0),_south(0),_north(0),
      _deltaLat(0),_deltaLon(0),
      _nLongitudePosts(0),_nLatitudePosts(0),
      _elevation(0)
    {}

    Geoid::~Geoid() throws()
    {
      delete[] _elevation;
    }

    Reference<Geoid> Geoid::read(istream& stream) throws (exception)
    {
      double south,north,west,east,deltaLat,deltaLon;
      
      stream >> south;
      if (stream.fail()) {
	throw ::std::runtime_error("Could not read southern latitude");
      }
      stream >> north;
      if (stream.fail()) {
	throw ::std::runtime_error("Could not read northern latitude");
      }
      stream >> west;
      if (stream.fail()) {
	throw ::std::runtime_error("Could not read western longitude");
      }
      stream >> east;
      if (stream.fail()) {
	throw ::std::runtime_error("Could not read eastern longitude");
      }
      stream >> deltaLat;
      if (stream.fail()) {
	throw ::std::runtime_error("Could not read delta latitude");
      }
      stream >> deltaLon;
      if (stream.fail()) {
	throw ::std::runtime_error("Could not read delta longitude");
      }

      size_t nLatPosts = (north - south)/deltaLat;
      size_t nLonPosts = (east - west)/deltaLon;

      Reference<Geoid> geoid = new Geoid();
      geoid->_elevation = new float[(nLatPosts+1)*(nLonPosts+1)];

      for (size_t lat=0;lat<nLatPosts;++lat) {
	for (size_t lon=0;lon<nLonPosts;++lon) {
	  float height;
	  stream >> height;
	  if (stream.fail()) {
	    throw runtime_error("Could not read geoid height");
	  }
	  geoid->_elevation[lat * (nLonPosts+1) + lon] = height;
	  geoid->_elevation[(lat+1) * (nLonPosts+1) + lon] = height;
	  geoid->_elevation[(lat+1) * (nLonPosts+1) + (lon+1)] = height;
	  geoid->_elevation[lat * (nLonPosts+1) + (lon+1)] = height;
	}
	
	// there's a left-over
	{
	  float height;
	  stream >> height;
	  if (stream.fail()) {
	    throw runtime_error("Could not read trailing geoid height");
	  }
	}
      }

      geoid->_south = toRadians(south);
      geoid->_north = toRadians(north);
      geoid->_west = clampLongitude(toRadians(west));
      geoid->_east = clampLongitude(toRadians(east));
      geoid->_deltaLat= toRadians(deltaLat);
      geoid->_deltaLon= toRadians(deltaLon);
      geoid->_nLatitudePosts = nLatPosts;
      geoid->_nLongitudePosts = nLonPosts;

      return geoid;
    }

    Reference<GeodeticDatum> Geoid::datum() const throws()
    { return _datum; }
    
    double Geoid::heightAboveEllipsoid(double lat, double lon, bool& isValid) const throws ()
    {
      lon = clampLongitude(lon);
      if (lon<0) {
	lon += 2*PI;
      }
      // shift the point so we don't have to account for the southwest point
      // in the subsequent calculations
      lat = _north - lat;
      lon -= _west;
      
      if (lat<0 || lon < 0) {
	isValid = false;
	return 0;
      }
      // compute s and w, which is the closest post (south-west of the given point)
      // we also compute dNS and dEW, which is a value between 0 and 1 and represents
      // the distance of the given point from the southwest point
      size_t s = static_cast<size_t>(lat/_deltaLat);
      size_t w = static_cast<size_t>(lon/_deltaLon);

      // check for validity of the region
      if (s >= _nLatitudePosts || w>=_nLongitudePosts) {
	isValid = false;
	return 0;
      }

      const double dNS = lat/_deltaLat - static_cast<double>(s);
      const double dEW =  lon/_deltaLon - static_cast<double>(w);
      
      // get the heights of the four posts around the given point
      // because we've made the elevation array larger, we don't have to
      // worry about accessing invalid memory
      const float* nw = _elevation+s*(_nLongitudePosts+1)+w;
      const float* sw = nw+(_nLongitudePosts+1);
      const float* se = sw+1;
      const float* ne = nw+1;
      
      // do bilinear interpolation, first across west->east, then south->north
      double xnorth = (1-dEW)*(*nw) + (dEW)*(*ne);
      double xsouth = (1-dEW)*(*sw) + (dEW)*(*se);
      double center= (1-dNS)*xnorth + (dNS)*xsouth;

      isValid = true;
      return center;
    }

    double Geoid::heightError (double , double ) const throws()
    { return _heightError; }
    

    double Geoid::postHeight (size_t latPostIndex, size_t lonPostIndex) const throws()
    {
      assert(latPostIndex < _nLatitudePosts);
      assert(lonPostIndex < _nLongitudePosts);
      // the width of the elevation grid is +1
      return _elevation[latPostIndex * (_nLongitudePosts+1) + lonPostIndex];
    }
  }
}
