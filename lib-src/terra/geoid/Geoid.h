#ifndef _TERRA_GEOID_GEOID_H
#define _TERRA_GEOID_GEOID_H

#ifndef _TERRA_HEIGHTMAP_H
#include <terra/HeightMap.h>
#endif

#ifndef _TERRA_GEODETICDATUM_H
#include <terra/GeodeticDatum.h>
#endif

#include <iosfwd>
#include <cassert>

namespace terra {
  namespace geoid {
    
    /**
     * A single geoid geoid implements the HeightMap interface over a rectangular
     * region. By spec, all heights contained in a GEOID file are with respect
     * to mean-sea level. 
     */
    class Geoid : public HeightMap {
      /** Copying disabled */
      Geoid(const Geoid&);
      Geoid&operator=(const Geoid&);

      /**
       * Constructor is private.
       */
    private:
      Geoid() throws();

      /**
       * Destructor
       */
    public:
      ~Geoid() throws();

      /**
       * Read a new geoid from an input stream containing a single
       * geoid at the current stream position.
       * @param stream an input stream
       * @return a geoid
       * @throws ::std::exception if some kind of error occurred
       */
    public:
      static ::timber::Reference<Geoid> read(::std::istream& stream) throws (::std::exception);
      
      /**
       * Get the longitude of the western geoid edge.
       * @return the longitude of western geoid edge
       */
    public:
      inline double west() const throws()
      { return _west; }

      /**
       * Get the longitude of the eastern geoid edge.
       * @return the longitude of eastern geoid edge
       */
    public:
      inline double east() const throws()
      { return _east; }

      /**
       * Get the latitude of the northern geoid edge.
       * @return the latitude of northern geoid edge
       */
    public:
      inline double north() const throws()
      { return _north; }

      /**
       * Get the latitude of the southern geoid edge.
       * @return the latitude of southern geoid edge
       */
    public:
      inline double south() const throws()
      { return _south; }

      /**
       * Get the number of posts in latitude.
       * @return the number of latitude points per longitude
       */
    public:
      inline size_t numLatitudePosts() const throws()
      {return _nLatitudePosts; }

      /**
       * Get the number of posts in longitude.
       * @return the number of longitude posts
       */
    public:
      inline size_t numLongitudePosts() const throws()
      {return _nLongitudePosts; }
      
      /**
       * Get the elevation values for a given post.
       * @param latPostIndex the index of a latitude post
       * @param lonPostIndex the index of a longitude post
       * @return the height at the specified post
       * @pre latPostIndex < numLatitudePosts()
       * @pre lonPostIndex < numLongitudePosts()
       */
    public:
      double postHeight (size_t latPostIndex, size_t lonPostIndex) const throws();
      
      /**
       * Get the underlying datum. All height queries return height
       * values with respect to this datum.
       * @return the datum
       */
    public:
      ::timber::Reference<GeodeticDatum> datum() const throws();
      
      /**
       * Get the height above the ellipsoid for a point on this height map.
       * 
       * @param lat the latitude of a point in radians
       * @param lon the longitude of a point in radians
       * @param isValid a flag indicating if the returned height is valid
       * @return the height above the ellipsoid (in meters)
       */
    public:
      double heightAboveEllipsoid(double lat, double lon, bool& isValid) const throws ();

      /**
       * Get the error in height that is expected at the specified latitude and longitude.
       * @param lat the latitude of a point in radians
       * @param lon the longitude of a point in radians
       * @return the 1-sigma error in height above the ellipsoid, or 0 if the location is an invalid one
       */
    public:
      double heightError (double lat, double lon) const throws();

       /** The height error */
    private:
      double _heightError;

      /** The spheroid */
    private:
      ::timber::Reference<GeodeticDatum> _datum;

      /** The western longitude (degrees) */
    private:
      double _west;

      /** The eastern longitude (degrees) */
    private:
      double _east;

      /** The south latitude (degrees) */
    private:
      double _south;

      /** The north latitude (degrees) */
    private:
      double _north;

      /** The size of a grid cell in latitude degrees */
    private:
      double _deltaLat;
      
      /** The the size */
    private:
      double _deltaLon;

      /** The number of points in longitude */
    private:
      size_t _nLongitudePosts;

      /** The number of points in latitude */
    private:
      size_t _nLatitudePosts;

      /** The elevation data (float precision is sufficient) */
    private:
      float* _elevation;
    };

  }
}


#endif
