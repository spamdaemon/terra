#include <terra/Bounds.h>
#include <terra/Loxodrome.h>
#include <terra/terra.h>
#include <iostream>
#include <cmath>

using namespace ::std;
using namespace ::timber;

namespace terra {

  namespace {
    inline bool checkOverlap(double aL, double aR, double bL, double bR)
    {
      return !((aL > bR) || (aR < bL));
    }

    inline void merge (double aL, double aR, double& bL, double& bR)
    {
      bL = min(aL,bL);
      bR = max(aR,bR);
    }
    inline Int intersectSimple(const Reference<GeodeticDatum>& B, double minLat, double maxLat, double westA, double eastA, double westB, double eastB, Bounds& b)
    {
      assert(westA<=eastA);
      assert(westB<=eastB);

      double minLon = max(westA,westB);
      double maxLon = min(eastA,eastB);
	
      if (minLon <= maxLon) {
	b = Bounds(GeodeticCoordinates(maxLat,clampLongitude(minLon),B),GeodeticCoordinates(minLat,clampLongitude(maxLon),B));
	return 1;
      }
      return 0;
    }
  }

  Bounds::Bounds (double lat0, double westLon, double lat1, double eastLon, const Reference<GeodeticDatum>& s) throws()
  {
    const double minLat = min(lat0,lat1);
    const double maxLat = max(lat0,lat1);

    _northWest = GeodeticCoordinates(maxLat,westLon,s);
    _southEast = GeodeticCoordinates(minLat,eastLon,s);
  }

  Bounds Bounds::fromDegrees (double lat0, double westLon, double lat1, double eastLon, const Reference<GeodeticDatum>& s) throws()
  {
    return Bounds(toRadians(lat0),toRadians(westLon),toRadians(lat1),toRadians(eastLon),s);
  }
  

  Bounds::Bounds (const GeodeticCoordinates& northwest, const GeodeticCoordinates& southeast) throws()
  : _northWest(northwest),_southEast(southeast)
  {
    assert(northwest.datum()->equals(*southeast.datum()));

    if (northwest.latitude()<southeast.latitude()) {
      _northWest=GeodeticCoordinates(southeast.latitude(),northwest.longitude(),northwest.datum());
      _southEast=GeodeticCoordinates(northwest.latitude(),southeast.longitude(),northwest.datum());
    }
  }

  Bounds::Bounds() throws()
  : _northWest(GeodeticCoordinates::fromDegrees(90,-180)),
    _southEast(GeodeticCoordinates::fromDegrees(-90,180))
  {
  }

  Bounds::Bounds(const Reference<GeodeticDatum>& d) throws()
  : _northWest(GeodeticCoordinates::fromDegrees(90,-180,d)),
    _southEast(GeodeticCoordinates::fromDegrees(-90,180,d))
  {
  }

  Bounds Bounds::create (const GeodeticCoordinates& a, const GeodeticCoordinates& b) throws()
  {
    assert(a.datum()->equals(*b.datum()));
    double xnorth = max(a.latitude(),b.latitude());
    double xsouth = min(a.latitude(),b.latitude());
    double xwest,xeast;
    if (abs(b.longitude()-a.longitude()) > PI) {
      xwest = max(a.longitude(),b.longitude());
      xeast = min(a.longitude(),b.longitude());
    }
    else {
      xwest = min(a.longitude(),b.longitude());
      xeast = max(a.longitude(),b.longitude());
    }
    GeodeticCoordinates tl,br;
    if (xwest==a.longitude() && xnorth==a.latitude()) {
      tl = a;
    }
    else if (xwest==b.longitude() && xnorth==b.latitude()) {
      tl = b;
    }
    else {
      tl = GeodeticCoordinates(xnorth,xwest,a.datum());
    }
    if (xeast==a.longitude() && xsouth==a.latitude()) {
      br = a;
    }
    else if (xeast==b.longitude() && xsouth==b.latitude()) {
      br = b;
    }
    else {
      br = GeodeticCoordinates(xsouth,xeast,a.datum());
    }
    return Bounds(tl,br);
  }


  Bounds Bounds::merge (const Bounds& b) const throws()
  {
    if (&b==this) {
      return *this;
    }
    assert(datum()->equals(*b.datum()));
      
    double westA = _northWest.longitude();
    double eastA = _southEast.longitude();
    double westB = b._northWest.longitude();
    double eastB = b._southEast.longitude();
    const double DELTA= 2.0*PI;

    if (westA > eastA) {
      eastA += DELTA;
    }
    if (westB > eastB) {
      eastB += DELTA;
    }
    double longitudes[3][2];
    longitudes[0][0] = min(westA,westB);
    longitudes[0][1] = max(eastA,eastB);
    longitudes[1][0] = min(westA,westB+DELTA);
    longitudes[1][1] = max(eastA,eastB+DELTA);
    longitudes[2][0] = min(westA,westB-DELTA);
    longitudes[2][1] = max(eastA,eastB-DELTA);
      
    size_t i=0;
    double diff = longitudes[0][1] - longitudes[0][0];
      
    for (size_t j=1;j<3;++j) {
      double xDiff = longitudes[j][1] - longitudes[j][0];
      if (xDiff < diff) {
	i=j;
	diff = xDiff;
      }
    }
      
    double minLon = clampLongitude(longitudes[i][0]);
    double maxLon = clampLongitude(longitudes[i][1]);
    if (diff >= 2.0*PI) {
      minLon = -PI;
      maxLon =  PI;
    }
    const double minLat = min(south(),b.south());
    const double maxLat = max(north(),b.south());

    return Bounds(GeodeticCoordinates(maxLat,minLon,datum()),GeodeticCoordinates(minLat,maxLon,datum()));
  }
    
  bool Bounds::intersects (const Bounds& b) const throws()
  { 
    Bounds tmp[3];
    return intersect(b,tmp) > 0; 
  }

  Int Bounds::intersect (const Bounds& b, Bounds result[3]) const throws()
  {
    assert(datum()->equals(*b.datum()));
    // check latitude first
    const double maxLat = min(north(),b.north());
    const double minLat = max(south(),b.south());
    if (maxLat < minLat) {
      return 0;
    }
    const Reference<GeodeticDatum>& B = b.datum();

    size_t i = 0;
    if (west() < east()) {
      if(b.west() < b.east()) {
	i = intersectSimple(B,minLat,maxLat,west(),east(),b.west(),b.east(),result[0]);
	if (i==0) {
	  i = intersectSimple(B,minLat,maxLat,west()+2*PI,east()+2*PI,b.west(),b.east(),result[0]);
	}
	if (i==0) {
	  i = intersectSimple(B,minLat,maxLat,west()-2*PI,east()-2*PI,b.west(),b.east(),result[0]);
	}
      }
      else {
	i += intersectSimple(B,minLat,maxLat,west(),east(),b.west(),PI,result[0]);
	i += intersectSimple(B,minLat,maxLat,west(),east(),-PI,b.east(),result[i]);
      }
    }
    else {
      if (b.west() < b.east()) {
	i += intersectSimple(B,minLat,maxLat,west(),PI,b.west(),b.east(),result[0]);
	i += intersectSimple(B,minLat,maxLat,-PI,east(),b.west(),b.east(),result[i]);
      }
      else {
	i = intersectSimple(B,minLat,maxLat,west(),PI,b.west(),PI,result[0]);
	i += intersectSimple(B,minLat,maxLat,west(),PI,-PI,b.east(),result[i]);
	i += intersectSimple(B,minLat,maxLat,-PI,east(),b.west(),PI,result[i]);
	i += intersectSimple(B,minLat,maxLat,-PI,east(),-PI,b.east(),result[i]);
      }
    }
      
    if (i==2) {
      if (result[0].east()==PI && result[1].west()==-PI) {
	result[0] = Bounds(GeodeticCoordinates(maxLat,result[0].west(),B),GeodeticCoordinates(minLat,result[1].east(),B));
	i = 1;
      }
      else if (result[1].east()==PI && result[0].west()==-PI) {
	result[0] = Bounds(GeodeticCoordinates(maxLat,result[1].west(),B),GeodeticCoordinates(minLat,result[0].east(),B));
	i = 1;
      }
    }
          
    return i;
  }

  GeodeticCoordinates Bounds::center() const throws()
  {
    double lat = 0.5*(north() + south());
    double lon = 0.5*(west()+east());
    double alt = 0.5*(_northWest.height()+_southEast.height());

    if (east() < west()) {
      lon += PI;
    }
    lon = clampLongitude(lon);
    return GeodeticCoordinates(lat,lon,alt,datum());
  }

  ::std::ostream& Bounds::print (::std::ostream& out) const
  {
    out << "[" 
	<< south() << "," << west() << ";" 
	<< north()    << "," << east() 
	<< " ] ";
    return out;
  }

  bool Bounds::containsLongitude(double lon) const throws()
  {
    double xwest = _northWest.longitude();
    double xeast = _southEast.longitude();
    if (xwest>xeast) {
      return xwest <= lon || lon <= xeast;
    }
    else {
      return xwest <= lon && lon <= xeast;
    }
  }

  GeodeticCoordinates Bounds::northEast() const throws() 
  { return GeodeticCoordinates(north(),east(),0,datum()); }
      
  GeodeticCoordinates Bounds::southWest() const throws() 
  { return GeodeticCoordinates(south(),west(),0,datum()); }

  
  double Bounds::edgeLength (Edge edge) const throws()
  {
    if (edge==NORTH || edge==SOUTH) {
      double lon0 = west();
      double lon1 = east();
      
      double dlon = east()-west();
      if (dlon< 0) {
	dlon += 2*PI;
      }
      return datum()->spheroid().parallelArcLength(edge==NORTH ? north() : south(),dlon);
    }
    else {
      // vertical distance is the same
      return  datum()->spheroid().meridianArcLength(south(),north());
    }

  }

    
  
}
