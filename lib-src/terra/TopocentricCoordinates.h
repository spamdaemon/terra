#ifndef _TERRA_TOPOCENTRICCOORDINATES_H
#define _TERRA_TOPOCENTRICCOORDINATES_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif 

#ifndef _TERRA_GEODETICCOORDINATES_H
#include <terra/GeodeticCoordinates.h>
#endif 

#ifndef _TERRA_TOPOCENTRICFRAME_H
#include <terra/TopocentricFrame.h>
#endif 

#include <iosfwd>
#include <cmath>

namespace terra {   
  class ECEFCoordinates;

  /**
   * TopocentricCoordinates are defined within a local reference frame defined by aTopocentricFrame.
   * Witin a small region around the center of the topocentric frame, the coordinates are a good 
   * approximation to the ellipsoid and provide a cartesian coordinate frame which simplifies work
   * with geographic coodinates.
   */
  class TopocentricCoordinates {

    /**
     * Create topocentric coordinates using a default frame.
     * The default coordinate frame is created by TopocentricFrame::create().
     */
  public:
    TopocentricCoordinates() throws();

    /**
     * Create a TopocentricCoordinates in a default frame. Typically
     * this constructor would be followed by a call to replaceFrame()
     * to move it to the correct coordinate frame.
     * @param px the x-coordinate in meters
     * @param py the x-coordinate in meters
     * @param alt the x-coordinate in meters
     */
  public:
    TopocentricCoordinates (double px, double py, double alt) throws();
    
    /**
     * Create a default TopocentricCoordinates. 
     * @param f a reference frame
     */
  public:
    inline TopocentricCoordinates (const ::timber::Reference<TopocentricFrame>& f) throws()
      : _frame(f)
      { _x[0] = _x[1] = _x[2] = 0; }
    
    /**
     * Create a tcs-origin. The frame of the origin may be used later to 
     * initialize new coordinates.
     * @param gc geodetic coordinates
     */
  public:
    TopocentricCoordinates (const GeodeticCoordinates& gc) throws();

    /**
     * Create tcs-coordinates from geodetic coordinates.
     * @param f a reference frame
     * @param gc geodetic coordinates
     */
  public:
    TopocentricCoordinates (const ::timber::Reference<TopocentricFrame>& f,
			    const GeodeticCoordinates& gc) throws();

     /**
     * Create a tcs-origin. The frame of the origin may be used later to 
     * initialize new coordinates.
     * @param gc ecef coordinates
     */
  public:
    TopocentricCoordinates (const ECEFCoordinates& gc) throws();

    /**
     * Create tcs-coordinates from geodetic coordinates.
     * @param f a reference frame
     * @param gc ecef coordinates
     */
  public:
    TopocentricCoordinates (const ::timber::Reference<TopocentricFrame>& f,
			    const ECEFCoordinates& gc) throws();

    /**
     * Create a TopocentricCoordinates specified by latitude and longitude using
     * the current datum. Positive TopocentricCoordinates indicate the northern
     * hemisphere, and positive longitudes indicate the eastern hemisphere.
     *
     * @param f the geodetic coordinates of the topocentric frame
     * @param px the x-coordinates in meters
     * @param py the x-coordinate in meters
     * @param alt the x-coordinate in meters
     */
  public:
    TopocentricCoordinates (const GeodeticCoordinates& f,
			    double px, double py, double alt) throws();

    /**
     * Create a TopocentricCoordinates specified by latitude and longitude using
     * the current datum. Positive TopocentricCoordinates indicate the northern
     * hemisphere, and positive longitudes indicate the eastern hemisphere.
     *
     * @param f a reference frame
     * @param px the x-coordinate in meters
     * @param py the x-coordinate in meters
     * @param alt the x-coordinate in meters
     */
  public:
    inline TopocentricCoordinates (const ::timber::Reference<TopocentricFrame>& f,
				   double px, double py, double alt) throws()
      : _frame(f)
    { _x[0] = px; _x[1] = py; _x[2] = alt; }
	
    /**
     * Copy operator.
     * @param c a coordinates object
     * @return *this
     */
  public:
    TopocentricCoordinates& operator= (const TopocentricCoordinates& c) throws();

    /**
     * Replace the coordinate frame with a new frame
     * @param frame a new frame
     */
  public:
    void replaceFrame (const  ::timber::Reference<TopocentricFrame>& f) throws();

    /**
     * Get the x-coordinate.
     * @return the x-coordinate
     */
  public:
    inline double x () const throws() { return _x[0]; }

    /**
     * Get the y-coordinate.
     * @return the y-coordinate
     */
  public:
    inline double y () const throws() { return _x[1]; }

    /**
     * Get the z-coordinate.
     * @return the z-coordinate
     */
  public:
    inline double z () const throws() { return _x[2]; }

    /**
     * Get the ecef coordinates for this point.
     * @return the ecef coordinates corresponding to this point.
     */
  public:
    ECEFCoordinates ecefCoordinates() const throws();

    /**
     * Get the geodetic coordinates for this point.
     * @return the geodetic coordinates corresponding to this point.
     */
  public:
    GeodeticCoordinates geodeticCoordinates() const throws();

    /**
     * Get one of the coordinates.
     * @param i a coordinate index
     * @pre REQUIRE_RANGE(i,0,2)
     * @return a reference to the coordinates
     */
  public:
    inline double operator() (size_t i) const throws() 
    {
      assert(i<3);
      return _x[i];
    }
    
    /**
     * Get the coordinates themselves.
     * @return a reference to the coordinates
     */
  public:
    inline const double* coordinates() const throws() { return _x; }

    /**
     * Get the spheroid.
     * @return the spheroid
     */
  public:
    inline const ::timber::Reference<TopocentricFrame>& frame () const throws() 
    { return _frame; }

    /**
     * Compare two TopocentricCoordinates. 
     * It is very likely that two TopocentricCoordinates are different,
     * because they are represented as float values.
     */
  public:
    bool equals (const TopocentricCoordinates& pt) const throws();

    /**
     * Compare two TopocentricCoordinates.
     * @param pt a Topocentric coordinate
     * @return true if <code>this->equals(pt)</code>
     */
  public:
    inline bool operator== (const TopocentricCoordinates& pt) const throws()
    { return equals(pt); }

    /**
     * Compare two TopocentricCoordinates.
     * @param pt a Topocentric coordinate
     * @return true if <code>!this->equals(pt)</code>
     */
  public:
    inline bool operator!= (const TopocentricCoordinates& pt) const throws()
    { return !equals(pt); }

    /**
     * Compute the distance between two TopocentricCoordinates. This distance
     * may not be close the real distance on the spheroid.
     * @param c an ecef coordinate object
     * @pre REQUIRE_EQUAL(c.datum(),datum())
     * @return distance the distance between two Topocentric coordinates.
     */
  public:
    double distance (const TopocentricCoordinates& c) const throws();

    /**
     * Get the angle of this point with respect to the origin.
     * If this point is directly north of the center, then 0 azimuth is returned,
     * directly east is 90 degrees azimuth. 
     * @return the azimuth of this point in radians
     */
  public:
    inline double azimuth() const throws()
    { return ::std::atan2(_x[0],_x[1]); }
    
    /**
     * Determine the elvation angle 
     * @return the elevation angle of this point and the ground plane in radians
     */
  public:
    double elevation() const throws();

    /**
     * Get the range of this point to the origin of its associated frame.
     * @return the distance to the center of the frame
     */
  public:
    inline double range() const throws()
    { return ::std::sqrt(_x[0]*_x[0] + _x[1]*_x[1]+  _x[2]*_x[2] ); }
    
    /**
     * Get the range of this point to the origin on the @code z=0 @endcode plane.
     * @return the distance on the ground to the center of the frame
     */
  public:
    inline double groundRange() const throws()
    { return ::std::sqrt(_x[0]*_x[0] + _x[1]*_x[1]); }
    
    /**
     * Create new coordinates offset by a given vector.
     * @param dx the change in x
     * @param dy the change in y
     * @param dz the change in x
     * @return new coordinates translated by the specified amount
     */
  public:
    TopocentricCoordinates travel (double dx , double dy, double dz) const throws();
    
    /**
     * Travel along a ground range and a heading at the current z value.
     * @param r the ground range
     * @param bearing the bearing in radians azimuth
     * @return new coordinates at the same z values as the original coordinates
     */
  public:
    TopocentricCoordinates travelRangeBearing (double r, double bearingAz) const throws();
    
    /**
     * Travel along a certain distance in a given bearing and elevation angle. Positive
     * elevation angles lead to larger z values, negative ones to smaller z values. 
     * @param r the ground range
     * @param bearingAz the bearing in radians azimuth
     * @param elevAngle the elevation angle in radians
     * @return the topocentric coordinates
     */
  public:
    TopocentricCoordinates travelRangeBearingElevation (double r, double bearingAz, double elevAngle) const throws();
 
    /**
     * Print these coordinates.
     * @param out an output stream
     * @return out
     */
  public:
    ::std::ostream& print ( ::std::ostream& out) const;

    /** The cartesian coordinates */
  private:
    double _x[3];

    /** The datum */
  private:
    ::timber::Reference<TopocentricFrame> _frame;
  };

}

/**
 * Print a TopocentricCoordinates object to a stream
 * @param out a stream
 * @param l a TopocentricCoordinates
 * @return out
 */
inline ::std::ostream& operator<< (::std::ostream& out, const ::terra::TopocentricCoordinates& l)
{ return l.print(out); }

#endif
