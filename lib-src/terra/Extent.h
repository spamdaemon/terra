#ifndef _TERRA_EXTENT_H
#define _TERRA_EXTENT_H

#ifndef _TERRA_H
#include <terra/terra.h>
#endif

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#include <iosfwd>

namespace terra {
   /** A bounds objet */
   class Bounds;

   /** A geodetic coordinates class */
   class GeodeticCoordinates;

   /** A spheroid */
   class GeodeticDatum;

   /**
    * An extent defines a region bound by minimum and maximum latitudes and longitudes. The
    * domain of this extent is -PI/2 to PI/2 in latitude and <em>infinite</em> in longitude.
    * This class behaves differently from Bounds and can represent bounds that wrap around
    * the earth multiple times.
    */
   class Extent
   {
         /**
          * Create a default extent (-PI/2,-PI) -> (PI/2,PI)
          */
      public:
         Extent()throws();

         /**
          * Create an extent that encloses a single point
          * @param c a coordinate
          */
      public:
         Extent(const GeodeticCoordinates& c)throws();

         /**
          * Create an extent that encloses a single point
          * @param c a coordinate
          */
      public:
         Extent(const Bounds& b)throws();

         /**
          * Create an extent that encloses a single point
          * @param lat a latitude
          * @param lon a longitude
          * @pre REQUIRE_RANGE(lat,-PI/2,PI/2);
          */
      public:
         Extent(double lat, double lon)throws();

         /**
          * Create a new extent
          * @param minLat the minimum latitude (bottom)
          * @param minLon the minimum longitude (left)
          * @param maxLat the maximum latitude (top)
          * @param maxLon the maximum longitude (right)
          * @pre REQUIRE_RANGE(minLat,-PI/2,PI/2);
          * @pre REQUIRE_RANGE(maxLat,-PI/2,PI/2);
          */
      public:
         Extent(double minLat, double minLon, double maxLat, double maxLon)throws();

         /**
          * Create an extent from latitude and longitude values specified in degrees.
          * @param lat0 a latitude (in radians)
          * @param leftLon the longitude of the left side (in radians)
          * @param lat1 a latitude (in radians)
          * @param rightLon the longitude of the right side (in radians)
          * @return an extent
          */
      public:
         static Extent fromDegrees(double lat0, double leftLon, double lat1, double rightLon)throws();

         /**
          * Create the minimum extent around two geodetic coordinates. The minimum extent
          * has a longitude difference of PI or less.
          * @param c1 a coordinate
          * @param c2 a coordinate
          * @return the minimum extent that contains both c1 and c2
          */
      public:
         static Extent createMinimumExtent(const GeodeticCoordinates& c1, const GeodeticCoordinates& c2)throws();

         /**
          * Create the maximum extent around two geodetic coordinates. The maximum extent
          * has a longitude difference of PI or more.
          * @param c1 a coordinate
          * @param c2 a coordinate
          * @return the maximum extent that contains both c1 and c2
          */
      public:
         static Extent createMaximumExtent(const GeodeticCoordinates& c1, const GeodeticCoordinates& c2)throws();

         /**
          * Create the smallest extent around two geodetic coordinates.
          * @param c1 a coordinate
          * @param c2 a coordinate
          * @return the smallest extent that contains both c1 and c2
          */
      public:
         static Extent create(const GeodeticCoordinates& c1, const GeodeticCoordinates& c2)throws();

         /**
          * Get the smallest extent that is equivalent to this extent.
          * @return an equivalent, but possibly smaller extent.
          */
      public:
         Extent equivalentExtent() const throws();

         /**
          * Get the bounds of this extent in the specified datum.
          * @param datum a datum
          * @return this extent as a bounds object
          */
      public:
         Bounds getBounds(const ::timber::Reference< GeodeticDatum>& datum) const throws();

         /**
          * Get the minimum latitude
          * @return the minimum latitude
          */
      public:
         inline double south() const throws() {return _miny;}

         /**
          * Get the minimum longitude
          * @return the minimum longitude
          */
      public:
         inline double west() const throws() {return _minx;}

         /**
          * Get the maximum latitude
          * @return the maximum latitude
          */
      public:
         inline double north() const throws() {return _maxy;}

         /**
          * Get the maximum longitude
          * @return the maximum longitude
          */
      public:
         inline double east() const throws() {return _maxx;}

         /**
          * Get the width of this extent. The width is always positive
          * @return the width
          */
      public:
         inline double width() const throws() {return _width;}

         /**
          * Get the height of this extent. The height is always positive
          * @return the width
          */
      public:
         inline double height() const throws() {return _height;}

         /**
          * Test if the specified latitude and longitude is within this extent.
          * No normalization is applied to either this extent or the longitude.
          * If the extent is from 0 to 2*PI and the longitude is specified as -PI,
          * then this method returns false.
          *
          * @param lat a latitude (in radians)
          * @param lon a longitude (in radians)
          * @return true if this extent contains the specified latitude and longitude
          */
      public:
         bool contains(double lat, double lon) const throws();

         /**
          * Test if the specified latitude and longitude is within this extent.
          * The point (or alternatively, the extent) may be assumed to have an error
          * associated with it.
          * @param lat a latitude (in radians)
          * @param lon a longitude (in radians)
          * @param err an error in the location or size of the bounds
          * @pre err >= 0
          * @return true if this extent contains the specified latitude and longitude
          */
      public:
         bool contains(double lat, double lon, double err) const throws();

         /**
          * Test if this extent intersects the specified extent.
          * @param e an extent
          * @return true if this extent and e intersect
          */
      public:
         bool intersects(const Extent& e) const throws();

         /**
          * Test if this extent encloses the specified extent.
          * @param e an extent
          * @return true if this extent contains e
          */
      public:
         bool encloses(const Extent& e) const throws();

         /**
          * Merge this exent with another extent.
          * @param e an extent
          * @return this
          */
      public:
         Extent& merge(const Extent& e)throws();

         /**
          * Shift this extent by a multiple of 2PI in longitude until it has an intersection. If this extent
          * cannot be shifted to intersect with e then this extent is
          * not modified.
          * @param e an extent
          * @return true if a shift existed, false otherwise
          */
      public:
         bool shiftTowards(const Extent& e)throws();

         /**
          * Print this extent object to the specified stream.
          * @param out an output stream
          * @return out
          */
      public:
         ::std::ostream& print(::std::ostream& out) const;

      private:
         double _minx, _miny;
         double _maxx, _maxy;
         double _width, _height;
   };
}

inline ::std::ostream& operator<<(::std::ostream& out, const ::terra::Extent& b)
{
   return b.print(out);
}

#endif
